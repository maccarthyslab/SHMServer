# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0008_session_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='cluster_group',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='virtualdatasetsequence',
            name='cluster_group',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
