# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0007_sequence_imgt_cdr3_string'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='description',
            field=models.CharField(max_length=40, blank=True),
        ),
    ]
