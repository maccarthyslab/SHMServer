# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mutation',
            name='aa_position',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sequence',
            name='consensus_offset_end',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sequence',
            name='consensus_offset_start',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sequence',
            name='frequency',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sequence',
            name='group',
            field=models.ForeignKey(blank=True, to='shmserver.Group', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='expire_by',
            field=models.DateTimeField(blank=True),
            preserve_default=True,
        ),
    ]
