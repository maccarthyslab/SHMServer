# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0013_sequence_cdr3_start_index'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='cell_type',
            field=models.CharField(max_length=5, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='sequence',
            name='conscount',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
