# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0009_auto_20160218_2029'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='j_consensus',
            field=models.ForeignKey(related_name='j_consensus', blank=True, to='shmserver.Consensus', null=True),
        ),
    ]
