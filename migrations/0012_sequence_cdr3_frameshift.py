# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0011_sequence_v_string'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='cdr3_frameshift',
            field=models.BooleanField(default=False),
        ),
    ]
