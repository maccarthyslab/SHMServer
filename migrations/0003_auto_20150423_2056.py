# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0002_auto_20150319_1732'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='consensus',
            table='consensuses',
        ),
        migrations.AlterModelTable(
            name='dataset',
            table='datasets',
        ),
        migrations.AlterModelTable(
            name='group',
            table='groups',
        ),
        migrations.AlterModelTable(
            name='illuminadata',
            table='illumina_data',
        ),
        migrations.AlterModelTable(
            name='mutation',
            table='mutations',
        ),
        migrations.AlterModelTable(
            name='sequence',
            table='sequences',
        ),
        migrations.AlterModelTable(
            name='session',
            table='sessions',
        ),
    ]
