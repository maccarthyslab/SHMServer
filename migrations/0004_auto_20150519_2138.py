# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0003_auto_20150423_2056'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='consensus',
            table='consensus',
        ),
        migrations.AlterModelTable(
            name='dataset',
            table='dataset',
        ),
        migrations.AlterModelTable(
            name='group',
            table='data_group',
        ),
        migrations.AlterModelTable(
            name='mutation',
            table='mutation',
        ),
        migrations.AlterModelTable(
            name='sequence',
            table='sequence',
        ),
        migrations.AlterModelTable(
            name='session',
            table='session',
        ),
    ]
