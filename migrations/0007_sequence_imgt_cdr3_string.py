# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0006_auto_20151203_2134'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='imgt_cdr3_string',
            field=models.TextField(null=True, blank=True),
        ),
    ]
