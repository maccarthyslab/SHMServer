# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0004_auto_20150519_2138'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hotspot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hotspot_target', models.CharField(max_length=1)),
                ('lcontext', models.CharField(max_length=3)),
                ('rcontext', models.CharField(max_length=3)),
                ('full_sequence', models.CharField(max_length=7)),
            ],
            options={
                'db_table': 'hotspot',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HotspotConsensus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('forward_orientation', models.BooleanField(default=True)),
                ('nt_position', models.IntegerField()),
                ('position_start', models.IntegerField()),
                ('position_end', models.IntegerField()),
                ('consensus', models.ForeignKey(to='shmserver.Consensus')),
                ('hotspot', models.ForeignKey(to='shmserver.Hotspot')),
            ],
            options={
                'db_table': 'consensus_hotspot',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Indel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('insertion_or_del', models.BooleanField(default=True)),
                ('indel_begin', models.IntegerField()),
                ('indel_end', models.IntegerField()),
                ('inserted_sequence', models.CharField(max_length=500)),
                ('sequence', models.ForeignKey(to='shmserver.Sequence')),
            ],
            options={
                'db_table': 'indel',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='illuminadata',
            name='member_of_pair',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='mutation',
            name='transversion',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
