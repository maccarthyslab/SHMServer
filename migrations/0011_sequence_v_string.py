# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0010_sequence_j_consensus'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='v_string',
            field=models.TextField(null=True, blank=True),
        ),
    ]
