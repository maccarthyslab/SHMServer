# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0012_sequence_cdr3_frameshift'),
    ]

    operations = [
        migrations.AddField(
            model_name='sequence',
            name='cdr3_start_index',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
