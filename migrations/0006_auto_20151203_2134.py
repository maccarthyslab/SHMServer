# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shmserver', '0005_auto_20150625_1823'),
    ]

    operations = [
        migrations.CreateModel(
            name='VirtualDataset',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_description', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='VirtualDatasetSequence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sequence', models.ForeignKey(to='shmserver.Sequence')),
                ('virtual_dataset', models.ForeignKey(to='shmserver.VirtualDataset')),
            ],
        ),
        migrations.AddField(
            model_name='virtualdataset',
            name='sequence_set',
            field=models.ManyToManyField(to='shmserver.Sequence', through='shmserver.VirtualDatasetSequence'),
        ),
        migrations.AddField(
            model_name='virtualdataset',
            name='session',
            field=models.ForeignKey(to='shmserver.Session'),
        ),
    ]
