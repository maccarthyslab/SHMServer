from django.shortcuts import get_object_or_404, render, render_to_response, redirect
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.views import generic
from django.db import transaction, connection
from django.template import RequestContext
from django.utils.html import escape
import datetime, re, pdb, os
from datetime import timedelta
from shmserver import models, helpers, plotters, querybuilders, reporters, statistics
from django.views.decorators.csrf import csrf_exempt
import hashlib, time

from Bio import SeqIO

def random_seq():
  hash = hashlib.sha1()
  hash.update(str(time.time()))
  return(hash.hexdigest()[:10])


def add_key_to_cookies(session_key, request):
  if request.session.get('saved_sessions') == None:
    request.session["saved_sessions"] = [session_key]
  else:
    if not session_key in request.session["saved_sessions"]:
      request.session["saved_sessions"] += [session_key]

def query_object_from_key(key, session):
  if key.startswith('v'):
    return(session.virtualdataset_set.get(id=int(key[1:len(key)])))
  else:
    return(session.dataset_set.get(id=int(key)))

class InvalidFastaException(Exception):
  pass

# Create your views here.

def index_page(request):
  if os.access('news_and_updates.xml',os.F_OK): #check if file exists
    import xml.etree.ElementTree as ET
    root = ET.parse('news_and_updates.xml').getroot()
    news = {"items":[]}
    for x in root:
      item_dict = {}
      for y in x:
        item_dict[y.tag] = y.text
      news["items"].append(item_dict)
  else:
    news = {}
  
  return render_to_response('shmserver/index.html', {'news': news}, context_instance=RequestContext(request))#, context_instance=RequestContext(request))

class CreateSessionView(generic.TemplateView):
  template_name = 'shmserver/create_session.html'

def help_page(request):
  return render_to_response('shmserver/help.html', {}, context_instance=RequestContext(request))
  
def contact_page(request):
  return render(request, 'shmserver/contact.html', {})

def reports_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  if request.GET.get('query_type1'):
    results_hash = {}
    ds_ids = request.GET.getlist('used_dataset')
    #raise Exception('wew')
    for ds_id in ds_ids:
      data_obj = query_object_from_key(ds_id, s)
      r = reporters.Reporter(data_obj)
      r.add_constraint(request.GET['query_type1'])
      if request.GET.get('query_type2'):
        r.add_constraint(request.GET['query_type2'])
      results_hash[data_obj.short_description] = r.partition()#{'row1':[35,60],'row2':[50,60]}
    #raise Exception(results_hash)
    return render(request, 'shmserver/reports.html', {'session': s, 'results_hash':results_hash})
  else:
    return render(request, 'shmserver/reports.html', {'session': s})
  pass

def show_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  return render(request, 'shmserver/show_session.html', {'session' : s})

def my_custom_page_not_found_view(request):
  return render(request, '404.html', {})
  
def server_error_view(request):
  #also put in code to log/notify
  return render(request, '500.html', {})

def edit_session(request, session_key):
  if request.method == 'GET':
    s = get_object_or_404(models.Session,key=session_key)
    return render(request, 'shmserver/edit_session.html', {'session' : s})
  elif request.method == 'POST':
    s = get_object_or_404(models.Session,key=session_key)
    s.description = request.POST['description']
    s.save()
    for key, value in request.POST.iteritems():
      if key.startswith('v'):
        vds = models.VirtualDataset.objects.get(id=key.replace("v",''))
        vds.short_description = value
        vds.save()
    add_key_to_cookies(session_key, request)
    return render(request, 'shmserver/show_session.html', {'session' : s, 'status_msg': "Your session has been updated!"})
  pass

def query_builder_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  return render(request, 'shmserver/query_builder.html', {'session' : s, 'all_hs': models.Hotspot.objects.all()})

def query_results_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  ds_ids = request.GET.getlist('used_dataset')
  if ds_ids == []:
    return render(request, 'shmserver/query_builder.html', {'session' : s})
  else:
    filtered_seq_set = []
    counts_json = []
    title_name = "" #for adding 
    for ds_id in ds_ids:
      df = query_object_from_key(ds_id, s)#get_object_or_404(models.Dataset,id=ds_id)
      qb = querybuilders.QueryBuilder(df)
      for hash_key in request.GET.keys():
        if re.search("^query_type\d+$",hash_key):
          magic_no_string = re.sub("[^0-9]+","",hash_key)
          things = {"query_type" : request.GET['query_type'+magic_no_string], "aa_fr": request.GET['aa_fr'+magic_no_string], "aa_to": request.GET['aa_to'+magic_no_string], "max_mut" : request.GET['max_mut'+magic_no_string], "min_mut" : request.GET['min_mut'+magic_no_string], "position" : request.GET['position'+magic_no_string],"hs_id": request.GET.get('hs_id'+magic_no_string)}
          title_name = request.GET['query_type'+magic_no_string]
          if "no_flag"+magic_no_string in request.GET.keys():
            things["no_flag"] = "yay"
          qb.add_query_hash(things)
      queried_set = qb.sequence_set()
      filtered_seq_set.append(queried_set)
      counts_json.append({"name":str(escape(df.short_description)),"id":str(ds_id),"query_type":str(escape(title_name)),"in":len(queried_set), "out":len(df.sequence_set.all())-len(queried_set)})
      qb.export_to_fasta(models.Dataset.querybuilder_default_file)
      #counts_json is being rendered directly to js. as such, anything that's not an integer has to be escaped.
    return render(request, 'shmserver/query_results.html', {'session' : s, 'dataset': df, 'queried_results' : filtered_seq_set, 'subcounts': counts_json, 'len': sum(map(lambda foo: foo['in'], counts_json)), 'ds_ids': ds_ids})

def list_imgt_genes(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  return render(request, 'shmserver/list_imgt_genes.html', {'session':s})

def mutations_per_sequence_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  count_dict = {}
  for set in s.real_and_virtual_sets():
    count_dict[str(set.set_id())] = set.export_plot()
  pcts = {}
  for id, things in count_dict.items():
    dset = query_object_from_key(id, s)
    seq_sum = dset.total_sequence_sum()
    pcts[id] = map(lambda foo: foo/float(seq_sum), things[1])
    print pcts[id]
  return render(request, 'shmserver/mutation_per_sequence.html', {'session' : s, 'seq_counts':count_dict, 'pcts':pcts})

def view_clusters_session(request, session_key, dataset_id):
  s = get_object_or_404(models.Session,key=session_key)
  ds = query_object_from_key(dataset_id, s)
  return render(request, 'shmserver/view_clusters.html', {'session' : s, 'dataset': ds})
  #pass
  
def hotspots_per_sequence_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  hs_list = map(lambda foo: foo.full_motif().upper(), models.Hotspot.objects.all())
  if(request.GET.get('genename') and request.GET.get('ds')):
    dataset = query_object_from_key(request.GET['ds'], s)
    plotters.Plotter().hotspots_sequence_plot(dataset,hs_list,models.Consensus.objects.get(id=request.GET['genename']))
    sets_to_use = [dataset]
    pass
  else:
    sets_to_use = s.real_and_virtual_sets()
    for dataset in sets_to_use:
      plotters.Plotter().hotspots_sequence_plot(dataset,hs_list)
  return render(request, 'shmserver/hotspots_per_sequence.html', {'session' : s, 'sets_to_use': sets_to_use})

def mutation_profile_session(request,session_key):
  s = get_object_or_404(models.Session,key=session_key)
  #raise Exception('woo')
  return render(request, 'shmserver/mutation_profile.html', {'session' : s})

#Note: to automate eg: curl -v -L -F wat1=@example.CASE1.fas http://localhost:8000/shmserver/upload
#-v is for getting the key/redirected url. -L is to confirm the html output is correct

def motif_positions_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  return render(request, 'shmserver/motif_positions.html', {'session' : s})

@csrf_exempt
def datasets_compare_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  d1 = query_object_from_key(request.GET['set1'], s)
  d2 = query_object_from_key(request.GET['set2'], s)
  selectable_genes = d1.consensuses_list() #ideally the superset of the two
  cons_gene = models.Consensus.objects.get(id=int(request.GET['genename'])) if request.GET.get('genename') else None

  plotters.Plotter().compare_plot(d1,d2, 'hi.png', cons_gene)
  image_root = s.mutation_histogram_image_url(d1.id,d2.id)
  return render(request, 'shmserver/datasets_compare.html', {'session' : s, 'image_root' : image_root, 'selectable_genes': selectable_genes})

def assign_groups_session(request, session_key):
  if request.method == 'POST':
    s = get_object_or_404(models.Session,key=session_key)
    ds = query_object_from_key(request.POST['set1'], s)
    if request.POST.get('cluster_param'):# and request.POST.get('cluster_param').isdigit():
      try:
        cutoff = float(request.POST['cluster_param'])
      except ValueError as e:
        return render(request, 'shmserver/show_session.html', {'session' : s, 'status_msg': 'Clustering parameter invalid (must be digit)'})
    else:
      #raise Exception('hammertime')
      cutoff = 0.68
    try:
      if ds.sequence_set.all()[0].imgt_cdr3_string == None:
        raise Exception("These sequences do not have cdr3 data!")
      else:
        ds.cluster_groups_in_dataset(cutoff)#plotters.Plotter().distribute_groups(ds, cutoff)
        ds.export_clusters()
    except Exception as e:
      return render(request, 'shmserver/show_session.html', {'session' : s, 'status_msg': e})
    else:
      return redirect('view_clusters', session_key=s.key, dataset_id=ds.set_id())#, {'session' : s, 'status_msg': 'Your groups have been saved!'})
      #return render(request, 'shmserver/view_clusters.html', {'session' : s, 'dataset': ds,'status_msg': 'Your groups have been saved!'})
  else:
    return render(request, 'shmserver/show_session.html', {'session' : s, 'status_msg': 'Try again.'})
  
def mutations_compare_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  ds_ids1 = request.GET.getlist('set1')
  ds_ids2 = request.GET.getlist('set2')
  sets1 = []
  sets2 = []
  #TODO: Redirect to main page if either list is empty
  for ds_id in ds_ids1:
    sets1 += [query_object_from_key(ds_id, s)]
  for ds_id in ds_ids2:
    sets2 += [query_object_from_key(ds_id, s)]
  m = statistics.pooled_mutation_statistics_dict(sets1,sets2)
  u = statistics.pooled_unique_mutation_statistics_dict(sets1,sets2)
  return render(request, 'shmserver/mutations_compare.html', {'session' : s, 'm':m, 'u':u, 'sets1':sets1, 'sets2':sets2})

def motifs_compare_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  ds_ids1 = request.GET.getlist('set1')
  ds_ids2 = request.GET.getlist('set2')
  sets1 = []
  sets2 = []
  #TODO: Redirect to main page if either list is empty
  for ds_id in ds_ids1:
    sets1 += [query_object_from_key(ds_id, s)]
  for ds_id in ds_ids2:
    sets2 += [query_object_from_key(ds_id, s)]
    
  m = statistics.pooled_motif_statistics_dict(sets1,sets2)
  u = statistics.pooled_unique_motif_statistics_dict(sets1,sets2)
  return render(request, 'shmserver/motifs_compare.html', {'session' : s, 'm':m, 'u':u, 'sets1':sets1, 'sets2':sets2})

def motif_mutations_session(request, session_key):
  s = get_object_or_404(models.Session,key=session_key)
  return render(request, 'shmserver/motif_mutations.html', {'session' : s})

## This function is used to generate virtual datasets using a user defined query.
def virtual_session_session(request, session_key):
  if request.method == 'POST':
    ds_ids = request.POST.getlist('used_dataset_ids')
    #note: next part is repetitive/needs refactoring, in more ways than one
    try:
      with transaction.atomic():
        if not request.POST.get('combine_sets'):
          vds_name = request.POST.get('vd_set_name') if request.POST.get('vd_set_name') else "virtualdataset"
          vds = models.VirtualDataset(session=get_object_or_404(models.Session,key=session_key), short_description=vds_name)
          vds.save()
          for ds_id in ds_ids:
            df = query_object_from_key(key=ds_id,session=get_object_or_404(models.Session,key=session_key))
            session = df.session
            qb = querybuilders.QueryBuilder(df)
            for hash_key in request.POST.keys():
              if re.search("^query_type\d+$",hash_key):
                magic_no_string = re.sub("[^0-9]+","",hash_key)
                #this needs a kwargs
                things = {"query_type" : request.POST['query_type'+magic_no_string], "aa_fr": request.POST['aa_fr'+magic_no_string], "aa_to": request.POST['aa_to'+magic_no_string], "max_mut" : request.POST['max_mut'+magic_no_string], "min_mut" : request.POST['min_mut'+magic_no_string], "position" : request.POST['position'+magic_no_string],"hs_id": request.POST['hs_id'+magic_no_string]}
                if "no_flag"+magic_no_string in request.POST.keys():
                  things["no_flag"] = "yay"
                qb.add_query_hash(things)
            queried_set = qb.sequence_set()
            for seq in queried_set:
              vdss = models.VirtualDatasetSequence(virtual_dataset=vds,sequence=seq)
              vdss.save()
        else:
          for ds_id in ds_ids:
            df = query_object_from_key(key=ds_id,session=get_object_or_404(models.Session,key=session_key))
            session = df.session
            qb = querybuilders.QueryBuilder(df)
            for hash_key in request.POST.keys():
              if re.search("^query_type\d+$",hash_key):
                magic_no_string = re.sub("[^0-9]+","",hash_key)
                vds_name = request.POST.get('vd_set_name')+df.short_description if request.POST.get('vd_set_name') else "virtualdataset"
                things = {"query_type" : request.POST['query_type'+magic_no_string], "aa_fr": request.POST['aa_fr'+magic_no_string], "aa_to": request.POST['aa_to'+magic_no_string], "max_mut" : request.POST['max_mut'+magic_no_string], "min_mut" : request.POST['min_mut'+magic_no_string], "position" : request.POST['position'+magic_no_string]}
                if "no_flag"+magic_no_string in request.POST.keys():
                  things["no_flag"] = "yay"
                qb.add_query_hash(things)
            queried_set = qb.sequence_set()
            vds = models.VirtualDataset(session=get_object_or_404(models.Session,key=session_key), short_description=vds_name)
            vds.save()
            for seq in queried_set:
              vdss = models.VirtualDatasetSequence(virtual_dataset=vds,sequence=seq)
              vdss.save()
    except Exception as e:
      #oh well. *insert shrug here*
      return render(request, 'shmserver/show_session.html', {'session': get_object_or_404(models.Session,key=session_key), 'status_msg':e})    
    return render(request, 'shmserver/show_session.html', {'session': get_object_or_404(models.Session,key=session_key), 'status_msg':"Your virtual dataset has been created!"})
  else:
    return render(request, 'shmserver/index.html')

@csrf_exempt
def upload_data(request):
  if request.method == 'POST':
    try:
      with transaction.atomic():
        # NOTE(srmeier): select a random session key and if it isn't unique get a new one
        input_key = random_seq()
        while len(models.Session.objects.filter(key=input_key)) > 0:
          input_key = random_seq()
        # NOTE(srmeier): create a new session for the data being uploaded
        s = models.Session(key=input_key,description=request.POST['description'],expire_by=datetime.datetime.now() + timedelta(weeks=4))
        s.save()

        # NOTE(srmeier): keep looping until we run out of submitted data
        upload_index = 1;
        while ("wat"+str(upload_index) in request.FILES) or ("imgtFile"+str(upload_index) in request.FILES):
          if "is_imgt-"+str(upload_index) in request.POST:
            # here we know the data is IMGT
            imgtFile = request.FILES["imgtFile"+str(upload_index)];
            desc_tag = request.POST["short_description"+str(upload_index)];
            records = None;
            if "wat"+str(upload_index) in request.FILES:
              fastaFile = request.FILES["wat"+str(upload_index)];
              records = SeqIO.parse(fastaFile, "fasta");
            s.import_imgt(imgtFile, desc_tag, records);
          else:
            # here we know the data is not IMGT
            fastaFile = request.FILES["wat"+str(upload_index)];
            desc_tag = request.POST["short_description"+str(upload_index)];
            consensus = request.POST["consensus"+str(upload_index)];
            records = SeqIO.parse(fastaFile, "fasta");
            s.import_data(records, desc_tag, consensus)
          upload_index += 1;

        # add the session key to the saved_sessions
        if request.session.get('saved_sessions') == None:
          request.session["saved_sessions"] = [input_key]
        else:
          request.session["saved_sessions"] += [input_key]
    except InvalidFastaException as e:
      return render(request, 'shmserver/create_session.html', {'error_message': str(e)})
    except Exception as e:
      return render(request, 'shmserver/create_session.html', {'error_message': str(e)})#"Upload failed. Please make sure your fasta file and descriptions are valid and try again."})
    #  #return render(request, 'shmserver/create_session.html', {'error_message': "Upload failed. Please make sure your fasta file and descriptions are valid and try again."})
    else:
      return HttpResponseRedirect('session/'+input_key)
  else:
    return render(request, 'shmserver/index.html')
