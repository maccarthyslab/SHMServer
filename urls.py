from django.conf import settings
from django.conf.urls import include, patterns, url

from shmserver import views


#This is a comment!
urlpatterns = patterns('',
    url(r'^$', views.index_page, name='index'),
    url(r'^session/create$', views.CreateSessionView.as_view(), name='create_session'),
    url(r'^session/(?P<session_key>\w+)$', views.show_session, name='show_session'),
    url(r'^session/(?P<session_key>\w+)/edit$', views.edit_session, name='edit_session'),
    url(r'^session/(?P<session_key>\w+)/mutations_per_sequence$', views.mutations_per_sequence_session, name='mutations_per_sequence'),
    url(r'^session/(?P<session_key>\w+)/hotspots_per_sequence$', views.hotspots_per_sequence_session, name='hotspots_per_sequence'),
    url(r'^session/(?P<session_key>\w+)/mutation_profile$', views.mutation_profile_session, name='mutation_profile'),
    url(r'^session/(?P<session_key>\w+)/reports$', views.reports_session, name='reports'),
    url(r'^session/(?P<session_key>\w+)/motif_position$', views.motif_positions_session, name='motif_positions'),
    url(r'^session/(?P<session_key>\w+)/motif_mutations$', views.motif_mutations_session, name='motif_mutations'),
    url(r'^session/(?P<session_key>\w+)/datasets_compare$', views.datasets_compare_session, name='datasets_compare'),
    url(r'^session/(?P<session_key>\w+)/motifs_compare$', views.motifs_compare_session, name='motifs_compare'),
    url(r'^session/(?P<session_key>\w+)/mutations_compare$', views.mutations_compare_session, name='mutations_compare'),
    url(r'^session/(?P<session_key>\w+)/query_builder$', views.query_builder_session, name='query_builder'),
    url(r'^session/(?P<session_key>\w+)/query_results$', views.query_results_session, name='query_results'),
    url(r'^session/(?P<session_key>\w+)/virtual_session$', views.virtual_session_session, name='virtual_session'),
    url(r'^session/(?P<session_key>\w+)/list_imgt_genes$', views.list_imgt_genes, name='list_imgt_genes'),
    url(r'^session/(?P<session_key>\w+)/assign_groups$', views.assign_groups_session, name='assign_groups'),
    url(r'^session/(?P<session_key>\w+)/(?P<dataset_id>\w+)/view_clusters$', views.view_clusters_session, name='view_clusters'),
    url(r'^upload$', views.upload_data, name='upload_form'),
    url(r'^help$', views.help_page, name='help'),
    url(r'^contact$', views.contact_page, name='contact')
)

handler404 = views.my_custom_page_not_found_view
handler500 = views.server_error_view
#For debug toolbar for local development use. See settings.py.dev to see how to activate.
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]