from django.db import models, connection
from django.db.models import Max, Count
from django.conf import settings
import re, json, numpy, pdb
from Bio import Seq, SeqIO, SeqUtils
from itertools import izip
from fractions import Fraction
import plotters, collections, itertools, os, scipy.stats, csv
import time
import editdistance

from imgt_importer import ImgtImporter


import numpy as np
from itertools import groupby, combinations
import scipy.cluster.hierarchy as sch
null_char = "*"
deleted_nt_char = '.'

nucleotides = ['A','C','G','T']

codons = {"AAA" : "K", "AAG" : "K", "AAT" : "N", "AAC" : "N", "ACA" : "T", "ACC" : "T", "ACG" : "T", "ACT" : "T","AGA" : "R", "AGC" : "S", "AGG" : "R", "AGT" : "S", "ATA" : "I", "ATC" : "I", "ATG" : "M", "ATT" : "I", "CAA" : "Q", "CAC" : "H", "CAG" : "Q", "CAT" : "H", "CCA" : "P", "CCC" : "P", "CCG" : "P", "CCT" : "P","CGA" : "R", "CGC" : "R", "CGG" : "R", "CGT" : "R", "CTA" : "L", "CTC" : "L", "CTG" : "L", "CTT" : "L","GAA" : "E", "GAC" : "D", "GAG" : "E", "GAT" : "D", "GCA" : "A", "GCC" : "A", "GCG" : "A", "GCT" : "A","GGA" : "G", "GGC" : "G", "GGG" : "G", "GGT" : "G", "GTA" : "V", "GTC" : "V", "GTG" : "V", "GTT" : "V","TAA" : null_char, "TAC" : "Y", "TAG" : null_char, "TAT" : "Y", "TCA" : "S", "TCC" : "S", "TCG" : "S", "TCT" : "S","TGA" : null_char, "TGC" : "C", "TGG" : "W", "TGT" : "C", "TTA" : "L", "TTC" : "F", "TTG" : "L", "TTT" : "F"}

imgt_species_abbreviations = {"Musmus":"Mus", "Homsap":"Homo sapiens"}
def hamdist(str1, str2):
  '''
  Hamming distance between two strings of equivalent length = # of mismatches
  '''
  diffs = 0
  for ch1, ch2 in zip(str1, str2):
    if ch1 != ch2:
      diffs = diffs + 1
  return diffs

def dictfetchall(cursor):
  "Returns all rows from a cursor as a dict"
  desc = cursor.description
  return [
    dict(zip([col[0] for col in desc], row))
    for row in cursor.fetchall()
  ]

def left_2_nt(zero_index,seq):
  '''
  Grab the two nucleotides on seq to the left of the zero_index.
  e.g. if sequence is AA.AGA and zero_index is 4 (5th char), will grab AA
  if zero_index is 1, will grab -A
  '''
  chr = ""
  pointer = zero_index
  while (len(chr) < 2):
    pointer -= 1
    if(pointer < 0):
      chr = '-' + chr
    elif (seq[pointer] != "." and seq[pointer] != '-'):
      chr = seq[pointer] + chr
  return chr
    
  
def right_2_nt(zero_index,seq):
  '''
  Grab the two nucleotides on seq to the right of the zero_index.
  e.g. if sequence is AA.AGA and zero_index is 4 (5th char), will grab A-
  if zero_index is 1, will grab AG
  '''
  chr = ""
  pointer = zero_index
  while (len(chr) < 2):
    pointer += 1
    if (pointer >= len(seq)):
      chr = chr + '-'
    elif (seq[pointer] != "." and seq[pointer] != '-'):
      chr = chr + seq[pointer]
  return chr

class InvalidFastaException(Exception):
  pass
  
def tags_from_annotation(anno_string):
  #input: a string like 'DUPCOUNT=14|CIGAR=14M'
  #output: a dict as such: {'DUPCOUNT':14,'CIGAR':'14M'}
  annotations_list = {}
  tags = re.split("[ |_]", anno_string)
  for tag in tags:
    if re.search(r'^\w+=', tag):
      val1, val2 = tag.split("=",2)
      annotations_list[val1] = val2
  return(annotations_list)

def sequence_compare(seq1, seq2):
	'''
	Compare sequences seq1 and seq2 and report all positions in which
	they fail to match.
	'''
	# Determine the length of each sequence
	len1 = len(seq1)
	len2 = len(seq2)
	# Prepare our list of mismatches
	mismatches = []

	if (seq1 != seq2):		
		# Step through the sequences, checking for mismatches
		for pos in range(0, min(len1, len2)):
			if seq1[pos].upper() != seq2[pos].upper():
				mismatches.append(pos)
	return mismatches
def splitIterator(text, size):
	'''
	Splits a string into equal chunks of given size
	'''
	assert size > 0, "size should be > 0"
	for start in xrange(0, len(text), size):
		yield text[start:start + size]


class Session(models.Model):
  #=======================================================
  key = models.CharField(max_length=45,blank=False)
  expire_by = models.DateTimeField(blank=True)
  description = models.CharField(max_length=40,blank=True)
  #=======================================================
  
  min_required_sequences = 1
  
  def real_and_virtual_sets(self):
    se1 = self.virtualdataset_set.all()
    se2 = self.dataset_set.all()
    return(list(itertools.chain(se2,se1)))
  
  def sequence_set_query_stuff(self):
    ds_ids = map(lambda foo: foo.id, self.dataset_set.all())
    return(Sequence.objects.filter(reduce(lambda x, y: x | y, [models.Q(dataset_id=ds_id) for ds_id in ds_ids])))
    
  def comparative_plot_file(self,dataset1id,dataset2id):
    return("compare_"+str(dataset1id)+"_"+str(dataset2id)+".png")
  def mutation_histogram_image_path(self,dataset1id,dataset2id):
    newpath = os.path.join(settings.MEDIA_ROOT, self.key)
    if not os.path.exists(newpath):
      os.makedirs(newpath)
      os.chmod(newpath, 0777)
    return(os.path.join(newpath, self.comparative_plot_file(dataset1id,dataset2id)))
  
  def mutation_histogram_image_url(self,dataset1id,dataset2id):
    return(settings.HACKISH_IMAGE_ROOT + settings.MEDIA_URL + self.key+'/'+self.comparative_plot_file(dataset1id,dataset2id))

  def import_data(self,records_fasta, short_description="", consensus="CONSENSUS"):
    new_dataset = Dataset(session=self,short_description=short_description)
    new_dataset.save()
    if short_description == "":
      new_dataset.short_description = "dataset_"+str(new_dataset.id)
      new_dataset.save()
    fasta_records = []
    for each_record in records_fasta:
      fasta_records.append(each_record)
    new_consensus = self.create_consensus(fasta_records, consensus)
    new_consensus.write_hotspots_to_table()
    
    self.create_sequences(new_dataset,new_consensus,fasta_records)
    self.create_mutations(new_dataset,new_consensus,fasta_records)
    
  def import_imgt(self, imgt_file, short_description="", records_fasta=None):
    gapped_input_file = imgt_file

    list_records_fasta = None;
    if records_fasta is not None:
      list_records_fasta = list(records_fasta);
    
    self.create_imgt_dataset_and_sequences(gapped_input_file.read().splitlines(), short_description, list_records_fasta)

  

  def create_imgt_dataset_and_sequences(self, input_lines, short_description="", list_records_fasta=None):
    new_dataset = Dataset(session=self)
    new_dataset.save()

    unique_strings = {}

    if short_description == "":
      new_dataset.short_description = "dataset_"+str(new_dataset.id)
    else:
      new_dataset.short_description = short_description

    i1 = ImgtImporter(input_lines, list_records_fasta)
    unique_strings = []
    unique_strings = i1.unique_strings()
    #print("Input file not in correct format. Skipping "+ str(gapped_file_list[1]) + " sequence.")
    tempseqs = []
    #tempdels = []

    for unique_string, value in unique_strings.iteritems():
      if value['consensus'] != None and value['j_consensus'] != None and value['count'] >= self.min_required_sequences: #magic number
        #TODO: Make sure the sequence string is proper.
        seq = Sequence(
          dataset=new_dataset, sequence_string = value['sequence_string'],
          v_string = value['v_string'], cdr3_frameshift = value.get('cdr3_frameshift'),
          consensus=value['consensus'],j_consensus=value['j_consensus'],prod_or_nonprod=value['prod_or_not'],
          gapped=True,v_region=value['v_str'],consensus_offset_start=value['cos'],consensus_offset_end=value['coe'],
          extra_notes=value['notes'],frequency=value['count'], imgt_cdr3_string=value['cdr3_str'],
          accession_number='',cdr3_start_index=value['csi'],conscount=value['conscount'],cell_type=value['celltype'],
          cluster_group=value['cluster_group']
        );
        #accession number is set because it's a required field for some reason
        tempseqs.append(seq)
    if len(tempseqs) > 0:
      new_dataset.save()
      Sequence.objects.bulk_create(tempseqs,batch_size=10000)
      new_dataset.create_mutations()
      for seq in tempseqs:
        pass
    else:
      raise Exception("This dataset has not enough sequences to be saved properly.")
    
  def create_consensus(self, fasta_records, consensus):
    '''
    input: scans a fasta input string such as
    >CONSENSUS
    AAA
    >read1
    ACG....
    and will find the record with the description (past the > ) matching the consensus input
    '''
    for each_record in fasta_records:
      if each_record.description == consensus:
        #self.consensus = each_record
        consensus_sequence = str(each_record.seq).upper()
        consensus_object = Consensus(sequence_string=consensus_sequence)
        consensus_object.save()
        return(consensus_object)
    raise InvalidFastaException("No Consensus sequence found")

  def create_sequences(self,dataset,consensus_object,fasta_records):
    bulk = []
    for each_record in fasta_records:
      description = each_record.description
      annotations_list = {}
      if description != "CONSENSUS":
        annotations_list = tags_from_annotation(description)
        #if not 'CIGAR' in annotations_list or 'CIGAR' in annotations_list and (re.search('[ID]',annotations_list['CIGAR']) == None):
        if 'CIGAR' in annotations_list and (re.search('[ID]',annotations_list['CIGAR']) != None):
          ref_seq = each_record.seq.upper()
          mm_string = annotations_list['CIGAR']
          stringlens = re.findall('[0-9]+',mm_string)
          types = re.findall('[DIM]',mm_string)
          insert_structs = []
          del_structs = []
          reconstructed = ''
          pointer = cons_pointer = 0
          for abc in range(len(stringlens)):
            if types[abc] == 'M':
              reconstructed += ref_seq[pointer:(pointer+int(stringlens[abc]))]
              pointer += int(stringlens[abc])
              cons_pointer += int(stringlens[abc])
              #print cons_pointer
            elif types[abc] == 'I':
              ref_insert = ref_seq[pointer:(pointer+int(stringlens[abc]))]
              prepointer = pointer
              pointer += int(stringlens[abc])
              insert_structs.append({"position":prepointer,"length":int(stringlens[abc]), "sequence":ref_insert})
            elif types[abc] == 'D':
              ref_delete = consensus_object.sequence_string[(cons_pointer):(cons_pointer+int(stringlens[abc]))]
              reconstructed += deleted_nt_char*int(stringlens[abc])
              
              del_structs.append({"position":cons_pointer+1,"length":int(stringlens[abc]), "sequence":ref_delete})
              cons_pointer += int(stringlens[abc])
          to_write = Sequence(sequence_string=each_record.seq.upper(), v_string=reconstructed.upper(), dataset=dataset,consensus=consensus_object)
          if 'DUPCOUNT' in annotations_list:
            to_write.frequency = int(annotations_list['DUPCOUNT'])
          to_write.save()
          for ins_struct in insert_structs:
            created_indel = Indel(sequence=to_write,inserted_sequence=ins_struct["sequence"],insertion_or_del=True,indel_begin=ins_struct["position"],indel_end=ins_struct["position"])
            created_indel.save()
          for del_struct in del_structs:
            created_indel = Indel(sequence=to_write,inserted_sequence=del_struct["sequence"],insertion_or_del=False,indel_begin=del_struct["position"],indel_end=del_struct["position"]+del_struct["length"]-1)
            created_indel.save()
        else:
          to_write = Sequence(sequence_string=each_record.seq.upper(), dataset=dataset,consensus=consensus_object)
          if 'DUPCOUNT' in annotations_list:
            to_write.frequency = int(annotations_list['DUPCOUNT'])
          bulk.append(to_write)
    #set global max_allowed_packet=64*1024*1024;
    #select @@max_allowed_packet;
    #bulk_create 
    Sequence.objects.bulk_create(bulk,batch_size=10000)
    
  def create_mutations(self,dataset,consensus_object,fasta_records):
    dataset.create_mutations()
    #sequences = Sequence.objects.filter(dataset=dataset)
    #for seq in sequences:
    #  seq.add_mutations()

  class Meta:
    db_table = "session"

#mixins have to inherit from object otherwise migrations will not work
#see: https://code.djangoproject.com/ticket/23574#no1
class DatasetAnalyzable(object):
  def assign_cluster_groups_without_r(self, cutoff=0.8):
    seqs = self.sequence_set.prefetch_related('consensus').all()
    groups = {}
    for record in seqs:
      if groups.get(record.imgt_v_gene() + record.imgt_j_gene()) == None:
        groups[record.imgt_v_gene() + record.imgt_j_gene()] = [record]
      else:
        groups[record.imgt_v_gene() + record.imgt_j_gene()].append(record)
    basegroup = 0
    totalgroups = 0
    for foo, gruppe in groups.items():
      basegroup += 1
      record_size = len(gruppe)
      d_mat = np.zeros((record_size,record_size))
      #print(totalgroups)
      if (record_size > 1):
        for j,k in combinations(range(record_size), 2):
          d_mat[j,k] = d_mat[k,j] = editdistance.eval(gruppe[k].sequence_string, gruppe[j].sequence_string)
        my_l = sch.linkage(d_mat,'single')
        ind = sch.fcluster(my_l, cutoff*d_mat.max(), 'distance')
        #print(d_mat)
        
        ind2 = [x + totalgroups for x in ind]
        for abcd in range(record_size):
          temp_record = gruppe[abcd]
          temp_record.cluster_group = ind2[abcd]
          temp_record.save()
        totalgroups = totalgroups + max(ind)
      else:
        temp_record = gruppe[0]
        temp_record.cluster_group = totalgroups + 1
        temp_record.save()
        totalgroups = totalgroups + 1
    print("There are "+str(totalgroups)+" clusters for Dataset "+str(self.id)+".")

  def cluster_groups_in_dataset(self,cutoff):
    plotters.Plotter().distribute_groups(self,cutoff)
  
  def hotspots_sequence_plot_path(self):
    return(self.common_root_path(self.lirong_plot_name))
    
  def hotspots_sequence_image_url(self):
    return(self.common_image_url(self.lirong_plot_name))
    
  def groups_plot_path(self):
    return(self.common_root_path(self.group_plot_name))
    
  def groups_image_url(self):
    return(self.common_image_url(self.group_plot_name))
  
  def groups_trees_plot_path(self):
    return(self.common_root_path(self.group_trees_plot_name))
    
  def groups_trees_image_url(self):
    return(self.common_image_url(self.group_trees_plot_name))
    
  def total_sequence_sum(self):
    #optimize this somehow
    seq_freqs = self.sequence_set.all().values('frequency')
    #seq_sum = 0
    seq_sum = sum(map(lambda foo: foo['frequency'] if foo['frequency'] != None else 1, seq_freqs))
    #for freq in seq_freqs:
    #  if freq['frequency'] == None:
    #    seq_sum += 1
    #  else:
    #    seq_sum += freq['frequency']
    ##dupcounts = itertools.imap(lambda foo: foo.norm_frequency(), sequences)
    return(seq_sum)
    
  def possible_mutations(self):
    '''
    Displays the total number of possible mutations from all consensuses.
    e.g. if there are one consensus AACGT, 2 As 1 C 1 G 1 T are possible.
    e.g. if there are two ACGT and CCGG, 1 A 3 C 3 G 1 T
    '''
    consensuses = list(set(map(lambda foo: foo.consensus, self.sequence_set.prefetch_related('consensus').all())))
    #seqs = list(set(map(lambda foo: foo.consensus, d1.sequence_set.all())))[0].sequence_set.filter(dataset = self).all()
    last_dict = {'A':0,'C':0,'G':0,'T':0}
    for nt in nucleotides:
      last_dict['total'+nt] = 0
    for consensus in consensuses:
      for nt in nucleotides:
        last_dict[nt] += consensus.sequence_string.count(nt)

    for seq in self.sequence_set.all():
      for nt in nucleotides:
        last_dict['total'+nt] += seq.consensus.sequence_string.count(nt)*seq.norm_frequency()
    return(last_dict)
    
  def possible_motif_mutations(self):
    '''
    Displays the total number of possible mutations from all consensuses.
    e.g. if there are one consensus AACGT, 2 As 1 C 1 G 1 T are possible.
    e.g. if there are two ACGT and CCGG, 1 A 3 C 3 G 1 T
    {'cC':{'G':4, 'T':8}}
    '''
    mutations_dict = {}
    all_hs = Hotspot.objects.all()
    for hs in all_hs:
      mutations_dict[hs.full_motif()] = 0
      mutations_dict['total'+hs.full_motif()] = 0
    consensuses = list(set(map(lambda foo: foo.consensus, self.sequence_set.prefetch_related('consensus').prefetch_related('consensus__hotspotconsensus_set').all())))
    for consensus in consensuses:
      hcs = consensus.hotspotconsensus_set.prefetch_related('hotspot').all()
      hs_count_dict = {}
      for hs in all_hs:
        hs_count_dict[hs.full_motif()] = len(hcs.filter(hotspot = hs))
        mutations_dict[hs.full_motif()] += hs_count_dict[hs.full_motif()]
      seqs = self.sequence_set.filter(consensus=consensus).values('frequency')

      for seq_num in seqs:

        norm_freq = seq_num['frequency'] if seq_num['frequency'] != None else 1
        for hs in all_hs:

          mutations_dict['total'+hs.full_motif()] += hs_count_dict[hs.full_motif()]*norm_freq
    return(mutations_dict)
    
  def total_groups(self):
    return(self.sequence_set.all().aggregate(Max('cluster_group'))['cluster_group__max'])
  
  def most_frequent_consensus(self):
    #most frequent means the most # of unique sequences, and is not normalized by frequency
    seqs_with_cons = self.sequence_set.prefetch_related('consensus').all()
    seqs_with_cons = seqs_with_cons.values('consensus').annotate(cons_ct=Count('consensus')).order_by('-cons_ct') #values consensus gets the id of the consensus, not the object itself   
    #print max()
    #consensuses = list(set(map(lambda foo: foo.consensus, self.sequence_set.prefetch_related('consensus').all())))
    return(Consensus.objects.get(id=seqs_with_cons[0]['consensus']))
  
  def consensuses_list(self):
    '''
    For imgt purposes, returns a list of consensuses of the 10 or so most frequently occurring consensus
    '''
    seqs_with_cons = self.sequence_set.prefetch_related('consensus').all()
    cons_freq_dict = {}
    for seq in seqs_with_cons:
      if cons_freq_dict.get(seq.consensus_id):
        cons_freq_dict[seq.consensus_id] += seq.norm_frequency()
      else:
        cons_freq_dict[seq.consensus_id] = seq.norm_frequency()
    cons_ids = ([k for k, v in sorted(cons_freq_dict.iteritems(), key=lambda(k, v): (-v, k))])
    final_cons_list = map(lambda foo: Consensus.objects.get(id=foo), cons_ids)
    return(final_cons_list)
  
  def consensuses_list_names(self):
    name_dict = map(lambda foo: foo.gene, self.consensuses_list())
    return(name_dict)
    
  def consensuses_list_freqs(self):
    '''
    displays the percentage of each gene in a dataset belonging to a specific consensus/IMGT gene.
    This is normalized by gene frequency, rather than looking at unique #.
    Some code is left in as comments if not normalizing/going by unique
    '''
    output_dict = {}
    seqs_with_cons = self.sequence_set.prefetch_related('consensus').all()
    cons_freq_dict = {}
    total = float(0)
    for seq in seqs_with_cons:
      total += seq.norm_frequency()
      if cons_freq_dict.get(seq.consensus_id):
        cons_freq_dict[seq.consensus_id] += seq.norm_frequency()#or 1
      else:
        cons_freq_dict[seq.consensus_id] = seq.norm_frequency()# or 1
    cons_ids = ([k for k, v in sorted(cons_freq_dict.iteritems(), key=lambda(k, v): (-v, k))])
    for cons_id in cons_ids:
      #there is an issue where multiple consensuses can have the same gene name and that skews things
      genename = Consensus.objects.get(id=cons_id).gene
      genefreq = cons_freq_dict[cons_id] / total
      if(output_dict.get(str(genename))):
        output_dict[str(genename)] += genefreq
      else:
        output_dict[str(genename)] = genefreq
    return(output_dict)
    
  
  def sequence_set_with_consensus(self, cons_obj):
    cons_id = cons_obj.id
    seqs_with_cons = self.sequence_set.prefetch_related('consensus').filter(consensus=cons_id)
    return(seqs_with_cons)
    
  def longest_consensus(self):
    consensuses = list(set(map(lambda foo: foo.consensus, self.sequence_set.prefetch_related('consensus').all())))
    longest = max(consensuses, key=lambda foo: len(foo.sequence_string))
    return(longest)
    
  def has_consensus(self,ref_cons):
    swc = self.sequence_set_with_consensus(ref_cons)
    if(len(swc) > 0):
      return True
    else:
      return False
  
  #lot of duplication here :/
  def total_sequence_sum_of_cons(self, ref_cons=None):
    longest_cons_obj = self.most_frequent_consensus() if ref_cons == None else ref_cons
    seq_freqs = self.sequence_set_with_consensus(longest_cons_obj).all().values('frequency')
    seq_sum = sum(map(lambda foo: foo['frequency'] if foo['frequency'] != None else 1, seq_freqs))
    return(seq_sum)
  def mutation_distribution_of_cons(self, ref_cons=None):
    field_key = 'sequence_string'
    
    longest_cons_obj = self.most_frequent_consensus() if ref_cons == None else ref_cons
    longest_cons = longest_cons_obj.sequence_string#max(self.sequence_set.values(field_key), key=lambda foo: foo[field_key])[field_key]
    y_coords = [0]*(len(longest_cons))
  
    seqs = self.sequence_set_with_consensus(longest_cons_obj).prefetch_related('mutation_set').all()
    mut_tuples = seqs.values_list('mutation__nt_position', 'frequency')
    for x in mut_tuples:
      if x[0] != None:
        y_coords[x[0] - 1] += (x[1] if x[1] != None else 1)
    return(y_coords)
  def rs_mutation_distribution_of_cons(self , ref_cons=None):
    field_key = 'sequence_string'
  
    longest_cons_obj = self.most_frequent_consensus()  if ref_cons == None else ref_cons
    longest_cons = longest_cons_obj.sequence_string#max(self.sequence_set.values(field_key), key=lambda foo: foo[field_key])[field_key]
    y_coords = [0]*(len(longest_cons))
  
    seqs = self.sequence_set_with_consensus(longest_cons_obj).prefetch_related('mutation_set').filter(mutation__r_or_s='R').all()
    mut_tuples = seqs.values_list('mutation__nt_position', 'frequency')
    for x in mut_tuples:
      if x[0] != None:
        y_coords[x[0] - 1] += (x[1] if x[1] != None else 1)
    return(y_coords)
    
  def mutation_distribution_fraction_of_cons(self,cons):
    y_coords = self.mutation_distribution_of_cons(cons)
    my_size = self.total_sequence_sum_of_cons(cons)
    y_coords = [float(x) / my_size for x in y_coords]
    return(y_coords)  
    
  def longest_consensus_length(self):
    return(len(self.longest_consensus().sequence_string))
    #field_key = 'sequence_string'
    #return len(max(self.sequence_set.values(field_key), key=lambda foo: foo[field_key])[field_key])
  def mutation_distribution(self):
    field_key = 'sequence_string'
    #doesn't make sense for varying consensus length
    #self.longest_consensus()
    longest_cons = self.longest_consensus().sequence_string#max(self.sequence_set.values(field_key), key=lambda foo: foo[field_key])[field_key]
    y_coords = [0]*(len(longest_cons))
    
    #There's an N+1 query here that should be optimized somehow
    #so far i've got x = sequence_set.prefetch_related('mutation_set').all()
    #map(lambda foo:foo['mutation__nt_position'],x.values_list('mutation__nt_position', 'frequency')))
    seqs = self.sequence_set.prefetch_related('mutation_set').all()
    mut_tuples = seqs.values_list('mutation__nt_position', 'frequency')
    for x in mut_tuples:
      if x[0] != None:
        y_coords[x[0] - 1] += (x[1] if x[1] != None else 1)
    return(y_coords)
    
  def rs_mutation_distribution(self):
    field_key = 'sequence_string'
    #doesn't make sense for varying consensus length
    #self.longest_consensus()
    longest_cons = self.longest_consensus().sequence_string#max(self.sequence_set.values(field_key), key=lambda foo: foo[field_key])[field_key]
    y_coords = [0]*(len(longest_cons))
    
    #There's an N+1 query here that should be optimized somehow
    #so far i've got x = sequence_set.prefetch_related('mutation_set').all()
    #map(lambda foo:foo['mutation__nt_position'],x.values_list('mutation__nt_position', 'frequency')))
    seqs = self.sequence_set.prefetch_related('mutation_set').filter(mutation__r_or_s='R').all()
    mut_tuples = seqs.values_list('mutation__nt_position', 'frequency')
    for x in mut_tuples:
      if x[0] != None:
        y_coords[x[0] - 1] += (x[1] if x[1] != None else 1)
    return(y_coords)
    
  def mutation_distribution_fraction(self):
    y_coords = self.mutation_distribution()
    my_size = self.total_sequence_sum()
    y_coords = [float(x) / my_size for x in y_coords]
    return(y_coords)
    
  def all_mutations(self):
    return Mutation.objects.filter(dataset=self)

  def mutation_type_count(self):
    '''
    output: a dict eg {'AG':4, 'AT':9} for 4 A to G mutations, 9 A to T, factoring in dupcount
    '''
    am = self.all_mutations()
    mut_list = {}
    mut_list['N'] = 0
    for fromnt in nucleotides:
      mut_list[fromnt] = 0
      for tont in list(set(nucleotides) - set([fromnt])):
        totalnt = fromnt+tont
        filtered_muts = am.filter(mutation_from=fromnt).filter(mutation_to=tont)
        if(len(filtered_muts) > 0):
          #note: .only(___) speeds up the query by ensuring we only query against the relevant parts.
          #see http://www.yilmazhuseyin.com/blog/dev/django-orm-performance-tips-part-2/
          stuff_count = map(lambda foo:foo['sequence__frequency'], filtered_muts.values('sequence__frequency'))
          stuff_count = map(lambda foo: foo if foo else 1, stuff_count)
          mut_list[totalnt] = sum(stuff_count)
          mut_list['N'] += sum(stuff_count)
          mut_list[fromnt] += sum(stuff_count)
    return(mut_list)
    
  def mutation_type_pct(self):
    '''
    output: a dict eg {'A': 0.03, 'G': 0.09, 'N': 0.15} (N represents total)
    Note: AT e.g. is A to T, divided by # A
    Note: A is A mutations, divided by N
    Note: N is All # mutations, divided by all possible mutations.
    '''
    mut_count = self.mutation_type_count()
    total_nts = self.possible_mutations()
    total_n = 0
    output_dict = {'A':0.0, 'C':0.0,'G':0.0,'T':0.0,'N':0.0}
    for fromnt in nucleotides:
      total_str = 'total'+fromnt
      total_n += total_nts.get(total_str)
      for tont in list(set(nucleotides) - set([fromnt])):
        totalnt = fromnt+tont
        if(mut_count.get(totalnt)):
          output_dict[totalnt] = mut_count[totalnt]
          if total_nts[fromnt] > 0:
            output_dict[totalnt] /= float(mut_count[fromnt])
          output_dict[fromnt] += mut_count[totalnt]
          output_dict['N'] += mut_count[totalnt]
    #normalize sum of C, G, etc mutations to divide by total mutations
    for fromnt in nucleotides:
      output_dict[fromnt] /= output_dict['N']
    output_dict['N'] /= float(total_n)
    return(output_dict)

  def mutation_dict_to_js(self, mutation_count):
    js_string = ""
    js_string += "["
    for fromnt in nucleotides:
      base_string = "{\"key\":\"" + fromnt + "\" "
      for tont in nucleotides:
        totalnt = fromnt+tont
        
        if mutation_count.get(totalnt):
          base_ct = mutation_count[totalnt]
        else:
          base_ct = 0
        
        base_string += ", \"" + tont + "\": " + str(base_ct)
        #inject friendly, watch out for this. I think it should be secure since everything is a constant
      js_string += base_string + "}"
      if fromnt != 'T': #hack
        js_string += ","
    js_string += "]"
    return(js_string)
    
  def motif_mutation_dict_to_js(self, mutation_count):
    js_string = ""
    js_string += "["
    motifs = map(lambda foo: foo.full_motif(), Hotspot.objects.all())
    for my_motif in motifs:
      base_string = "{\"key\":\"" + my_motif + "\" "
      for tont in nucleotides:
        if mutation_count.get(my_motif) and mutation_count[my_motif].get(tont):
          base_ct = mutation_count[my_motif].get(tont)
        else:
          base_ct = 0
        base_string += ", \"" + tont + "\": " + str(base_ct)
        #inject friendly, watch out for this. I think it should be secure since everything is a constant
      js_string += base_string + "}"
      if my_motif != 'Ga': #hack
        js_string += ","
    js_string += "]"
    return(js_string)
  def js_friendly_mutation_dict(self):
    mutation_count = self.mutation_type_count() #{'GT': 1L, 'CA': 307L, 'CG': 5807L, 'GA': 6091L, 'CT': 7996L, 'TA': 1L}
    return(self.mutation_dict_to_js(mutation_count))
    
  def js_friendly_unique_mutation_dict(self):
    mutation_count = self.unique_mutation_type_count() #{'GT': 1L, 'CA': 307L, 'CG': 5807L, 'GA': 6091L, 'CT': 7996L, 'TA': 1L}
    return(self.mutation_dict_to_js(mutation_count))

  def js_friendly_motif_mutation_dict(self):
    mutation_count = self.mutations_at_motifs() #{'GT': 1L, 'CA': 307L, 'CG': 5807L, 'GA': 6091L, 'CT': 7996L, 'TA': 1L}
    return(self.motif_mutation_dict_to_js(mutation_count))

  def js_friendly_unique_motif_mutation_dict(self):
    mutation_count = self.unique_mutations_at_motifs() #{'GT': 1L, 'CA': 307L, 'CG': 5807L, 'GA': 6091L, 'CT': 7996L, 'TA': 1L}
    return(self.motif_mutation_dict_to_js(mutation_count))  

  def unique_mutation_type_count(self):
    am = self.all_mutations()
    #all pcts will end in key pct. so Npct, TApct, etc
    #my reasoning is we count the number of uniquely occuring position at each consensus
    #so total % is calculated by sum of lengths of all consensus
    #still need to clarify: are percentages divided by this? or does Apct, Cpct etc refer to 
    pct_suffix = 'pct'
    mut_list = {}
    mut_list['N'] = 0
    mut_list['N' + pct_suffix] = 0.0
    possible_sum = sum(map(lambda foo: len(foo.sequence_string),self.consensuses_list()))
    for fromnt in nucleotides:
      mut_list[fromnt] = 0
      mut_list[fromnt + pct_suffix] = 0.0
      for tont in list(set(nucleotides) - set([fromnt])):
        totalnt = fromnt+tont
        filtered_muts = am.filter(mutation_from=fromnt).filter(mutation_to=tont)
        if(len(filtered_muts) > 0):
          stuff_count = map(lambda foo:foo, filtered_muts.values_list('nt_position', 'sequence__consensus_id'))
          mut_list[totalnt] = len(set(stuff_count))
          mut_list[fromnt] += len(set(stuff_count))
          mut_list['N'] += len(set(stuff_count))
          
          
          mut_list[totalnt+pct_suffix] = float(len(set(stuff_count)))
          mut_list[fromnt + pct_suffix] += len(set(stuff_count))
          mut_list['N'+ pct_suffix] += len(set(stuff_count))
      if(mut_list[fromnt] > 0):
        for tont in list(set(nucleotides) - set([fromnt])):
          totalnt = fromnt+tont
          if(mut_list.get(totalnt+pct_suffix)):
            mut_list[totalnt+pct_suffix] /= mut_list[fromnt]
    for fromnt in nucleotides:
      mut_list[fromnt+pct_suffix] /= mut_list['N']
    mut_list['N'+pct_suffix] /= possible_sum 
    return(mut_list)
    #
    
  def non_mutation_type_count(self):
    pass
  
  def mutation_type_frac(self):
    am_len = len(self.all_mutations())
    counts = self.mutation_type_count()
    for cnt in counts:
      counts[cnt] = Fraction(counts[cnt], am_len)
    return(counts)
    
  def media_path(self):
    corepath = os.path.join(settings.MEDIA_ROOT, self.session.key)
    if not os.path.exists(corepath):
      os.makedirs(corepath)
      os.chmod(corepath, 0777)
    newpath = os.path.join(corepath,str(self.id))    
    if not os.path.exists(newpath):
      os.makedirs(newpath)
      os.chmod(newpath, 0777)
    return(newpath)

  def querybuilder_fasta_export_url(self):
    return(self.common_image_url(self.querybuilder_default_file))
    

  def common_root_path(self, file_name):
    return(os.path.join(self.media_path(), file_name))
  
  def common_image_url(self, file_name):
    return(settings.HACKISH_IMAGE_ROOT + settings.MEDIA_URL + self.session.key+"/"+str(self.id)+"/"+file_name)
  
  def mutation_histogram_image_path(self):
    return(self.common_root_path(self.mutation_distribution_name))
  
  def mutation_histogram_image_url(self):
    return(self.common_image_url(self.mutation_distribution_name))
  
  def mutation_histogram(self):
    
    #do the following for specific datasets (not virtual datasets)
    #but it will return 1 row for each sequence without a mutation :c
    #cursor = connection.cursor
    #cursor.execute("select count(*) as freq from sequence s left join mutation m on s.id = m.sequence_id where s.dataset_id = %s group by s.id", [self.id])
    #count_list = dictfetchall(cursor)
    #map(lambda foo: foo['freq'], count_list)
    #return(map(lambda foo: foo.mutation_set.count(), self.sequence_set.all()))
    return(map(lambda foo: foo.mutation__count, self.sequence_set.prefetch_related('mutation_set').annotate(Count('mutation'))))

  def mutation_per_sequence_statistics(self):
    seqs_with_muts = self.sequence_set.prefetch_related('mutation_set')
    a = (map(lambda foo: [foo.mutation__count, foo.norm_frequency()],seqs_with_muts.annotate(Count('mutation'))))
    a2 = map(lambda foo: foo[0], a)
    counter = collections.Counter(a2)
    x_coords = range(max(counter.keys())+1)
    y_coords = [0]*len(x_coords)
    for foo in a:
      y_coords[foo[0]] += foo[1]
    return([x_coords,y_coords])

  def export_plot(self):
    max_limit = 20
    mutation_stats = self.mutation_per_sequence_statistics()
    #TODO: normalize this with mutation cnt
    x_coords, y_coords = mutation_stats[0], mutation_stats[1]
   
    if not (os.path.isfile(self.mutation_histogram_image_path()) ):
      p = plotters.Plotter().line_plot(x_coords, y_coords,self.mutation_histogram_image_path())
    if(len(mutation_stats[0]) > max_limit+1):
      mutation_stats[0] = range(max_limit+1)
      #for i in range(max_limit):
      #  mutation_stats[1].append(tempdict[i])
      mutation_stats[1][max_limit] = sum(map(lambda foo: mutation_stats[1][foo], range(max_limit, len(mutation_stats[1]))))
      mutation_stats[1] = mutation_stats[1][0:(max_limit+1)]
    return(mutation_stats)

class Dataset(models.Model, DatasetAnalyzable):

  #map(lambda foo: foo.mutation_set.count(), d1.sequence_set.all())
  
  ######################
  session = models.ForeignKey(Session)
  publication = models.TextField()
  short_description = models.CharField(max_length=100)
  ######################
  
  lirong_plot_name = "hotspotPlot.png"
  group_plot_name = "groupPlot.png"
  group_trees_plot_name = "groupTreesPlot.png"
  mutation_distribution_name = "my_line_plot.png"
  querybuilder_default_file = "querybuilder_results.fasta"
  cluster_fasta_default_file = "clusters.csv"
  
  class Meta:
    db_table = "dataset"
  
  def set_id(self):
    return(str(self.id))
  def mutations_at_motifs(self):
    hotspots = Hotspot.objects.all()
    mut_list = {}
    cursor = connection.cursor()
    start = time.time()
    #This is a pretty bad query that involves joining of two tables. If you explain this squery it will show that it doesn't rely entirely on indexes :(
    cursor.execute(" select frequency as number, hc.hotspot_id as id, m.mutation_from, m.mutation_to, sequence.consensus_id from sequence left join mutation m on sequence.id = m.sequence_id left join consensus_hotspot hc on sequence.consensus_id = hc.consensus_id where sequence.dataset_id = %s and m.nt_position = hc.nt_position group by m.id, hc.hotspot_id, sequence.consensus_id;", [self.id])
    total_list = dictfetchall(cursor)
    #raise Exception()
    #the following should also be optimized somehow; it can actually be much longer to execute than the sql before it
    hs_dict = {}
    for hs in Hotspot.objects.all():
      hs_dict[hs.id] = hs.full_motif()
    for mutations_dict in total_list:
      hs_id = mutations_dict['id']
      mutations_dict['number'] = 1 if mutations_dict['number'] == None else mutations_dict['number']
      freq = mutations_dict['number']
      freq = 1 if freq == None else freq
      motif_key = hs_dict[hs_id]
      if motif_key in mut_list:
        if mut_list[motif_key].get(mutations_dict['mutation_to']):
          mut_list[motif_key][mutations_dict['mutation_to']] += mutations_dict['number']
        else:
          mut_list[motif_key][mutations_dict['mutation_to']] = mutations_dict['number']
      else:
        mut_list[motif_key] = {}#{'A':0,'C':0,'G':0,'T':0}
        mut_list[motif_key][mutations_dict['mutation_to']] = mutations_dict['number']#*freq
    return(mut_list)
    
  def unique_mutations_at_motifs(self):
    hotspots = Hotspot.objects.all()
    mut_list = {}
    
    quick_hs_lookup_dict = {}
    
    cursor = connection.cursor()
    #cursor.execute("select * from (select frequency as number, hc.hotspot_id as id, m.nt_position, m.mutation_from, m.mutation_to from sequence left join mutation m on sequence.id = m.sequence_id left join consensus_hotspot hc on sequence.consensus_id = hc.consensus_id where sequence.dataset_id = %s and m.nt_position = hc.nt_position group by m.id, hc.hotspot_id) as poo group by nt_position, mutation_from, mutation_to, id;", [self.id])
    #cursor.execute(" select frequency as number, hc.hotspot_id as id, m.mutation_from, m.mutation_to from sequence left join mutation m on sequence.id = m.sequence_id left join consensus_hotspot hc on sequence.consensus_id = hc.consensus_id where sequence.dataset_id = %s and m.nt_position = hc.nt_position group by m.id, hc.hotspot_id;", [self.id])
    #print "select count(*), mutation_from, mutation_to, nt_position, id, consensus_id from (select frequency as number, hc.hotspot_id as id, m.mutation_from, m.mutation_to, m.nt_position, sequence.consensus_id as consensus_id from sequence left join mutation m on sequence.id = m.sequence_id left join consensus_hotspot hc on sequence.consensus_id = hc.consensus_id where sequence.dataset_id = 3 and m.nt_position = hc.nt_position group by m.id, m.nt_position, hc.hotspot_id, sequence.consensus_id) as poo group by nt_position, mutation_from, mutation_to, id, consensus_id;"
    cursor.execute("select count(*), mutation_from, mutation_to, nt_position, id, consensus_id from (select frequency as number, hc.hotspot_id as id, m.mutation_from, m.mutation_to, m.nt_position, sequence.consensus_id as consensus_id from sequence left join mutation m on sequence.id = m.sequence_id left join consensus_hotspot hc on sequence.consensus_id = hc.consensus_id where sequence.dataset_id = %s and m.nt_position = hc.nt_position group by m.id, m.nt_position, hc.hotspot_id, sequence.consensus_id) as poo group by nt_position, mutation_from, mutation_to, id, consensus_id;", [self.id])
    total_list = dictfetchall(cursor)
    #print total_list
    for mutations_dict in total_list:
      hs_id = mutations_dict['id']
      if quick_hs_lookup_dict.get(hs_id):
        motif_key = quick_hs_lookup_dict[hs_id]
      else:
        hs_motif = hotspots.filter(id=hs_id)[:1].get().full_motif()
        motif_key = hs_motif
        quick_hs_lookup_dict[hs_id] = hs_motif
      if motif_key in mut_list:
        if not mutations_dict['mutation_to'] in mut_list[motif_key]:
          mut_list[motif_key][mutations_dict['mutation_to']] = 1
        else:
          mut_list[motif_key][mutations_dict['mutation_to']] += 1
      else:
        mut_list[motif_key] = {}
        mut_list[motif_key][mutations_dict['mutation_to']] = 1
    return(mut_list)
  def export_clusters(self):
    csv_filename = os.path.join(self.media_path(), 'clusters.csv')
    with open(csv_filename, 'wb') as csvfile:
      spamwriter = csv.writer(csvfile)
      spamwriter.writerow(['CDR3.IMGT','V.GENE.and.allele','J.GENE.and.allele','Cluster'])

      for seq in self.sequence_set.all():
        spamwriter.writerow([seq.imgt_cdr3_string,seq.imgt_v_gene(),seq.imgt_j_gene(),seq.cluster_group])
  def cluster_csv_export_url(self):
    return(self.common_image_url('clusters.csv'))
    
  def create_mutations(self):
    mut_list = []
    stuff = self.sequence_set.prefetch_related('consensus')
    for seq in stuff:
      mut_list += seq.find_associated_mutations(seq.consensus.sequence_string,seq.cdr3_start_index)
    Mutation.objects.bulk_create(mut_list,batch_size=999)

    #seq.add_mutations()
  
class Consensus(models.Model):
  accession_number = models.CharField(max_length=500, db_index=True)
  consensus_group = models.CharField(max_length=45)
  consensus_subgroup = models.CharField(max_length=45)
  short_description = models.CharField(max_length=500)
  allele = models.CharField(max_length=45)
  gene = models.CharField(max_length=45)
  species = models.CharField(max_length=500)
  gapped = models.BooleanField(default=False)
  sequence_string = models.TextField()
  v_region = models.TextField()
  
  def identify_hotspots(self):
    return_hash = {}
    for hotspot in Hotspot.objects.all():
      full_motif = hotspot.full_motif()
      left_offset = len(hotspot.lcontext)
      coords = SeqUtils.nt_search(self.sequence_string,full_motif.upper())
      return_hash[hotspot] = [x+ 1 + left_offset for x in coords[1:]]# + 1 + left_offset
    return(return_hash)
  
  def identify_rc_hotspots(self):
    return_hash = {}
    for hotspot in Hotspot.objects.all():
      full_motif = str(Seq.Seq(hotspot.full_motif()).reverse_complement())
      right_offset = len(hotspot.rcontext)
      coords = SeqUtils.nt_search(self.sequence_string,full_motif.upper())
      return_hash[hotspot] = [x+ 1 + right_offset for x in coords[1:]]# + 1 + left_offset
    return(return_hash)
    
  def write_hotspots_to_table(self):
    forward_spots = self.identify_hotspots()
    reverse_spots = self.identify_rc_hotspots()
    
    HotspotConsensus.objects.all().filter(consensus=self).delete()
    for forward_spot in forward_spots:
      for index in forward_spots[forward_spot]:
        hc = HotspotConsensus(forward_orientation=True,consensus=self,hotspot=forward_spot,nt_position=index,position_start=index-len(forward_spot.lcontext),position_end=index+len(forward_spot.rcontext))
        hc.save()

  def hotspot_total_coordinates(self):
    spots_dict = {}
    hset = Hotspot.objects.all()
    hcset = self.hotspotconsensus_set
    for hs in hset:
      spots_dict[hs.full_motif()] = map(lambda foo: foo.nt_position, hcset.filter(hotspot=hs))
    return(spots_dict)
    
  def js_friendly_hotspot_total_coordinates(self):
    return(json.dumps(self.hotspot_total_coordinates()))
    

  class Meta:
    db_table = "consensus"

class Group(models.Model):
  dataset = models.ForeignKey(Dataset)
  class Meta:
    db_table = "data_group" #Not group because group is a reserved mysql word

class Sequence(models.Model):
  
  ######################
  dataset = models.ForeignKey(Dataset)
  consensus = models.ForeignKey(Consensus)
  cluster_group = models.IntegerField(blank=True,null=True)
  group = models.ForeignKey(Group, db_index=True, blank=True, null=True)
  accession_number = models.CharField(max_length=500, db_index=True)
  gapped = models.BooleanField(default=False)
  prod_or_nonprod = models.BooleanField(default=False)
  sequence_string = models.TextField()
  v_region = models.TextField()
  consensus_offset_start = models.IntegerField(blank=True, null=True) #ie if sequence is ...acg... offset start is 4 end is 6
  consensus_offset_end = models.IntegerField(blank=True, null=True)
  extra_notes = models.CharField(max_length=1000)
  frequency = models.IntegerField(blank=True,null=True) #DUPCOUNT from FASTA header
  cdr3_frameshift = models.BooleanField(blank=True,default=False) #for nonproductive sequences that were nonproductive due to a frameshift

  conscount = models.IntegerField(blank=True,null=True)
  cell_type = models.CharField(max_length=5,blank=True,null=True)
  # the next are probably imgt specific
  j_consensus = models.ForeignKey(Consensus,blank=True,null=True,related_name='j_consensus')#for IMGT J field; ok to leave blank
  imgt_cdr3_string = models.TextField(blank=True,null=True) #This is ungapped! For clustering purposes
  v_string = models.TextField(blank=True,null=True)#the bases that correspond directly to the consensus, minus indels. Has gaps.
  cdr3_start_index = models.IntegerField(blank=True,null=True)#position (n+1) of where cdr3 is believed to start, relative to "v_string"
  #0 of cdr3 start index means no cdr3 could be found. Sorry for magic value!
  
  
  
  ########################
  
  def find_associated_mutations(self,con_str=None,max_index=None):
    '''
    input: optional consensus string to save a DB lookup, and optional max index so mutations in cdr3 are not added
    output: an array of mutation objects
    '''
    
    to_save = []
    if self.v_string != None:
      seq_str = self.v_string
    else:
      seq_str = self.sequence_string
    
    if con_str == None:
      consensus_object = self.consensus
      con_str = consensus_object.sequence_string

    mismatch_array = sequence_compare(seq_str,con_str)
    str_temp = ''

    con_split_list = list(splitIterator(con_str, 3))
    seq_split_list = list(splitIterator(seq_str, 3))
    mutations_to_str = []
    for each in mismatch_array:
      mutations_to_str.append(seq_str[each])

    offset_to_remove = [i for i,x in enumerate(mutations_to_str) if x == '.']
    mismatch_array  = list(set(mismatch_array) - set(offset_to_remove))
    mismatch_array.sort()
    if(max_index != None and max_index > 0):
      mismatch_array = filter(lambda foo: foo + 1 <= max_index, mismatch_array)
    
    for each in mismatch_array:
      nt_position = each + 1
      mutation_from = con_str[each]
      mutation_to = seq_str[each]

      lcontext = left_2_nt(each,con_str)
      rcontext = right_2_nt(each,con_str)

      if (con_str[each] == 'A' and seq_str[each] == 'G') or (con_str[each] == 'G' and seq_str[each] == 'A') or (con_str[each] == 'C' and seq_str[each] == 'T') or (con_str[each] == 'T' and seq_str[each] == 'C'):
      	transversion = 0
      else:
      	transversion = 1
      germ_codon = con_split_list[each/3]
      seq_codon = seq_split_list[each/3]
      aa_position = (each/3) + 1
      if (len(germ_codon) < 3) or (len(seq_codon) < 3) or (codons.has_key(seq_split_list[each/3]) == False) or (codons.has_key(con_split_list[each/3]) == False):
        germ_codon = seq_codon = ''
        r_or_s = aa_from = aa_to = ''
      else:
      	temp_seq_str = list(con_str)
      	temp_seq_str[each] = seq_str[each]
      	temp_seq_str = "".join(temp_seq_str)
      	temp_seq_str_list = list(splitIterator(temp_seq_str, 3))
      	aa_from = codons[con_split_list[each/3]]
      	aa_to = codons[temp_seq_str_list[each/3]]
      	if (aa_from == aa_to):
      		r_or_s = 'S'
      	else:
      		r_or_s = 'R'
      my_mutation = Mutation(dataset=self.dataset,sequence=self,accession_number=self.accession_number,nt_position=nt_position,mutation_from=str(mutation_from),mutation_to=str(mutation_to),lcontext=str(lcontext),rcontext=str(rcontext),transversion=transversion)

      # NOTE(srmeier): check to make sure the 'mutation_to' and 'mutation_from' below to 'nucleotides'
      mutation_to_isvalid = False;
      mutation_from_isvalid = False;
      for base in nucleotides:
        if mutation_to == base:
          mutation_to_isvalid = True;
        if mutation_from == base:
          mutation_from_isvalid = True;

      # NOTE(srmeier): if the mutation to and from are both valid then count the change as a mutation
      if mutation_to_isvalid and mutation_from_isvalid:
        if (aa_position != -1):
          my_mutation.r_or_s = r_or_s
          my_mutation.germ_codon = str(germ_codon)
          my_mutation.seq_codon = str(seq_codon)
          my_mutation.amino_acid_from = aa_from
          my_mutation.amino_acid_to = aa_to
        to_save.append(my_mutation)

    return to_save
  
  def add_mutations(self):
    if self.v_string != None:
      seq_str = self.v_string
    else:
      seq_str = self.sequence_string
      
    consensus_offset_start = re.search("[A-Za-z]", seq_str).start() + 1
    consensus_offset_end = len(seq_str) - re.search("[A-Za-z]", seq_str[::-1]).start()
    self.consensus_offset_start = consensus_offset_start
    self.consensus_offset_end = consensus_offset_end
    self.save()
    for mut in self.find_associated_mutations(self.consensus.sequence_string):
      mut.save()
        
  def add_indels(self):
    pass
  
  def cigar_string(self):
    #placeholder
    return((str(len(self.consensus.sequence_string)))+"M")
  
  def norm_frequency(self):
    #in case sequences are uploaded WITHOUT dupcount
    if self.frequency == None:
      return 1
    else:
      return self.frequency
  
  def mutation_position_distribution(self):
    return(map(lambda foo: int(foo['nt_position']),self.mutation_set.all().values('nt_position')))
  
  def rs_mutation_position_distribution(self):
    return(map(lambda foo: int(foo['nt_position']),self.mutation_set.filter(r_or_s='R').all().values('nt_position')))
  
  def imgt_j_gene(self):
    if self.j_consensus == None:
      return('none')
    else:
      j_gene = self.j_consensus.allele
      return(j_gene)
    
  def imgt_v_gene(self):
    v_gene = self.consensus.allele
    return(v_gene)

  class Meta:
    db_table = "sequence"

class Mutation(models.Model):
  dataset = models.ForeignKey(Dataset)
  sequence = models.ForeignKey(Sequence)
  accession_number = models.CharField(max_length=500)
  nt_position = models.IntegerField()
  mutation_from = models.CharField(max_length=1)
  mutation_to = models.CharField(max_length=1)
  amino_acid_from = models.CharField(max_length=1)
  amino_acid_to = models.CharField(max_length=1)
  r_or_s = models.CharField(max_length=3)
  germ_codon = models.CharField(max_length=3)
  seq_codon = models.CharField(max_length=3)
  aa_position = models.IntegerField(blank=True, null=True)
  lcontext = models.CharField(max_length=3)
  rcontext = models.CharField(max_length=3)
  transversion = models.BooleanField(default=False)
  class Meta:
    db_table = "mutation"
    
class Indel(models.Model):
  sequence = models.ForeignKey(Sequence)
  insertion_or_del = models.BooleanField(default=True) #by convention, True = insertion, False = del
  indel_begin = models.IntegerField()
  indel_end = models.IntegerField()
  inserted_sequence = models.CharField(max_length=500)
  
  class Meta:
    db_table = "indel"
    
class Hotspot(models.Model):
  #conventionally should be stored in forward orientation. If storing TC/GA, etc. store as TC, not GA.
  hotspot_target = models.CharField(max_length=1)
  lcontext = models.CharField(max_length=3)
  rcontext = models.CharField(max_length=3)
  full_sequence = models.CharField(max_length=7)
  
  def full_motif(self):
    return(self.lcontext.lower()+self.hotspot_target.upper()+self.rcontext.lower())
    
  class Meta:
    db_table = "hotspot"
  
class HotspotConsensus(models.Model):
  consensus = models.ForeignKey(Consensus)
  hotspot = models.ForeignKey(Hotspot)
  forward_orientation = models.BooleanField(default=True)
  nt_position = models.IntegerField()
  position_start = models.IntegerField()
  position_end = models.IntegerField()
  
  class Meta:
    db_table = "consensus_hotspot"

class IlluminaData(models.Model):
  sequence = models.ForeignKey(Sequence)
  instrument_name = models.CharField(max_length=500)
  run_id = models.CharField(max_length=45)
  flowcell_id = models.CharField(max_length=45)
  flowcell_lane = models.CharField(max_length=45)
  tile_number = models.IntegerField()
  x_coordinate = models.IntegerField()
  y_coordinate = models.IntegerField()
  member_of_pair = models.BooleanField(default=False)
  read_fails_filter = models.CharField(max_length=1)
  control_bit = models.IntegerField()
  index_sequence = models.CharField(max_length=45)
  class Meta:
    db_table = "illumina_data"
    
class VirtualDataset(models.Model, DatasetAnalyzable):
  ######################
  session = models.ForeignKey(Session)
  short_description = models.CharField(max_length=100)
  sequence_set = models.ManyToManyField(Sequence, through='VirtualDatasetSequence')
  ######################
  
  lirong_plot_name = "vhotspotPlot.png"
  mutation_distribution_name = "vmy_line_plot.png"
  querybuilder_default_file = "vquerybuilder_results.fasta"
  
  def all_mutations(self):
    return Mutation.objects.filter(sequence__in=self.sequence_set.all())

  def set_id(self):
    return('v' + str(self.id))
  def mutations_at_motifs(self):
    hotspots = Hotspot.objects.all()
    mut_list = {}
    
    cursor = connection.cursor()
    #TODO: This query is SLOOOOOOW! There may be a fix to make it faster but I have no clue. Look into EXPLAIN
    
    cursor.execute(" select seq.frequency as number, hc.hotspot_id as id, m.mutation_from, m.mutation_to, seq.consensus_id from sequence seq left join shmserver_virtualdatasetsequence vds on seq.id = vds.sequence_id left join mutation m on seq.id = m.sequence_id left join consensus_hotspot hc on seq.consensus_id = hc.consensus_id where virtual_dataset_id = %s and m.nt_position = hc.nt_position group by m.id, seq.consensus_id, hc.hotspot_id;", [self.id])
    total_list = dictfetchall(cursor)
    for mutations_dict in total_list:
      hs_id = mutations_dict['id']
      mutations_dict['number'] = 1 if mutations_dict['number'] == None else mutations_dict['number']
      freq = mutations_dict['number']
      freq = 1 if freq == None else freq
      #freq = 1 if mutations_dict['frequency'] == None else mutations_dict['frequency']
      motif_key = hotspots.filter(id=hs_id)[:1].get().full_motif()
      if motif_key in mut_list:
        if mut_list[motif_key].get(mutations_dict['mutation_to']):
          mut_list[motif_key][mutations_dict['mutation_to']] += mutations_dict['number']#*freq
        else:
          mut_list[motif_key][mutations_dict['mutation_to']] = mutations_dict['number']
      else:
        mut_list[motif_key] = {}
        mut_list[motif_key][mutations_dict['mutation_to']] = mutations_dict['number']#*freq
    return(mut_list)
  def unique_mutations_at_motifs(self):
    hotspots = Hotspot.objects.all()
    mut_list = {}
    
    cursor = connection.cursor()
    cursor.execute("select a.id, a.mutation_to, a.mutation_from, a.nt_position, count(*) as number from (select count(*) as number, hc.hotspot_id as id, m.nt_position, m.mutation_from, m.mutation_to from sequence seq left join shmserver_virtualdatasetsequence vds on seq.id = vds.sequence_id left join mutation m on seq.id = m.sequence_id left join consensus_hotspot hc on seq.consensus_id = hc.consensus_id where virtual_dataset_id = %s and m.nt_position = hc.nt_position group by  m.nt_position, m.mutation_from, m.mutation_to, hc.hotspot_id)a group by a.id, a.mutation_to, a.mutation_from;", [ self.id])
    total_list = dictfetchall(cursor)
    
    for mutations_dict in total_list:
      hs_id = mutations_dict['id']
      motif_key = hotspots.filter(id=hs_id)[:1].get().full_motif()
      if motif_key in mut_list:
        mut_list[motif_key][mutations_dict['mutation_to']] = mutations_dict['number']
      else:
        mut_list[motif_key] = {}
        mut_list[motif_key][mutations_dict['mutation_to']] = mutations_dict['number']
    return(mut_list)

  
class VirtualDatasetSequence(models.Model):
  ######################
  sequence = models.ForeignKey(Sequence)
  virtual_dataset = models.ForeignKey(VirtualDataset)
  cluster_group = models.IntegerField(blank=True,null=True)
  ######################

