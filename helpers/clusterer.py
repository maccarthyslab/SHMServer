import scipy.cluster.hierarchy as sch
from joblib import Parallel, delayed
import multiprocessing as mp
import numpy as np
from itertools import groupby, combinations
from pathos.multiprocessing import ProcessingPool
import editdistance


def thing(string_i,string_j):
  return(editdistance.eval(string_i, string_j))

class Clusterer:
  def __init__(self,string_records):
    '''
    '''
    self.django_records = string_records
    self.record_size = len(string_records)
    self.d_mat = np.zeros((self.record_size,self.record_size))
    self.pool = ProcessingPool(4)

  def assign_clusters(self):
    num_cores = mp.cpu_count()
    cutoff=0.8
    #seqs = self.sequence_set.prefetch_related('consensus').all()
    groups = {}
    for record in self.django_records:
      if groups.get(record.imgt_v_gene() + record.imgt_j_gene()) == None:
        groups[record.imgt_v_gene() + record.imgt_j_gene()] = [record]
      else:
        groups[record.imgt_v_gene() + record.imgt_j_gene()].append(record)
    
    basegroup = 0
    totalgroups = 0
    #record_size = len(seqs)
    #d_mat = np.zeros((record_size,record_size))
    
    #blahblah = Parallel(n_jobs=8)(delayed(thing)(self.django_records[i].imgt_cdr3_string, self.django_records[j].imgt_cdr3_string) for i, j in combinations(range(self.record_size),2))
    print(blahblah)
    #for j,k in combinations(range(record_size), 2):
    #  d_mat[j,k] = d_mat[k,j] = editdistance.eval(seqs[k].sequence_string, seqs[j].sequence_string)
    my_l = sch.linkage(self.d_mat,'ward')
    ind = sch.fcluster(my_l, cutoff*self.d_mat.max(), 'distance')