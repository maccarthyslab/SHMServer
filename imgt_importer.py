#from shmserver.models import Consensus, Dataset, Sequence, Mutation
import shmserver.models
import re
import pdb

imgt_species_abbreviations = {"Musmus":"Mus", "Homsap":"Homo sapiens"}

class ImgtImporter:
  cons_dict = {}
  lines = []
  fasta_records = None

  def __init__(self, input_lines, list_records_fasta=None):
    self.lines = input_lines
    self.fasta_records = list_records_fasta
  
  def tags_from_annotation(self,anno_string): #duplicated in models.py ; beware
    #input: a string like 'DUPCOUNT=14|CIGAR=14M'
    #output: a dict as such: {'DUPCOUNT':14,'CIGAR':'14M'}
    annotations_list = {}
    tags = re.split("[ |_]", anno_string)
    for tag in tags:
      if re.search(r'^\w+=', tag):
        val1, val2 = tag.split("=",2)
        annotations_list[val1] = val2
    return(annotations_list)
    
  def grab_cons_from_name(self,temp):
    '''
    input: an IMGT gene name such as Musmus IGHV1-72*01 F
    output: the Django Consensus object whose name and species matches up
    '''
    con_obj = None
    if (len(temp) >= 2): #eg Musmus IGHV1-72*01 F (F is there for some reason)
      con_species = temp[0].strip()
      con_allele = temp[1].strip()
      if self.cons_dict.get(con_allele):
        return(self.cons_dict[con_allele])
      #sometimes imgt will put in "Less than 6 nucleotides are aligned" instead of a V.
      #this is a bad workaround for now. Sorry!
      if con_allele != "than":
        full_con_species = imgt_species_abbreviations.get(con_species)
        filtered_cons = shmserver.models.Consensus.objects.filter(allele=con_allele).filter(species__startswith=full_con_species)
  
        if(len(filtered_cons) > 0):
          con_obj = filtered_cons[0] #TODO: species on consensus needs to match
          self.cons_dict[con_allele] = con_obj
    return(con_obj)
  
  def grab_imgt_info(self, unique_strings, gapped_file_list, sequence_str):
    '''
    input: a dict to be updated and an imgt file row (called gapped_file_list), and the sequence string
    output: a hash of information such as count, productive, frameshift, etc corresponding to input seq
    '''
    if self.fasta_records is not None:
      annotations = self.tags_from_annotation(self.fasta_records[(int(gapped_file_list[0])-1)].id)
    else:
      annotations = self.tags_from_annotation(gapped_file_list[1])
    
    if (unique_strings.get(sequence_str) == None):#unique_strings.get(sequence_str):
      #For now, this assumes that there's a 1:1 mapping between Sequence id (imgt_seqnum) to VDJ sequence.
      #If there are multiple sequence ids to a given sequence, the first imgt_seqnum will be used and dupcount/frequency will be incremented.
      output_dict = {'count': 0, 'imgt_seqnum': []}

      # NOTE(srmeier): set these here so that we don't have to depend on the "unique_string" elsewhere
      # because "unique_string" could change in the future depending on how we define unique sequences
      output_dict['sequence_string'] = re.sub('\.', '', gapped_file_list[6].upper())
      output_dict['v_string'] = gapped_file_list[6].upper()

      prod_or_not = (gapped_file_list[2].startswith('productive'))
      output_dict['prod_or_not'] = prod_or_not
      
      #conscount
      if annotations.get('CONSCOUNT'):
        output_dict['conscount'] = annotations['CONSCOUNT']
      else:
        output_dict['conscount'] = None
      
      if annotations.get('CELLTYPE'):
        output_dict['celltype'] = annotations['CELLTYPE']
      else:
        output_dict['celltype'] = None

      if annotations.get('CLONE'):
        output_dict['cluster_group'] = annotations['CLONE']
      else:
        output_dict['cluster_group'] = None
      
      cdr3_string = gapped_file_list[14].upper()
      cdr3_string = re.sub('\.', '', cdr3_string)
      cdr3_len = len(cdr3_string)
  
      output_dict['cdr3_frameshift'] = (cdr3_len % 3 != 0 and prod_or_not == False)
  
      temp = gapped_file_list[3].split() #tokenize via blank space
      tempj = gapped_file_list[4].split()
  
      con_obj = self.grab_cons_from_name(temp)
      output_dict['consensus'] = con_obj
      output_dict['j_consensus'] = self.grab_cons_from_name(tempj)
  
      consensus_offset_start = re.search("[A-Za-z]", sequence_str).start() + 1
      consensus_offset_end = len(sequence_str) - re.search("[A-Za-z]", sequence_str[::-1]).start()
      
      if con_obj != None:
        v_str = con_obj.v_region
      else:
        v_str = ''
      note = "Consensus: " + gapped_file_list[3] + "; " + gapped_file_list[4] 
      output_dict['notes'] = note
  
      output_dict['cos'] = consensus_offset_start
      output_dict['v_str'] = v_str
      output_dict['cdr3_str'] = cdr3_string
      output_dict['deletions'] = []
      
      #This is confusing for me - but the basic idea is the v region alone will never have the cdr3 string
      #since the cdr3 region includes the DJ as well as intersections. Thus, the cdr3 has to be compared to
      #the sequence as a whole. It is possible that the cdr3 misses in which case the mutations might be added
      output_dict['csi'] = sequence_str.rfind(cdr3_string) + 1 #v_str.rfind(cdr3_string)?
  
      #find indels
      if(con_obj == None or (len(sequence_str) != len(con_obj.sequence_string))):
        pass
      else:
        del_indices = [(a.start(), a.end()) for a in list(re.finditer('\.+', sequence_str))]
        for del_set in del_indices:
          output_dict['deletions'].append({'begin':del_set[0]+1,'end':del_set[1], 'inserted_sequence': con_obj.sequence_string[del_set[0]:del_set[1]]})
      output_dict['coe'] = consensus_offset_end
    else:
      output_dict = unique_strings[sequence_str]
    
    #NOTE(srmeier): keep a note of all the non-unique sequence IDs in case we need to pull meta info from the FASTA file
    output_dict['imgt_seqnum'].append(int(gapped_file_list[0]));

    if(annotations.get("DUPCOUNT")):
      output_dict['count'] += int(annotations['DUPCOUNT'])
    else:
      output_dict['count'] += 1
    return(output_dict)
  
  def unique_strings(self):
    '''
    output: a dict of unique sequences as the keys (sequence strings) with corresponding info about count, etc
    from the input which was passed (an imgt table)
    ex: {'AAA':{'count':3,prod:True,..}, 'AAC':{'count':2},...} etc
    '''
    unique_strings = {}
    
    for in_line in self.lines:
      gapped_file_list = in_line.split("\t")
      if gapped_file_list[0] == "Sequence number":
        skip = 1
      elif len(gapped_file_list) == 19:
        group_id = gapped_file_list[18]
        sequence_str = gapped_file_list[6].upper()

        # NOTE(srmeier): updating hash key by including the CELLTYPE
        if self.fasta_records is not None:
          annotations = self.tags_from_annotation(self.fasta_records[(int(gapped_file_list[0])-1)].id)
        else:
          annotations = self.tags_from_annotation(gapped_file_list[1])

        # NOTE(srmeier): add the CELLTYPE annotation to the hash key
        if annotations.get('CELLTYPE'):
          #NOTE(srmeier): if we don't have a cell type then just keep the hash key the same
          sequence_str = annotations['CELLTYPE']+"__"+sequence_str

        if sequence_str == "":
          skip=1
        else:
          unique_strings[sequence_str] = self.grab_imgt_info(unique_strings, gapped_file_list, sequence_str)

    return(unique_strings)
    #pass
  
  pass
