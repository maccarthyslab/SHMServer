from django.conf import settings
import collections, itertools, os
import pyRserve, numpy

#misnomer - this is a class that interfaces python models to R
class Plotter:

  def distribute_groups(self,dataset, cutoff):
    
    conn = pyRserve.connect()
    conn.r.pwdRoot = settings.PROJECT_LOCATION_FOR_RSERVE# + "/shmserver/Rscripts"
    conn.r.scriptRoot = settings.PROJECT_LOCATION_FOR_RSERVE + "/shmserver/Rscripts/clustergroups2.R"
    
    seq_set = map(lambda foo: foo.imgt_cdr3_string, dataset.sequence_set.all())
    seq_set_v = map(lambda foo: foo.imgt_v_gene(), dataset.sequence_set.all())
    seq_set_j = map(lambda foo: foo.imgt_j_gene(), dataset.sequence_set.all())
    conn.r.cdr3s = seq_set
    conn.r.v_genes = seq_set_v
    conn.r.j_genes = seq_set_j
    conn.r.cutree_cutoff = cutoff
    conn.r.groupsFileName = dataset.groups_plot_path()
    conn.r.treesFileName = dataset.groups_trees_plot_path()
    conn.eval("setwd(pwdRoot)")
    conn.eval("my_df <- data.frame(CDR3.IMGT=unlist(cdr3s), J.GENE.and.allele=unlist(j_genes), V.GENE.and.allele=unlist(v_genes))")
    conn.eval("f <- my_df")
    conn.eval("source('shmserver/Rscripts/clustergroups2.R')")
    groups = conn.r.hc_groups
    s1 = dataset.sequence_set.all()
    for i in range(0,len(groups)):
      temp_s = s1[i] #done this way because s1[i].group_id= doesn't work
      temp_s.cluster_group = groups[i] #or group_id but that is complicated atm
      temp_s.save()
  
  def hotspots_sequence_plot(self, dataset,hotspot_list,consensus_obj=None):
    file_pos = dataset.hotspots_sequence_plot_path()
    #if not os.path.isfile(file_pos):
    conn = pyRserve.connect()
    conn.r.pwdRoot = settings.PROJECT_LOCATION_FOR_RSERVE
    conn.r.scriptRoot = settings.PROJECT_LOCATION_FOR_RSERVE+"/shmserver/Rscripts/liRongplot.R"
    
    conn.eval("setwd(pwdRoot)")
    
    conn.r.outputFilename = file_pos
    
    consensus_obj = dataset.most_frequent_consensus() if consensus_obj == None else consensus_obj
    
    #conn.r.vRegion = dataset.longest_consensus().sequence_string
    conn.r.vRegion = consensus_obj.sequence_string
    conn.r.numSequences = dataset.total_sequence_sum_of_cons(consensus_obj)#len(dataset.sequence_set.all())
    #conn.r.ntPositionCounts = dataset.mutation_distribution()
    conn.r.ntPositionCounts = dataset.mutation_distribution_of_cons(consensus_obj)
    conn.r.hotspot_list = hotspot_list
    
    conn.r.plotName = dataset.short_description + " " + consensus_obj.gene# + str()
    
    conn.eval("vMotifs <- unlist(hotspot_list)")
    conn.eval("ntPositionCounts <- unlist(ntPositionCounts)")
    
    conn.r.rsCounts = dataset.rs_mutation_distribution_of_cons(consensus_obj)
    conn.eval("rsCounts <- unlist(rsCounts)")
    conn.eval("source(scriptRoot)")
  
  def compare_plot(self, dataset1,dataset2,file_root='hi.png', consensus_obj=None):
    file_pos = dataset1.session.mutation_histogram_image_path(dataset1.id,dataset2.id)
    #if not os.path.isfile(file_pos):
    consensus_obj = dataset1.most_frequent_consensus() if consensus_obj == None else consensus_obj
    consensus_len = len(consensus_obj.sequence_string)
    conn = pyRserve.connect()
    conn.r.pwdRoot = settings.PROJECT_LOCATION_FOR_RSERVE
    conn.r.scriptRoot = settings.PROJECT_LOCATION_FOR_RSERVE+"/shmserver/Rscripts/positionAnalysis.R"
    
    #i have to do it this way because numpy concatentation is nonsensical
    conn.r.d1ele1 = range(1,consensus_len+1)
    conn.r.d1ele2 = list(consensus_obj.sequence_string)
    conn.r.d1ele3 = dataset1.mutation_distribution_of_cons(consensus_obj)
    conn.r.d1ele4 = dataset1.mutation_distribution_fraction_of_cons(consensus_obj)
    conn.r.d1name = dataset1.short_description
    
    if dataset2.has_consensus(consensus_obj):
      d2_consensus = consensus_obj
    else:
      d2_consensus = dataset2.most_frequent_consensus()
    
    
    conn.r.d2ele1 = range(1,len(d2_consensus.sequence_string)+1)
    conn.r.d2ele2 = list(d2_consensus.sequence_string)
    conn.r.d2ele3 = dataset2.mutation_distribution_of_cons(d2_consensus)
    conn.r.d2ele4 = dataset2.mutation_distribution_fraction_of_cons(d2_consensus)
    conn.r.d2name = dataset2.short_description
    
    conn.r.fileRoot = file_pos
    conn.eval("print(unlist(d1ele1))")
    conn.eval("print(unlist(d1ele2))")
    conn.eval("print(unlist(d1ele3))")
    conn.eval("print(unlist(d1ele4))")
    conn.eval("origdWT <- data.frame(V1=unlist(d1ele1), V2=unlist(d1ele2), V3=unlist(d1ele3), V4=unlist(d1ele4))")
    conn.eval("origdTG <- data.frame(V1=unlist(d2ele1), V2=unlist(d2ele2), V3=unlist(d2ele3), V4=unlist(d2ele4))")
    conn.r.genename = consensus_obj.gene
    conn.eval("source(scriptRoot)")
    #conn.eval("setwd(pwdRoot)") set in the script - should fix this
    
    conn.eval("doPositionAnalysis(pwdRoot,fileRoot,0,0)")
  
  def line_plot(self, x_coords, y_coords, file_root, input_params={}):
    conn = pyRserve.connect()
    conn.r.pwdRoot = settings.PROJECT_LOCATION_FOR_RSERVE
    conn.r.scriptRoot = settings.PROJECT_LOCATION_FOR_RSERVE+"/shmserver/Rscripts/frequencyPlot.R"
    conn.eval("source(scriptRoot)")
    conn.eval("setwd(pwdRoot)")
    
    conn.r.file_root = file_root
    conn.r.xstuff = x_coords
    conn.r.ystuff = y_coords
    conn.r.xlab = input_params.get("xlab") if input_params.get("xlab") else "Mutations per read"
    conn.r.ylab = input_params.get("ylab") if input_params.get("ylab") else "Number of reads"
    conn.r.main_title = input_params.get("main") if input_params.get("main") else "Mutations per Sequence"
    #theoretically injectable code ahoy!
    conn.eval("mutPlot(file_root,unlist(xstuff),unlist(ystuff),xlab,ylab,main_title)")