var barPadding = 1;

var hotspotMargin = 300

var margin = {top: 20, right: 20, bottom: hotspotMargin + 20, left: 40};

var w = 700 - margin.left - margin.right;
var h = hotspotMargin + 300 - margin.top - margin.bottom;

var x = d3.scale.ordinal().domain(d3.range(dataset.length)).rangeRoundBands([0, w], .05)
var y = d3.scale.linear().domain([0,d3.max(dataset)]).range([h,0]);

var xAxis = d3.svg.axis().scale(x).orient("bottom").tickValues(x.domain().filter(function(d, i) { return !(i % 50); }));

var yAxis = d3.svg.axis().scale(y).orient("left").ticks(10);

var svg = d3.select("div#hotspots_plot")
            .append("svg")
            .attr("width", w+margin.left+margin.right)
            .attr("height", h + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.selectAll("rect")
   .data(dataset)
   .enter()
   .append("rect")
   .attr("class","bar")
   .attr("x", function(d,i) {
       return x(i);
   })
   .attr("y", function(d) {
       return y(d);  //Height minus data value
   })
   .attr("width", x.rangeBand())
   .attr("height", function(d) {
       return h-y(d);  //Just the data value
   });

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + h+ ")")
    .call(xAxis)
  .append("text")
    .attr("y", 35)
    .attr("x", w/2)
    .text("Hotspot position");

svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
  .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", margin.left/4)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Mutation %");

/*svg.append("text")
    .attr("x",x(0)-30)
    .attr("y",3+hotspotMargin)//hotspotMargin+3)
    .attr("font-size","10px")
    .attr("fill","red")
    .text("ACGT");
    
svg.append("text")
    .attr("x",x(0)-30)
    .attr("y",13+hotspotMargin)
    .attr("font-size","10px")
    .text("GGT");
*/
offset = 0;
colors = ["red","green","blue","purple"];
Object.keys(blah).forEach(function(key){
  svg.append("text")
      .attr("x",x(0)-30)
      .attr("y",3+hotspotMargin+offset*10)
      .attr("font-size","10px")
      .attr("fill",colors[offset % 4])
      .text(key);
  
  spots_places = blah[key];
  
  for(i=0; i < spots_places.length;i++){
    svg.append("circle")
        .attr("cx",x(spots_places[i]))
        .attr("cy",hotspotMargin+10*offset)
        .attr("r",2)
        .attr("fill",colors[offset % 4]);
  }
  offset += 1;
});

//for(i=0; i < spots1.length;i++){
//  svg.append("circle")
//      .attr("cx",x(spots1[i]))
//      .attr("cy",hotspotMargin+0)
//      .attr("r",3)
//      .attr("fill","red");
//}
//for(i=0; i < spots2.length;i++){
//  svg.append("circle")
//      .attr("cx",x(spots2[i]))
//      .attr("cy",hotspotMargin+10)
//      .attr("r",3);
//}
//