var makeLineGraph = function(divname, dataset, titlename){
  //based off of http://www.janwillemtulp.com/2011/04/01/tutorial-line-chart-in-d3/
  var margin = {top: 10, right: 10, bottom: 30, left: 50},
      width = 480 - margin.left - margin.right,
      height = 250 - margin.top - margin.bottom;
  
  var data = [4905,1295,77,17,33,18,3];//[3, 6, 2, 7, 5, 2, 1, 3, 8, 9, 2, 5, 7];
  //var formatDate = d3.time.format("%d-%b-%y");
  var data = dataset;
  var x = d3.scale.linear().domain([0,data.length - 1])
      .range([0, width]);

  var y = d3.scale.linear().domain([0,d3.max(data)])
      .range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left");
  var vis = d3.select("body")
      .append("svg:svg")
      .attr("width", width)
      .attr("height", height)
  
  var g = vis.append("svg:g")
      .attr("transform", "translate(0, 200)");
  var line = d3.svg.line()
      .x(function(d,i) { return x(i); })
      .y(function(d) { return y(d); });
  //g.append("svg:path").attr("d", line(data));
  var svg = d3.select("#reportertable").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  //d3.tsv("data.tsv", type, function(error, data) {
  //  if (error) throw error;
  //
  //  x.domain(d3.extent(data, function(d) { return d.date; }));
  //  y.domain(d3.extent(data, function(d) { return d.close; }));

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .append("text")
        .attr("class", "reports")
        .attr("x", 50)
        .attr("y",-15)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Hi there");

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
      .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("Count");

    svg.append("path")
        .attr("class", "reports")
        .datum(data)
        .attr("d", line);
  //});

}