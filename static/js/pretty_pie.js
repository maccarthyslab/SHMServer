

var makePrettyPie = function(divname, dataset, titlename){
  var barPadding = 1;
  
  var hotspotMargin = 0;
  //https://bl.ocks.org/mbostock/3887235
  var margin = {top: 20, right: 20, bottom: hotspotMargin + 20, left: 40};
  
  var w = 250 - margin.left - margin.right;
  var h = hotspotMargin + 250 - margin.top - margin.bottom;
  var radius = Math.min(w, h) / 2.2;
  
  var color = d3.scale.ordinal()
      .range(["#00FF00", "#FF00FF", "#FFFF00", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
  
  var arc = d3.svg.arc()
      .outerRadius(radius - 10)
      .innerRadius(0);
  
  var labelArc = d3.svg.arc()
      .outerRadius(radius - 40)
      .innerRadius(radius - 40);
  
  var svg = d3.select(divname).append("svg")
      .data([dataset])
      .attr("width", w)
      .attr("height", h)
    .append("g")
      .attr("transform", "translate(" + w / 2 + "," + h / 2 + ")");
      
  svg.append("text")
                      .attr("x", 0)             
                      .attr("y", -h/2+margin.top)
                      .attr("text-anchor", "middle")  
                      .style("font-size", "16px") 
                      .style("text-decoration", "underline")  
                      .text(titlename);
                      
  var pie = d3.layout.pie().value(function(d) {return d.value;});
  var arcs = svg.selectAll("g.slice")
          .data(pie)
          .enter()
          .append("svg:g")
          .attr("class", "slice")
          .on('mouseover', function(d){
            $("#tooltip").html(d.data.label+"<br/>"+d.data.value).show();
          })
          .on('mousemove', function(d){
            $("#tooltip").css('left', 400)//d3.mouse(this)[0])
                        .css('top', 250)//d3.mouse(this)[1]-20)
          })
          .on('mouseout', function(d){
            $("#tooltip").html('').hide();
          });
  arcs.append("svg:path")
                  .attr("fill", function(d, i) { return color(i); } )
                  .attr("d", arc);
                  //.append("svg:title").text(function(d){return d.value}); //For tooltip mouseover
  
  arcs.append("div")
      .style("position", "absolute")
      .style("z-index", "10")
      .style("visibility", "hidden")
      .style("background", "#000")
      .text("a simple tooltip");
  
  arcs.append("svg:text")
      .attr("transform", function(d) {                    
            d.innerRadius = 0;
            d.outerRadius = radius;
            return "translate(" + arc.centroid(d) + ")";
                })
              .attr("text-anchor", "middle") 
              .text(function(d, i) { return dataset[i].label; });
}