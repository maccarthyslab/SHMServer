
function render_plot(div_id, data,show_text){
  var margin = {top: 20, right: 20, bottom: 30, left: 40},
      width = 480 - margin.left - margin.right,
      height = 250 - margin.top - margin.bottom;
  
  var y = d3.scale.ordinal()
      .rangeRoundBands([0, height], .1);
  
  var x = d3.scale.linear()
      .rangeRound([0, width]);
  
  var color = d3.scale.ordinal()
      .range(["#D03030", "#B020B0", "#10F080", "#20C0C0"]);
  
  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left");
  
  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("top")
      .tickFormat(d3.format(".2s"));
      
  var svg = d3.select(div_id).append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
    //data = [{"key":"A" , "A": 0, "C": 0, "G": 0, "T": 0},{"key":"C" , "A": 307, "C": 0, "G": 5807, "T": 7996},{"key":"G" , "A": 6091, "C": 0, "G": 0, "T": 1},{"key":"T" , "A": 1, "C": 0, "G": 0, "T": 0}];
  
    color.domain(d3.keys(data[0]).filter(function(key) { return key !== "key"; }));
  
    data.forEach(function(d) {
      var y0 = 0;
      d.ages = color.domain().map(function(name) { return {name2: (d.key +" to " + name), name: name, y0: y0, y1: y0 += +d[name]}; });
      d.total = d.ages[d.ages.length - 1].y1;
    });
  
    //data.sort(function(a, b) { return b.total - a.total; });
  
    y.domain(data.map(function(d) { return d.key; }));
  
    x.domain([0, d3.max(data, function(d) { return d.total; })]);
  
    svg.append("g")
        .attr("class", "y axis")
        //.attr("transform", "translate("+width + ",0)")
        .call(yAxis);
   
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0,300")
        .call(xAxis)
      .append("text")
        //.attr("transform", "rotate(-90)")
        .attr("x", 6)
        .attr("dx", ".71em")
        .style("text-anchor", "end")
        //.attr("transform", "translate(0,300");
  
    var state = svg.selectAll(".fasdfasdf")
        .data(data)
      .enter().append("g")
        .attr("class", "g")
        .attr("transform", function(d) {return "translate(0," + y(d.key) + ")"; });
  
    state.selectAll("rect")
        .data(function(d) { return d.ages; })
      .enter().append("rect")
        .attr("height", y.rangeBand())
        .attr("x", function(d) { return x(d.y0); })
        .attr("width", function(d) {return x(d.y1) - x(d.y0); })
        .style("fill", function(d) { return color(d.name); })
        .on('mouseover', function(d){
          $("#tooltip").html(d.name2+"<br/>"+(d.y1 - d.y0).toString()).show();
        })
        .on('mousemove', function(d){
          $("#tooltip").css('left', 400)//d3.mouse(this)[0])
                      .css('top', 250)//d3.mouse(this)[1]-20)
        })
        .on('mouseout', function(d){
          $("#tooltip").html('').hide()
        });
    if(show_text == 1){
      state.selectAll("text")
        .data(function(d) { return d.ages; })
        .enter().append("text")
        .attr("x", function(d) {return (x(d.y0) + x(d.y1))/2;})
        .attr("y", function(d) { return 30; })
        .style("font-size","10px")
        .text(function(d){
          if(d.y1-d.y0 > 0){
            return d.y1-d.y0;
          } else {
            return ''
          }
        ;});
    }
    //svg.append("text").attr("x", width+10).attr("y",height/2).text("total");
}