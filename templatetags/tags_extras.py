from django import template
from django.conf import settings
from shmserver import models
from django.utils.datastructures import SortedDict


register = template.Library()

@register.simple_tag
def foobar():
  return "Zomg!"

@register.filter(name='cut')
def cut(value, arg):
  return value.replace(arg, '')

@register.filter(name='round')
def round(value):
  if value == None:
    return 'None'
  else:
    return '{0:.4g}'.format(value)

@register.filter(name='round2')
def round2(value):
  if value == None:
    return 'None'
  else:
    return '{0:.2g}'.format(value)
    
@register.filter(name='dict_from_key_off_index')
def dict_from_key_off_index(value, key):
  #use: {{dict|key:key}}
  return(value[str(key)][1])
@register.filter(name='dict_from_key_as_is')
def dict_from_key_as_is(value, key):
  #use: {{dict|key:key}}
  return(value[str(key)])
@register.filter(name='listsort')
def listsort(value):
  if isinstance(value, dict):
    new_dict = SortedDict()
    key_list = sorted(value,key=value.get, reverse=True)
    for key in key_list:
      new_dict[key] = value[key]
    return new_dict.items()
  elif isinstance(value, list):
    return sorted(value)
  else:
    return value

listsort.is_safe = True


@register.filter(name='desc_from_key')
def desc_from_key(value, arg):
  if models.Session.objects.filter(key=arg).exists():
    return models.Session.objects.get(key=arg).description
    
@register.simple_tag
def help_contact():
  return settings.ADMINISTRATOR_EMAIL
