import collections, itertools, Bio, os
from django.db.models import Count, Q, Max, Sum, When, Case, IntegerField
from shmserver.models import Hotspot
from django.db import connection
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import pdb
import time

#this is similar code to the querybuilder and could prob use a bit of refactoring. oh well
class Reporter:
  valid_constraints = ['mutations_per_seq']
  upper_bound = 10
  clade_sizes = {'mutations_per_seq':10}
  
  #this is for getting a query type, eg at mutations, to the type of filter
  #that django filter needs to get the right criteria. see filter for cell
  input_constraint_to_filter_name = {'at_mutations':'num_at_mutation','cg_mutations':"num_cg_mutation",
  'sequence_count':'frequency','silent_mutations':'num_s_mutation','mutations_per_sequence':'num_mutation',
  'rep_mutations':'num_r_mutation','transition_mutations':'num_trans_mutation','transversion_mutations':'num_tsv_mutation',
  'indels_per_sequence':'num_indel','wrc_hotspots':'num_wrc_mutation'}
  
  def __init__(self, dataset):
    self.dataset = dataset
    self.query_constraints = []
    self.base_sequence_set = dataset.sequence_set.prefetch_related('consensus').prefetch_related('mutation_set').all()
  
  def add_constraint(self,constraint):
    if(len(self.query_constraints) <= 1):
      self.query_constraints.append(constraint)
      
  def filter_for_cell(self,constraint,seq_set,criteria):
    '''
    input: a constraint type, a Django QuerySet of sequences, and a number to match (e.g. 3)
    The constraint type logic has to be manually handled for now, so this is a giant switch statement really 
    '''
    if(criteria < 10):
      kwargs = {self.input_constraint_to_filter_name[constraint]:criteria}
    else:
      kwargs = {self.input_constraint_to_filter_name[constraint]+"__gte":10}
    temp_set = seq_set.filter(**kwargs)
    return(temp_set)
  
  def breakdown_seq_by_constraint(self,constraint,seq_set):
    '''
    input: a constraint type, and seqset
    output: a dict
    '''
    count_dict = []
    if(constraint  == "mutations_per_sequence"):
      orig_set = self.base_sequence_set.annotate(num_mutation=Count('mutation'))
      max_obs = orig_set.aggregate(Max('num_mutation'))['num_mutation__max']
      
      for i in range(max_obs+1):
        seq_set = orig_set.filter(num_mutation__exact=i)
        seq_ct = len(seq_set)
        count_dict['row1'].append(seq_ct)
    
  def max_ct_from_criteria(self, constraint, set_to_be_used=None):
    '''
    input: a constraint string
    output: The maximum number of that constraint occurring in a sequence set (e.g. 5 AT mutations)
    '''
    set_to_be_used = self.base_sequence_set
    fetched = self.prefetch_seq_from_criteria(constraint, set_to_be_used)
    if(constraint == 'at_mutations'):
      x = fetched.aggregate(Max('num_at_mutation'))['num_at_mutation__max']
    elif(constraint == 'cg_mutations'):
      x = fetched.aggregate(Max('num_cg_mutation'))['num_cg_mutation__max']
    elif(constraint == "silent_mutations"):
      x = fetched.aggregate(Max('num_s_mutation'))['num_s_mutation__max']
    elif(constraint == "rep_mutations"):
      x = fetched.aggregate(Max('num_r_mutation'))['num_r_mutation__max']
    elif(constraint == "transition_mutations"):
      x = fetched.aggregate(Max('num_trans_mutation'))['num_trans_mutation__max']
    elif(constraint == 'transversion_mutations'):
      x = fetched.aggregate(Max('num_tsv_mutation'))['num_tsv_mutation__max']
    elif(constraint == 'indels_per_sequence'):
      x = fetched.aggregate(Max('num_indel'))['num_indel__max']
    elif(constraint == 'wrc_hotspots'):
      x = fetched.aggregate(Max('num_wrc_mutation'))['num_wrc_mutation__max']
    elif(constraint == 'mutations_per_sequence'):
      x = fetched.aggregate(Max('num_mutation'))['num_mutation__max']
    elif(constraint == 'sequence_count'):
      x = fetched.aggregate(Max('frequency'))['frequency__max']
    else:
      x = 2 #w/e
    if x == None:
      return(0)
    else:
      return(x) 
  
  def prefetch_seq_from_criteria(self, constraint, set_to_be_used=None):
    '''
    input: constraint string
    output: returns the sequence set with the appropriate annotations/aggregates for querying/filtering
    also: see http://stackoverflow.duapp.com/questions/30752268/how-to-filter-objects-for-count-annotation-in-django
    which explains the weird syntax but it works?
    '''
    set_to_be_used = self.base_sequence_set if set_to_be_used == None else set_to_be_used
    if(constraint == "mutations_per_sequence"):
      return(set_to_be_used.annotate(num_mutation=Count('mutation')))
    elif(constraint == "at_mutations"):
      return(set_to_be_used.annotate(num_at_mutation=Sum(Case(When(Q(mutation__mutation_from='A')|Q(mutation__mutation_from='T'), then=1), default=0,output_field=IntegerField()))))
    elif(constraint == "cg_mutations"):
      return(set_to_be_used.annotate(num_cg_mutation=Sum(Case(When(Q(mutation__mutation_from='C')|Q(mutation__mutation_from='G'), then=1), default=0,output_field=IntegerField()))))
    elif(constraint == "silent_mutations"):
      return(set_to_be_used.annotate(num_s_mutation=Sum(Case(When(mutation__r_or_s='S', then=1), default=0, output_field=IntegerField()))))
    elif(constraint == "rep_mutations"):
      return(set_to_be_used.annotate(num_r_mutation=Sum(Case(When(mutation__r_or_s='R', then=1), default=0, output_field=IntegerField()))))
    elif(constraint == "transition_mutations"):
      return(set_to_be_used.annotate(num_trans_mutation=Sum(Case(When(mutation__transversion=False, then=1), default=0, output_field=IntegerField()))))
    elif(constraint == "transversion_mutations"):
      return(set_to_be_used.annotate(num_tsv_mutation=Sum(Case(When(mutation__transversion=True, then=1), default=0, output_field=IntegerField()))))
    elif(constraint == "indels_per_sequence"):
      return(set_to_be_used.annotate(num_indel=Count('indel')))
    elif(constraint == "wrc_hotspots"):
      return(set_to_be_used.annotate(num_wrc_mutation=Sum(Case(When(Q(mutation__lcontext='AG')|Q(mutation__lcontext='AA')|Q(mutation__lcontext='TG')|Q(mutation__lcontext='TA'), then=1), default=0, output_field=IntegerField()))))
    else:
      return(set_to_be_used)
  
  def partition(self):
    '''
    output: if there are one or more query constraints (max two),
    will return an array separated
    e.g. [[3,6],[4,5]]
    e.g. {'row1':[3,6],'row2':[4,5]}
    e.g. {'row1':[35,60],'row2':[50,60]}
    '''
    count_list = []
    if(len(self.query_constraints) == 1):
      init_list = []
      orig_set = self.prefetch_seq_from_criteria(self.query_constraints[0])
      lim_i = min([self.upper_bound+1,self.max_ct_from_criteria(self.query_constraints[0])+1])
      for i in range(lim_i):
        seq_ct = self.filter_for_cell(self.query_constraints[0], orig_set,i)
        init_list.append(len(seq_ct))
      count_list.append(init_list)  
      return(count_list)
    elif(len(self.query_constraints) == 2):
      orig_set = self.prefetch_seq_from_criteria(self.query_constraints[0])
      orig_set = self.prefetch_seq_from_criteria(self.query_constraints[1], orig_set)
      lim_j = min([self.upper_bound+1,self.max_ct_from_criteria(self.query_constraints[1])+1])
      lim_i = min([self.upper_bound+1,self.max_ct_from_criteria(self.query_constraints[0])+1])
      for j in range(lim_j):
        rowname = 'row'+str(j+1)
        init_list = []
        for i in range(lim_i):
          temp_set = self.filter_for_cell(self.query_constraints[0], orig_set,i)
          temp_set = self.filter_for_cell(self.query_constraints[1], temp_set,j)
          init_list.append(temp_set.count())
        count_list.append(init_list)  
      return(count_list)
    