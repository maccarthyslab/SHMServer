from django.test import TestCase
from shmserver import models, querybuilders, reporters, statistics
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import datetime
import pdb
from fractions import Fraction
from django.utils import timezone
import numpy
import re

# Create your tests here.

class FunnyTestCase(TestCase):
  def setUp(self):
      test_fasta = """
>CONSENSUS
ACCACCTCC
>record_1
ACTACCTCG
>record_2
ACCACCTGC
      """
      f = open('temp.fasta','w')
      f.write(test_fasta)
      f.close()
      self.parsed = SeqIO.parse('temp.fasta', 'fasta')
      self.session = models.Session(expire_by=timezone.now())
      self.session.save()

class TestIgnoreDeletionsMutations(TestCase):
  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.ds1 = models.Dataset(session=self.session)
    self.ds1.save()
    self.c1 = models.Consensus(sequence_string='AAA...CCC')
    self.c1.save()
    
  def test_mutations(self):
    s1 = models.Sequence(dataset=self.ds1, consensus=self.c1,sequence_string='AAAG..CCC')
    s1.save()
    s1.add_mutations()
    self.assertEqual(len(models.Mutation.objects.all()),0)

class TestAddMutations(TestCase):
  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.ds1 = models.Dataset(session=self.session)
    self.ds1.save()
    self.c1 = models.Consensus(sequence_string='AAAGGGCCC')
    self.c1.save()
    
  def test_find_mutations(self):
    s1 = models.Sequence(dataset=self.ds1, consensus=self.c1,sequence_string='AAAGCGCCC')
    s1.save()
    muts = s1.find_associated_mutations()
    self.assertEqual(1,len(muts))
    self.assertEqual(5,muts[0].nt_position)
    
  def test_find_mutations_with_max_index(self):
    s1 = models.Sequence(dataset=self.ds1, consensus=self.c1,sequence_string='AAAGCGCCC')
    s1.save()
    muts = s1.find_associated_mutations(max_index=4)
    self.assertEqual(0,len(muts))
    muts2 = s1.find_associated_mutations(max_index=5)
    self.assertEqual(1,len(muts2))
  def test_add_mutations(self):
    s1 = models.Sequence(dataset=self.ds1, consensus=self.c1,sequence_string='AAAGCGCCC')
    s1.save()
    s1.add_mutations()
    self.assertEqual(len(s1.mutation_set.all()),1)
    m1 = s1.mutation_set.all()[0]
    self.assertEqual(m1.nt_position, 5)
  def test_lcontext_and_rcontext(self):
    s1 = models.Sequence(dataset=self.ds1, consensus=self.c1,sequence_string='AAAGCGCCC')
    s1.save()
    s1.add_mutations()
    m1 = s1.mutation_set.all()[0]
    self.assertEqual(m1.lcontext, 'AG')
    self.assertEqual(m1.rcontext, 'GC')
  def test_oob_lcontext_and_rcontext(self):
    s1 = models.Sequence(dataset=self.ds1, consensus=self.c1,sequence_string='AGAGGGCAC')
    s1.save()
    s1.add_mutations()
    m1 = s1.mutation_set.all()[0]
    m2 = s1.mutation_set.all()[1]
    self.assertEqual(m1.lcontext, '-A')
    self.assertEqual(m2.rcontext, 'C-')
    
  def test_lcontext_with_lgaps(self):
    c1 = models.Consensus(sequence_string = "AAA.GGCCC")
    c1.save()
    s1 = models.Sequence(dataset=self.ds1, consensus=c1,sequence_string='..A.GTCCC')
    s1.save()
    s1.add_mutations()
    m1 = s1.mutation_set.all()[0]
    self.assertEqual(m1.lcontext,'AG')

class MyFirstTestCase(FunnyTestCase):

  def test_true_is_true(self):
    self.assertEqual(4,4)
    
  def test_importer_creates_consensus(self):
    self.assertEqual(len(models.Consensus.objects.all()),0)
    self.session.import_data(self.parsed)
    #self.cs.write_consensus()
    self.assertEqual(len(models.Consensus.objects.all()),1)
    
  def test_importer_creates_mutations(self):
    self.assertEqual(len(models.Mutation.objects.all()),0)
    self.session.import_data(self.parsed)
    self.assertEqual(len(models.Mutation.objects.all()),3)
    
  def test_importer_creates_sequences(self):
    self.assertEqual(len(models.Sequence.objects.all()),0)
    self.session.import_data(self.parsed)
    self.assertEqual(len(models.Sequence.objects.all()),2)
    
  def test_importer_autogenerates_description(self):
    self.session.import_data(self.parsed)
    test_dataset = models.Dataset.objects.all()[:1].get()
    test_id = test_dataset.id
    test_name = "dataset_" + str(test_id)
    self.assertEqual(test_dataset.short_description,test_name)
    
  def test_importer_uses_provided_description(self):
    self.session.import_data(self.parsed, "my_dataset")
    test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dataset.short_description,"my_dataset")
    
  def test_importer_populates_hotspots(self):
    h = models.Hotspot(lcontext='AC',hotspot_target='C')
    h.save()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),0)
    self.session.import_data(self.parsed)
    #c = models.Consensus.objects.all()[:1].get()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),2)
class DatasetTestCase(FunnyTestCase):
  def test_mutation_histogram(self):
    self.session.import_data(self.parsed)
    test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dataset.mutation_histogram(), [2,1]) #tomato tomahto
    
  def test_mutation_distribution(self):
    self.session.import_data(self.parsed)
    test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dataset.mutation_distribution(), [0,0,1,0,0,0,0,1,1])
    
  def test_mutation_type_count(self):
    self.session.import_data(self.parsed)
    test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dataset.mutation_type_count()['CT'],1)
    self.assertEqual(test_dataset.mutation_type_count()['CG'],2)
    
  def test_mutation_type_frac(self):
    self.session.import_data(self.parsed)
    test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dataset.mutation_type_frac()['CT'],Fraction(1,3))
    self.assertEqual(test_dataset.mutation_type_frac()['CG'],Fraction(2,3))
    
  def test_mutations_at_motifs(self):
    self.session.import_data(self.parsed)
    test_dataset = models.Dataset.objects.all()[:1].get()

    h = models.Hotspot(lcontext='TC',hotspot_target='C')
    h.save()
    c = models.Consensus.objects.all()[:1].get()
    c.write_hotspots_to_table()
    self.assertEqual(test_dataset.mutations_at_motifs(), {"tcC":{"G":1}})

class MotifMutationTestCase(TestCase):
  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.ds1 = models.Dataset(session=self.session)
    self.ds1.save()
    
    cons = models.Consensus(sequence_string="ACCACCTCC")
    cons.save()
    
    models.Sequence(dataset=self.ds1,consensus=cons,sequence_string="ACTACCTCG",frequency=4).save()
    models.Sequence(dataset=self.ds1,consensus=cons,sequence_string="ACCACCTGC",frequency=1).save()
    for seq in models.Sequence.objects.all():
      seq.add_mutations()
  def test_mutations_at_motifs_with_dupcount(self):

    h = models.Hotspot(lcontext='TC',hotspot_target='C')
    h.save()
    c = models.Consensus.objects.all()[:1].get()
    c.write_hotspots_to_table()
    self.assertEqual(self.ds1.mutations_at_motifs(), {"tcC":{"G":4}})

class UniqueMotifTestCase(TestCase):
  
  def setUp(self):
      test_fasta = """
>CONSENSUS
AGCAGCTCC
>record_2|DUPCOUNT=4
AGTAGCTCC
>record_3|DUPCOUNT=1
AGCAGTTCC
>record_4|DUPCOUNT=4
ACCACTTCC
      """
      f = open('temp.fasta','w')
      f.write(test_fasta)
      f.close()
      h = models.Hotspot(lcontext='AG',hotspot_target='C')
      h.save()
      self.parsed = SeqIO.parse('temp.fasta', 'fasta')
      self.session = models.Session(expire_by=timezone.now())
      self.session.save()

  def test_unique_mutations_at_motifs(self):
    self.session.import_data(self.parsed)
    test_dataset = models.Dataset.objects.all()[:1].get()
    c = models.Consensus.objects.all()[:1].get()
    c.write_hotspots_to_table()
    self.assertEqual(test_dataset.mutations_at_motifs(), {"agC":{"T":9}})
    self.assertEqual(test_dataset.unique_mutations_at_motifs(), {"agC":{"T":2}})
  
class SequenceTestCase(FunnyTestCase):
  def test_mutation_position_distribution(self):
    self.session.import_data(self.parsed)
    sequence = models.Sequence.objects.filter(sequence_string="ACTACCTCG")[0]
    self.assertEqual(sequence.mutation_position_distribution(),[3,9])
    
class MySecondTestCase(FunnyTestCase):
      
  def test_record_mutation_transition(self):
    self.assertEqual(len(models.Sequence.objects.all()),0)
    self.session.import_data(self.parsed)
    sequence = models.Sequence.objects.filter(sequence_string="ACCACCTGC")
    mutation = models.Mutation.objects.filter(sequence=sequence)
    self.assertEqual(len(mutation),1)
    actual_mutation = mutation[0]
    self.assertEqual(actual_mutation.r_or_s,'R')
    self.assertEqual(actual_mutation.amino_acid_from,'S')
    self.assertEqual(actual_mutation.amino_acid_to,'C')
    #self.assertEqual(len(models.Mutation.objects.all()),2)
  
  def test_record_silent_mutation(self):
    self.assertEqual(len(models.Sequence.objects.all()),0)
    self.session.import_data(self.parsed)
    sequence = models.Sequence.objects.filter(sequence_string="ACTACCTCG")
    mutation = models.Mutation.objects.filter(sequence=sequence)
    self.assertEqual(len(mutation),2)
    actual_mutation = mutation[0]
    self.assertEqual(actual_mutation.r_or_s,'S')
    self.assertEqual(actual_mutation.amino_acid_from,'T')
    self.assertEqual(actual_mutation.amino_acid_to,'T')

class CaseWithLowerFasta(TestCase):
  def setUp(self):
      test_fasta = """
>CONSENSUS
ACCACCTCC
>record_2
ACCACCtGC
      """
      f = open('temp.fasta','w')
      f.write(test_fasta)
      f.close()
      self.parsed = SeqIO.parse('temp.fasta', 'fasta')
      self.session = models.Session(expire_by=timezone.now())
      self.session.save()
      
  def test_case_insensitive_mutation(self):
    self.assertEqual(len(models.Sequence.objects.all()),0)
    self.session.import_data(self.parsed)
    sequence = models.Sequence.objects.filter(sequence_string="ACCACCtGC")
    mutation = models.Mutation.objects.filter(sequence=sequence)
    actual_mutation = mutation[0]
    self.assertEqual(actual_mutation.mutation_to,'G')

class CaseWithIndel(TestCase):
  def setUp(self):
    test_fasta = """
>CONSENSUS
ACCACCTCC
>record_2|DUPCOUNT=4|CIGAR=7M1I2M
ACCACCtAGC
>record_3|DUPCOUNT=1|CIGAR=6M1D2M
ACTACCCG
>record_4|DUPCOUNT=1|CIGAR=5M2I2M2D
ACCACAACT
    """
    f = open('temp.fasta','w')
    f.write(test_fasta)
    f.close()
    self.parsed = SeqIO.parse('temp.fasta', 'fasta')
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.session.import_data(self.parsed)
    
  def test_proper_insert(self):
    df = self.session.dataset_set.all()[:1].get()
    self.assertEqual(df.sequence_set.all()[0].sequence_string,'ACCACCTAGC')
    self.assertEqual(df.sequence_set.all()[0].v_string,'ACCACCTGC')
  def test_proper_insert_mutation(self):
    seq = self.session.dataset_set.all()[:1].get().sequence_set.all()[0]
    self.assertEqual(seq.mutation_set.all()[0].nt_position,8)
    #self.assertEqual(df.sequence_set.all()[0].v_string,'ACCACCTGC')
      
  def test_proper_delete(self):
    df = self.session.dataset_set.all()[:1].get()
    self.assertEqual(df.sequence_set.all()[1].sequence_string,'ACTACCCG')
    self.assertEqual(df.sequence_set.all()[1].v_string,'ACTACC.CG')
  def test_proper_delete_mutation(self):
    seq = self.session.dataset_set.all()[:1].get().sequence_set.all()[1]
    self.assertEqual(seq.mutation_set.all()[0].nt_position,3)
    self.assertEqual(seq.mutation_set.all()[1].nt_position,9)
    #self.assertEqual(df.sequence_set.all()[0].v_string,'ACCACCTGC')
  def test_insert_and_delete(self):
    df = self.session.dataset_set.all()[:1].get()
    self.assertEqual(df.sequence_set.all()[2].sequence_string,'ACCACAACT')
    self.assertEqual(df.sequence_set.all()[2].v_string,'ACCACCT..')

  def test_indel_table(self):
    df = self.session.dataset_set.all()[:1].get()
    seq1 = df.sequence_set.all()[0]
    self.assertEqual(len(seq1.indel_set.all()), 1)
    actual_indel = seq1.indel_set.all()[0]
    self.assertEqual(actual_indel.inserted_sequence,'A')
    self.assertEqual(actual_indel.indel_begin,7)
    self.assertEqual(actual_indel.indel_end,7)
  def test_indel_table_with_delete(self):
    df = self.session.dataset_set.all()[:1].get()
    seq1 = df.sequence_set.all()[1]
    self.assertEqual(len(seq1.indel_set.all()), 1)
    actual_indel = seq1.indel_set.all()[0]
    self.assertEqual(actual_indel.insertion_or_del,False)
    self.assertEqual(actual_indel.inserted_sequence,'T')
    self.assertEqual(actual_indel.indel_begin,7)
    self.assertEqual(actual_indel.indel_end,7)
  def test_indel_table_with_both(self):
    df = self.session.dataset_set.all()[:1].get()
    seq1 = df.sequence_set.all()[2]
    self.assertEqual(len(seq1.indel_set.all()), 2)
    actual_indel = seq1.indel_set.all()[0]
    self.assertEqual(actual_indel.insertion_or_del,True)
    self.assertEqual(actual_indel.inserted_sequence,'AA')
    self.assertEqual(actual_indel.indel_begin,5)
    self.assertEqual(actual_indel.indel_end,5)
    
    actual_indel = seq1.indel_set.all()[1]
    self.assertEqual(actual_indel.insertion_or_del,False)
    self.assertEqual(actual_indel.inserted_sequence,'CC')
    self.assertEqual(actual_indel.indel_begin,8)
    self.assertEqual(actual_indel.indel_end,9)
  def test_query_builder(self):
    df = self.session.dataset_set.all()[:1].get()
    constraint_hash = {"query_type":"indels_per_sequence","min_mut":"2"}
    qb = querybuilders.QueryBuilder(df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()#[:1].get()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACCACAACT") #this one is same length but has two indels
    
class CaseWithDupCount(TestCase):
  def setUp(self):
    test_fasta = """
>CONSENSUS
ACCACCTCC
>record_2|DUPCOUNT=4
ACCACCtGC
>record_3|DUPCOUNT=1
ACTACCTGG
    """
    f = open('temp.fasta','w')
    f.write(test_fasta)
    f.close()
    self.parsed = SeqIO.parse('temp.fasta', 'fasta')
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.session.import_data(self.parsed) 
  
  def test_total_mutation_count(self):
    test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dataset.mutation_type_count()['CT'],1)
    self.assertEqual(test_dataset.mutation_type_count()['CG'],6)
    self.assertEqual(test_dataset.mutation_type_count()['A'],0)
    
  def test_mutation_count_to_js(self):
    test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dataset.js_friendly_mutation_dict(), '[{"key":"A" , "A": 0, "C": 0, "G": 0, "T": 0},{"key":"C" , "A": 0, "C": 0, "G": 6, "T": 1},{"key":"G" , "A": 0, "C": 0, "G": 0, "T": 0},{"key":"T" , "A": 0, "C": 0, "G": 0, "T": 0}]')
    
  def test_unique_mutation_count(self):
    test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dataset.unique_mutation_type_count()['CT'],1)
    self.assertEqual(test_dataset.unique_mutation_type_count()['CG'],2)
    
  def test_unique_mutation_count_to_js(self):
    test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dataset.js_friendly_unique_mutation_dict(), '[{"key":"A" , "A": 0, "C": 0, "G": 0, "T": 0},{"key":"C" , "A": 0, "C": 0, "G": 2, "T": 1},{"key":"G" , "A": 0, "C": 0, "G": 0, "T": 0},{"key":"T" , "A": 0, "C": 0, "G": 0, "T": 0}]')
     
  def test_total_sequence_sum(self):
    df = self.session.dataset_set.all()[:1].get()
    self.assertEqual(df.total_sequence_sum(),5)
    
  def test_sequence_frequency_import(self):
    sequence = models.Sequence.objects.filter(sequence_string="ACCACCtGC")
    actual_sequence = sequence[0]
    self.assertEqual(actual_sequence.frequency,4)
    
  def test_mutation_frequency_count(self):
    df = self.session.dataset_set.all()[:1].get()#.sequence_set.all()
    mdf = df.mutation_distribution_fraction()
    self.assertEqual([0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 0.0, 1, 0.2],mdf)
    #pass

class HotspotCase(TestCase):
  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.dataset = models.Dataset(session=self.session)
    self.dataset.save()
    
  def test_full_motif(self):
    h = models.Hotspot(lcontext='AG',hotspot_target='C')
    h.save()
    self.assertEqual(h.full_motif(),'agC')
    
  def test_identify_hotspots(self):
    #FORWARD ORIENTATION ONLY
    h = models.Hotspot(lcontext='AG',hotspot_target='C')
    h.save()
    c = models.Consensus(sequence_string="AAAAGCAAA")
    c.save()
    shazzam = c.identify_hotspots()
    self.assertEqual(shazzam,{h:[6]})
    
  def test_identify_degenerate_hotspots(self):
    h = models.Hotspot(lcontext='WR',hotspot_target='C')
    h.save()
    c = models.Consensus(sequence_string="AAAAGCAAA")
    c.save()
    shazzam = c.identify_hotspots()
    self.assertEqual(shazzam,{h:[6]})
    
  def test_identify_rc_hotspots(self):
    h = models.Hotspot(lcontext='AG',hotspot_target='C')
    h.save()
    c = models.Consensus(sequence_string="AAAAGCTAA")
    c.save()
    shazzam = c.identify_rc_hotspots()
    self.assertEqual(shazzam,{h:[5]})
    
  def test_write_hotspots_to_table(self):
    h = models.Hotspot(lcontext='AG',hotspot_target='C')
    h.save()
    c = models.Consensus(sequence_string="AAAAGCAAA")
    c.save()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),0)
    c.write_hotspots_to_table()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),1)
    hc = models.HotspotConsensus.objects.all()[:1].get()
    self.assertEqual(hc.nt_position,6)

  def test_write_multiple_hotspots_to_table(self):
    h = models.Hotspot(lcontext='AG',hotspot_target='C')
    h.save()
    h2 = models.Hotspot(lcontext='TT',hotspot_target='C')
    h2.save()
    c = models.Consensus(sequence_string="CCCAGCAAATTC")
    c.save()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),0)
    c.write_hotspots_to_table()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),2)

  def test_write_correct_hotspot_begin_to_table(self):
    h = models.Hotspot(lcontext='AG',hotspot_target='C')
    h.save()
    c = models.Consensus(sequence_string="CCCAGCAAATTC")
    c.save()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),0)
    c.write_hotspots_to_table()
    hc= models.HotspotConsensus.objects.all()[:1].get()
    self.assertEqual(hc.position_start,4)
    self.assertEqual(hc.position_end,6)

  def test_write_correct_hotspot_end_to_table(self):
    h = models.Hotspot(lcontext='A',hotspot_target='G', rcontext='CA')
    h.save()
    c = models.Consensus(sequence_string="CCCAGCAAATTC")
    c.save()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),0)
    c.write_hotspots_to_table()
    hc= models.HotspotConsensus.objects.all()[:1].get()
    self.assertEqual(hc.position_start,4)
    self.assertEqual(hc.nt_position,5)
    self.assertEqual(hc.position_end,7)
    
  def test_write_overlapping_hotspots_idempotence(self):
    h = models.Hotspot(lcontext='WR',hotspot_target='C')
    h.save()
    c = models.Consensus(sequence_string="AAAAGCTAA")
    c.save()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),0)
    c.write_hotspots_to_table()
    c.write_hotspots_to_table()
    self.assertEqual(models.HotspotConsensus.objects.all().count(),1)

class ReporterTestCase(FunnyTestCase):
  def setUp(self):
    super(ReporterTestCase, self).setUp()
    self.session.import_data(self.parsed)
    self.df = self.session.dataset_set.all()[:1].get()
  
  def test_reporter_one_criteria(self):
    constraint_type = "mutations_per_sequence"
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type)
    self.assertEqual(r.partition(),[[0,1,1]])
    #self.assertEqual(1,2)
  def test_reporter_at_mutations(self):
    constraint_type = "at_mutations"
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type)
    self.assertEqual(r.partition(),[[2]]) #neither test sequence has an AT mutation
  def test_reporter_cg_mutations(self):
    constraint_type = "cg_mutations"
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type)
    self.assertEqual(r.partition(),[[0,1,1]])
  def test_reporter_max_ct(self):
    constraint_type = "cg_mutations"
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type)
    self.assertEqual(r.max_ct_from_criteria("cg_mutations"),2)
  def test_reporter_silent_mutations(self):
    constraint_type = "silent_mutations"
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type)
    self.assertEqual(r.partition(),[[1,0,1]])
  def test_reporter_replacement_mutations(self):
    constraint_type = "rep_mutations"
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type)
    self.assertEqual(r.partition(),[[1,1]])
  def test_reporter_transition_mutations(self):
    constraint_type = "transition_mutations"
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type)
    self.assertEqual(r.partition(),[[1,1]]) #one sequence has 1 transition one has none
  def test_reporter_transversion_mutations(self):
    constraint_type = "transversion_mutations"
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type)
    self.assertEqual(r.partition(),[[0,2]]) #both sequences have 1 transversion

  def test_two_criteria(self):
    constraint_type_1 = "transversion_mutations"
    constraint_type_2 = "transition_mutations"
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type_1)
    r.add_constraint(constraint_type_2)
    self.assertEqual(r.partition(),[[0,1],[0,1]])
  def test_two_criteria_with_diff_cts(self):
    constraint_type_1 = "silent_mutations" #1 0 1
    constraint_type_2 = "transition_mutations" #1 1 0
    r = reporters.Reporter(self.df)
    r.add_constraint(constraint_type_1)
    r.add_constraint(constraint_type_2)
    self.assertEqual(r.partition(),[[1,0,0],[0,0,1]])
class QueryBuilderCase(FunnyTestCase):

  def setUp(self):
    super(QueryBuilderCase, self).setUp()
    self.session.import_data(self.parsed)
    self.df = self.session.dataset_set.all()[:1].get()

  def test_query_builder(self):
    constraint_hash = {"query_type":"mutated_at_site","position":"8"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()#[:1].get()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACCACCTGC")
  def test_query_builder_with_not(self):
    constraint_hash = {"query_type":"mutated_at_site","position":"8","no_flag":"yay"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()#[:1].get()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACTACCTCG")
  def test_query_builder_with_multiple(self):
    constraint_hash = {"query_type":"mutated_at_site","position":"3"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    constraint_hash = {"query_type":"mutated_at_site","position":"9"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()#[:1].get()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACTACCTCG")
  def test_query_builder_mutation_count(self):
    constraint_hash = {"query_type":"mutations_per_sequence","min_mut":"2"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()#[:1].get()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACTACCTCG")
  def test_query_builder_max_mutation_count(self):
    constraint_hash = {"query_type":"mutations_per_sequence","max_mut":"1"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()#[:1].get()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACCACCTGC")
  def test_query_builder_aa_from(self):
    constraint_hash = {"query_type":"mutating_aa","aa_fr":"T"} #1 mutation happens and is T -> T
    qb = querybuilders.QueryBuilder(self.df) 
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACTACCTCG")
  def test_query_builder_aa_to(self):
    constraint_hash = {"query_type":"mutating_aa","aa_to":"C"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACCACCTGC")
  def test_query_builder_aa_from_and_to(self):
    constraint_hash = {"query_type":"mutating_aa","aa_fr":"S","aa_to":"C"} #1 mutation happens and is T -> T
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACCACCTGC")
  def test_query_builder_aa_from_and_to_negative(self):
    constraint_hash = {"query_type":"mutating_aa","aa_fr":"T","aa_to":"C"} #1 mutation happens and is T -> T
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()
    self.assertEqual(0,len(stuff))
    
  def test_query_builder_at_mutations(self):
    constraint_hash = {"query_type":"at_mutations","min_mut":"1"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()
    self.assertEqual(0,len(stuff)) #all mutations in our test set are C->T or C->G
  
  def test_query_builder_with_virtual_dataset(self):
    vds = models.VirtualDataset(session=self.session, short_description='')
    vds.save()
    for seq in self.df.sequence_set.all():
      vdss = models.VirtualDatasetSequence(virtual_dataset=vds,sequence=seq)
      vdss.save()
    self.assertEqual(len(vds.sequence_set.all()),2)
    
    constraint_hash = {"query_type":"mutating_aa","aa_fr":"S","aa_to":"C"}
    qb = querybuilders.QueryBuilder(vds)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()
    
    self.assertEqual(len(stuff),1)
  
  def test_query_builder_cg_mutations(self):
    constraint_hash = {"query_type":"cg_mutations","min_mut":"2"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACTACCTCG")
  def test_combine_query_builder(self):
    constraint_hash = {"query_type":"mutations_per_sequence","max_mut":"1"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    constraint_hash = {"query_type":"mutated_at_site","position":"8"}
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()#[:1].get()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACCACCTGC")
  def test_combine_query_builder_two(self):
    constraint_hash = {"query_type":"mutations_per_sequence","max_mut":"1"}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    constraint_hash = {"query_type":"mutated_at_site","position":"8", "no_flag":"yay"}
    qb.add_query_hash(constraint_hash)
    stuff = qb.sequence_set()
    self.assertEqual(0,len(stuff))
    
  #reminder: consensus is ACCACCTCC
  #and sequences are: ACCACCTGC and ACTACCTCG
  #so contexts will be  AC*AC for first, and CT*C and TC*
  
  def test_query_builder_degenerate_hotspot_contexts(self):
    h = models.Hotspot(lcontext='Y',hotspot_target='C') #Y = T or C
    h.save()
    
    constraint_hash = {"query_type":"mutations_at_hotspot","hs_id":str(h.id)}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    
    stuff = qb.sequence_set()
    
    self.assertEqual(2,len(stuff))
    self.assertEqual(stuff[1].sequence_string, "ACCACCTGC")
    self.assertEqual(stuff[0].sequence_string, "ACTACCTCG")
    
  def test_query_builder_multiple_contexts(self):
    h = models.Hotspot(lcontext='Y',hotspot_target='C',rcontext='A') #Y = T or C
    h.save()
  
    constraint_hash = {"query_type":"mutations_at_hotspot","hs_id":str(h.id)}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
  
    stuff = qb.sequence_set()
    #print stuff[0].sequence_string
    self.assertEqual(1,len(stuff))
    #self.assertEqual(stuff[1].sequence_string, "ACCACCTGC")
    self.assertEqual(stuff[0].sequence_string, "ACTACCTCG")

  def test_query_builder_mutated_at_hotspot_two_consensus(self):
    h = models.Hotspot(lcontext='S',hotspot_target='C') #Y = T or C
    h.save()
    
    c2 = models.Consensus(sequence_string="AGCCCCCCC")
    c2.save()
    s2 = models.Sequence(dataset=self.df, consensus=c2, sequence_string="AGCTCCCCC")
    s2.save()
    s2.add_mutations()
    
    for c in models.Consensus.objects.all():
      c.write_hotspots_to_table()
      
    constraint_hash = {"query_type":"mutations_at_hotspot","hs_id":str(h.id)}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
    
    stuff = qb.sequence_set()
    
    self.assertEqual(2,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACTACCTCG")
    self.assertEqual(stuff[1].sequence_string, "AGCTCCCCC")
    
  def test_query_builder_mutated_at_hotspot_one_consensus(self):
    h = models.Hotspot(lcontext='C',hotspot_target='C')
    h.save()
  
    for c in models.Consensus.objects.all():
      c.write_hotspots_to_table()
  
    constraint_hash = {"query_type":"mutations_at_hotspot","hs_id":str(h.id)}
    qb = querybuilders.QueryBuilder(self.df)
    qb.add_query_hash(constraint_hash)
  
    stuff = qb.sequence_set()
    self.assertEqual(1,len(stuff))
    self.assertEqual(stuff[0].sequence_string, "ACTACCTCG")
  def test_query_builder_mutated_at_hotspot_one_consensus(self):
    h = models.Hotspot(lcontext='C',hotspot_target='C')
    h.save()
class MutationStatisticsForMultipleConsensus(TestCase):
  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.ds1 = models.Dataset(session=self.session)
    self.ds1.save()
    h = models.Hotspot(lcontext='C',hotspot_target='C')
    h.save()
    h = models.Hotspot(lcontext='T',hotspot_target='G')
    h.save()
    cons = models.Consensus(sequence_string="ACCACCTCC",gene="IGHV1-01")
    cons.save()
    cons.write_hotspots_to_table()
    
    self.cons = cons
    
    cons2 = models.Consensus(sequence_string="GTTGTTCTT",gene="IGHV1-02")
    cons2.save()
    cons2.write_hotspots_to_table()
    self.cons2 = cons2
    
    models.Sequence(dataset=self.ds1,consensus=cons,sequence_string="ACTACCTCG",frequency=4).save()
    models.Sequence(dataset=self.ds1,consensus=cons,sequence_string="ACCACCTGC",frequency=1).save()
    models.Sequence(dataset=self.ds1,consensus=cons2,sequence_string="GTTATTCTT",frequency=3).save()

    self.ds2 = models.Dataset(session=self.session)
    self.ds2.save()
    models.Sequence(dataset=self.ds2,consensus=cons,sequence_string="ACTACCTCG",frequency=3).save()
    models.Sequence(dataset=self.ds2,consensus=cons,sequence_string="ACCACCTGC",frequency=3).save()
    models.Sequence(dataset=self.ds2,consensus=cons2,sequence_string="GATGTTCTT",frequency=3).save()

    for seq in models.Sequence.objects.all():
      seq.add_mutations()

  def test_with_nil_frequency(self):
    cons3 = models.Consensus(sequence_string="GTTGTTCTT",gene="IGHV1-02")
    cons3.save()
    cons3.write_hotspots_to_table()
    self.ds3 = models.Dataset(session=self.session)
    self.ds3.save()
    models.Sequence(dataset=self.ds3,consensus=cons3,sequence_string="GATGTTCTT").save()
    for seq in models.Sequence.objects.filter(dataset=self.ds3):
      seq.add_mutations()
    
    self.assertEqual(self.ds3.possible_motif_mutations()['tG'],1)

  def test_possible_motif(self):
    self.assertEqual(self.ds1.possible_motif_mutations()['tG'], 1)
  
  def test_multiple_consensuses(self):
    self.assertEqual(self.ds1.possible_mutations()['C'], 7)#7
    
  def test_multiple_sequences(self):
    self.assertEqual(self.ds1.possible_mutations()['totalC'], 33) #6 per first 5 sequences

  def test_most_frequent_consensus(self):
    #for imgt data. Frequency = most unique sequences, not raw ct
    #self.assertEqual(self.cons.sequence_string,"ACCACCTCGG")
    self.assertEqual(self.ds1.most_frequent_consensus(),self.cons)
  
  def test_multiple_consensuses(self):
    self.assertEqual(self.ds1.consensuses_list(),[self.cons,self.cons2])

  def test_multiple_consensuses_names(self):
    self.assertEqual(self.ds1.consensuses_list_names(),["IGHV1-01","IGHV1-02"])
  def test_multiple_consensuses_freqs(self):
    #now goes off of frequency not unique sequences
    self.assertEqual(self.ds1.consensuses_list_freqs(),{"IGHV1-01":float(5)/8,"IGHV1-02":float(3)/8})
  
  def test_sequences_with_consensus(self):
    #for imgt data. Frequency = most unique sequences, not raw ct
    #self.assertEqual(self.cons.sequence_string,"ACCACCTCGG")
    subset = self.ds1.sequence_set.exclude(sequence_string="GTTATTCTT").all()
    test_set = self.ds1.sequence_set_with_consensus(self.cons).all()
    self.assertEqual(len(test_set),2)
    self.assertEqual(test_set[0].sequence_string,subset[0].sequence_string)
    self.assertEqual(test_set[1].sequence_string,subset[1].sequence_string)
  def test_mutation_statistics_for_one_dataset(self):
    test_dict = statistics.mutation_statistics_dict(self.ds1, self.ds2)
    #test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dict['C_to_T']['CT'], 4)
    self.assertEqual(test_dict['C_to_G']['CT'], 5)
    self.assertEqual(test_dict['G_to_A']['CT'], 3)
    
    self.assertEqual(test_dict['C_to_T']['CA'], 3)
    self.assertEqual(test_dict['C_to_G']['CA'], 6)
    self.assertEqual(test_dict['T_to_A']['CA'], 3)
    
    self.assertEqual(test_dict['C_to_G']['SITES_CT'], 7)
    self.assertEqual(test_dict['C_to_G']['NCT'], 33)
  def test_mutation_statistics_sum_for_one_dataset(self):
    test_dict = statistics.mutation_statistics_dict(self.ds1, self.ds2)
    #test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dict['C_to_T']['CT'], 4)
    self.assertEqual(test_dict['C_to_G']['CT'], 5)
    self.assertEqual(test_dict['total: C']['CT'], 9)
    self.assertEqual(test_dict['total: C']['CT'], 9)
  def test_unique_mutation_statistics_for_one_dataset(self):
    test_dict = statistics.unique_lazy_mutation_statistics_dict(self.ds1, self.ds2)
    #test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dict['C_to_T']['CT'], 1)
    self.assertEqual(test_dict['C_to_G']['CT'], 2)
    self.assertEqual(test_dict['G_to_A']['CT'], 1)
  
    self.assertEqual(test_dict['C_to_T']['CA'], 1)
    self.assertEqual(test_dict['C_to_G']['CA'], 2)
    self.assertEqual(test_dict['T_to_A']['CA'], 1)
  
    self.assertEqual(test_dict['C_to_G']['SITES_CT'], 7)
  def test_motif_stuff(self):
    test_dict = statistics.motif_statistics_dict(self.ds1,self.ds2)
    self.assertEqual(test_dict['cC_to_T']['CT'],4)
    self.assertEqual(test_dict['tG_to_A']['CT'],3) #for unique should be 1
    self.assertEqual(test_dict['tG_to_A']['NCT'],3) #are u sure about this
  
class MutationStatistics(TestCase):
  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.ds1 = models.Dataset(session=self.session)
    self.ds1.save()
    
    self.cons = models.Consensus(sequence_string="ACCACCTCC")
    self.cons.save()
    
    models.Sequence(dataset=self.ds1,consensus=self.cons,sequence_string="ACTACCTCG",frequency=4).save()
    models.Sequence(dataset=self.ds1,consensus=self.cons,sequence_string="ACCACCTGC",frequency=1).save()
    
    self.ds2 = models.Dataset(session=self.session)
    self.ds2.save()
    models.Sequence(dataset=self.ds2,consensus=self.cons,sequence_string="ACTACCTCG",frequency=3).save()
    models.Sequence(dataset=self.ds2,consensus=self.cons,sequence_string="ACCACCTGC",frequency=3).save()
    
    for seq in models.Sequence.objects.all():
      seq.add_mutations()
    
  def test_mutation_statistics_for_one_dataset(self):
    test_dict = statistics.mutation_statistics_dict(self.ds1, self.ds2)
    #test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dict['C_to_T']['CT'], 4)
    self.assertEqual(test_dict['C_to_G']['CT'], 5)
    self.assertEqual(test_dict['C_to_T']['CA'], 3)
    self.assertEqual(test_dict['C_to_G']['CA'], 6)
    
    self.assertEqual(test_dict['C_to_G']['SITES_CT'], 6)
    
  def test_unique_mutation_statistics_for_one_dataset(self):
    test_dict = statistics.unique_lazy_mutation_statistics_dict(self.ds1, self.ds2)
    #test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dict['C_to_T']['CT'], 1)
    self.assertEqual(test_dict['C_to_G']['CT'], 2)
    self.assertEqual(test_dict['C_to_T']['CA'], 1)
    self.assertEqual(test_dict['C_to_G']['CA'], 2)
  
    self.assertEqual(test_dict['C_to_G']['SITES_CT'], 6)

class PooledMutationStatistics(MutationStatistics):
  def setUp(self):
    super(PooledMutationStatistics, self).setUp()
    self.ds3 = models.Dataset(session=self.session)
    self.ds3.save()
    
    seq1 = models.Sequence(dataset=self.ds3,consensus=self.cons,sequence_string="ACTACCTCG",frequency=3)
    seq1.save()
    seq1.add_mutations()
    
    seq2 = models.Sequence(dataset=self.ds3,consensus=self.cons,sequence_string="ACCACCTGC",frequency=3)
    seq2.save()
    seq2.add_mutations()
    
  def test_unique_mutation_statistics_for_pooled_datasets(self):
    test_dict = statistics.pooled_unique_mutation_statistics_dict([self.ds1], [self.ds2,self.ds3])
    #test_dataset = models.Dataset.objects.all()[:1].get()
    self.assertEqual(test_dict['C_to_T']['CT'], 1)
    self.assertEqual(test_dict['C_to_G']['CT'], 2)
    self.assertEqual(test_dict['C_to_T']['CA'], 2)
    self.assertEqual(test_dict['C_to_G']['CA'], 4)
  
    self.assertEqual(test_dict['C_to_G']['SITES_CA'], 12)
  def test_mutation_statistics_for_pooled_datasets(self):

    test_dict = statistics.pooled_mutation_statistics_dict([self.ds1], [self.ds2,self.ds3])

    self.assertEqual(test_dict['C_to_G']['CA'], 12)
    self.assertEqual(test_dict['C_to_G']['SITES_CA'], 12)
class MotifStatistics(TestCase):
  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.ds1 = models.Dataset(session=self.session)
    self.ds1.save()
    
    h = models.Hotspot(lcontext='C',hotspot_target='C')
    h.save()
    cons = models.Consensus(sequence_string="ACCACCTCC")
    cons.save()
    models.Sequence(dataset=self.ds1,consensus=cons,sequence_string="ACTACCTCG",frequency=4).save()
    models.Sequence(dataset=self.ds1,consensus=cons,sequence_string="ACCACCTGC",frequency=1).save()

    self.ds2 = models.Dataset(session=self.session)
    self.ds2.save()
    
    models.Sequence(dataset=self.ds2,consensus=cons,sequence_string="ACTACCTCG",frequency=3).save()
    models.Sequence(dataset=self.ds2,consensus=cons,sequence_string="ACCACCTGC",frequency=3).save()
    for seq in models.Sequence.objects.all():
      seq.add_mutations()
    c = models.Consensus.objects.all()[:1].get()
    c.write_hotspots_to_table()
    
  def test_motif_statistics(self):
    test_dict = statistics.motif_statistics_dict(self.ds1, self.ds2)
    self.assertEqual(test_dict['cC_to_T']['CT'], 4)
    
class SequenceTest(TestCase):
  def test_mutations_from_cons(self):
    pass
    #thing = models.Sequence.mutations_from_cons('AAA','AAC')
    #self.assertEqual(len(thing),1)
  
class PooledMotifStatistics(MotifStatistics):
  def setUp(self):
    super(PooledMotifStatistics, self).setUp()
    self.ds3 = models.Dataset(session=self.session)
    self.ds3.save()
    
    self.cons = models.Consensus.objects.all()[0]
    
    seq1 = models.Sequence(dataset=self.ds3,consensus=self.cons,sequence_string="ACTACCTCG",frequency=3)
    seq1.save()
    seq1.add_mutations()
    
    seq2 = models.Sequence(dataset=self.ds3,consensus=self.cons,sequence_string="ACCACCTGC",frequency=3)
    seq2.save()
    seq2.add_mutations()
  def test_pooled_motif_statistics(self):
    test_dict = statistics.pooled_motif_statistics_dict([self.ds1], [self.ds2,self.ds3])
    self.assertEqual(test_dict['cC_to_T']['CA'], 6)
  def test_pooled_unique_motif_statistics_dict(self):
    test_dict = statistics.pooled_unique_motif_statistics_dict([self.ds1], [self.ds2,self.ds3])
    self.assertEqual(test_dict['cC_to_T']['CA'], 2)

class GenericStatisticsComparisons(TestCase):
  def test_merge_case(self):
    test_dict_1 = {'CG':5, 'CT':3}
    test_dict_2 = {'CG':8, 'CT':6}
    total_num_dict_1 = {'C':20,'totalC':20}
    total_num_dict_2 = {'C':20,'totalC':20}
    test_dict_results = statistics.generic_dict('C',test_dict_1,test_dict_2,total_num_dict_1,total_num_dict_2, 'CG')
    self.assertEqual(test_dict_results['CA'], 8)

class Blahblah(TestCase):
  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.ds1 = models.Dataset(session=self.session)
    self.ds1.save()
    
    h = models.Hotspot(lcontext='C',hotspot_target='C')
    h.save()
    cons = models.Consensus(sequence_string="ACCACCTCC")
    cons.save()
    seq1 = models.Sequence(dataset=self.ds1,consensus=cons,sequence_string="ACTACCTCG",frequency=4)
    seq1.save()
    models.Sequence(dataset=self.ds1,consensus=cons,sequence_string="ACCACATGC",frequency=1).save()

    for seq in models.Sequence.objects.all():
      seq.add_mutations()
      
    self.vds = models.VirtualDataset(session=self.session)
    self.vds.save()
    
    self.vdss = models.VirtualDatasetSequence(sequence=seq1, virtual_dataset=self.vds)
    self.vdss.save()
    c = models.Consensus.objects.all()[:1].get()
    c.write_hotspots_to_table()
  def test_vds_sequence_sum(self):
    self.assertEqual(self.vds.total_sequence_sum(),4)
  def test_mutation_count(self):
    self.assertEqual(self.vds.mutation_type_count()['CT'],4)
    self.assertEqual(self.vds.mutation_type_count()['CG'],4)
  def test_sum_of_mutation_types(self):
    self.assertEqual(self.vds.mutation_type_count()['C'],8)
    self.assertEqual(self.vds.mutation_type_count()['N'],8)
  def test_mutation_pct(self):
    self.assertEqual(self.vds.mutation_type_pct()['C'], 1.0) #all mutations are C
    self.assertEqual(self.vds.mutation_type_pct()['CT'], 0.5) #half of C mutations are to T
    self.assertEqual(self.vds.mutation_type_pct()['CG'], 0.5) #half of C mutations are to G
    self.assertEqual(self.vds.mutation_type_pct()['N'], float(2)/9) #all mutations are C
  def test_motif_stuff(self):
    self.assertEqual(self.vds.mutations_at_motifs(),{'cC':{'G':4, 'T':4}})
    
  def test_unique_motif_stuff(self):
    cons = models.Consensus.objects.all()[:1].get()
    seq3 = models.Sequence(dataset=self.ds1,consensus=cons,sequence_string="ACTCCCTCT",frequency=2)
    seq3.save()
    seq3.add_mutations()
    
    self.vdss = models.VirtualDatasetSequence(sequence=seq3, virtual_dataset=self.vds)
    self.vdss.save()
    self.assertEqual(self.vds.mutations_at_motifs(),{'cC':{'G':4, 'T':8}})
    self.assertEqual(self.vds.unique_mutations_at_motifs(),{'cC':{'G':1, 'T':2}})
class DifferingConsensusCase(TestCase):
  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    self.ds1 = models.Dataset(session=self.session)
    self.ds1.save()
  
    h = models.Hotspot(lcontext='C',hotspot_target='C')
    h.save()
    cons1 = models.Consensus(sequence_string="ACCACCTCC")
    cons1.save()
    seq1 = models.Sequence(dataset=self.ds1,consensus=cons1,sequence_string="ACTACCTCG",frequency=4)
    seq1.save()
    cons2 = models.Consensus(sequence_string="ATGAAAACCTCC")
    cons2.save()
    models.Sequence(dataset=self.ds1,consensus=cons2,sequence_string="ATGAAAACGTCG",frequency=1).save()
  
    for seq in models.Sequence.objects.all():
      seq.add_mutations()
    for c in models.Consensus.objects.all():
      c.write_hotspots_to_table()
  def test_stuff(self):
    self.assertEqual(self.ds1.mutation_type_count()['CT'],4)
    self.assertEqual(self.ds1.mutation_type_count()['CG'],6)
  def test_unique_stuff(self):
    self.assertEqual(self.ds1.unique_mutation_type_count()['CT'],1)
    self.assertEqual(self.ds1.unique_mutation_type_count()['CG'],3)
  def test_motif(self):
    self.assertEqual(self.ds1.mutations_at_motifs(), {'cC':{'G':6,'T':4}})

class IMGTImporterCase(TestCase):

  def setUp(self):
    self.session = models.Session(expire_by=timezone.now())
    self.session.save()
    #id is being set because for some reason the consensus ids are renewed every time the test is rerun, but the consensus id field on the sequences are not. hence a mysql constraint mismatch
    self.vcons = models.Consensus(id=1,v_region="ACCACCTCC",sequence_string="ACCACCTCC",species="Mus",allele='TestV')
    self.vcons.save()
    self.jcons = models.Consensus(id=2,v_region="ACCACCTCC",sequence_string="ACCACCTCC",species="Mus",allele='TestJ')
    self.jcons.save()
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
"1	AG391_DN_1_67941-1-1	productive	Musmus TestV	Musmus TestJ	Musmus TestD	ACCACCTCG	aaa	aaa	aaa	aaa	aaa	aaa	aaa	NNN	NNN	aaa	aaa	aaa"] #the NNN should be ACCACCTCC but we are removing mutations in that region for now;
    for i in range(3):
      #algorithm removes things not occuring
      self.input_imgt_lines.append(self.input_imgt_lines[1])

  def test_create_imgt_dataset_and_sequences(self):
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    self.assertEqual(len(models.Sequence.objects.all()), 1)
    self.assertEqual(models.Sequence.objects.all()[0].consensus, self.vcons)
    self.assertEqual(models.Sequence.objects.all()[0].j_consensus, self.jcons)

  def test_create_imgt_mutations(self):
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    self.assertEqual(len(models.Mutation.objects.all()), 1) 
    self.assertEqual(models.Mutation.objects.all()[0].nt_position, 9)
   
  def test_max_cdr3_index(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
"1	AG391_DN_1_67941-1-1	productive	Musmus TestV	Musmus TestJ	Musmus TestD	ACCACCTCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	TCC	TCC	aaa	aaa	aaa"]
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    self.assertEqual(len(models.Sequence.objects.all()), 1)
    self.assertEqual(models.Sequence.objects.all()[0].cdr3_start_index,7)
  def test_max_cdr3_index_no_cdr3(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
"1	AG391_DN_1_67941-1-1	productive	Musmus TestV	Musmus TestJ	Musmus TestD	ACCACCTCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	NNN	NNN	aaa	aaa	aaa"]
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    self.assertEqual(len(models.Sequence.objects.all()), 1)
    self.assertEqual(models.Sequence.objects.all()[0].cdr3_start_index,0)
  def test_no_cdr3_muts(self):
    #self.assertEqual(2,3)
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
"1	AG391_DN_1_67941-1-1	productive	Musmus TestV	Musmus TestJ	Musmus TestD	ACGACCTGC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	TGC	TGC	aaa	aaa	aaa",
"2	AG391_DN_1_67942-1-1	productive	Musmus TestV	Musmus TestJ	Musmus TestD	AGCACCTGC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	NNN	NNN	aaa	aaa	aaa"]

    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    self.assertEqual(len(models.Sequence.objects.all()), 2)
    self.assertEqual(len(models.Sequence.objects.all()[0].mutation_set.all()), 1) 
    self.assertEqual(len(models.Sequence.objects.all()[1].mutation_set.all()), 2)
  def test_create_imgt_dels(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
"1	AG391_DN_1_67941-1-1	productive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa"]
    for i in range(3):
      self.input_imgt_lines.append(self.input_imgt_lines[1])
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    
    self.assertEqual(len(models.Indel.objects.all()), 1)
    
    imgt_indel = models.Indel.objects.all()[0]
    
    self.assertEqual(imgt_indel.indel_begin, 4)
    self.assertEqual(imgt_indel.indel_end, 6)
    self.assertEqual(imgt_indel.insertion_or_del, False)
    
  def test_no_cdr3_frameshift(self):
    #pass
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
"1	AG391_DN_1_67941-1-1	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
"2	AG391_DN_1_67941-1-1	productive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCG	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTC	ACCACCTCG	aaa	aaa	aaa"]

    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    self.assertEqual(len(models.Sequence.objects.all()), 2)

    imgt_seq = models.Sequence.objects.all()[0]

    self.assertEqual(imgt_seq.cdr3_frameshift, False)  
    imgt_seq2 = models.Sequence.objects.all()[1]
    self.assertEqual(imgt_seq2.cdr3_frameshift, False)  

  def test_conscount_from_header(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
"1	AG391_DN_1_67941-1-1_CONSCOUNT=3_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTC	ACCACCTCC	aaa	aaa	aaa"]
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    self.assertEqual(len(models.Sequence.objects.all()), 1)

    imgt_seq = models.Sequence.objects.all()[0]

    self.assertEqual(imgt_seq.conscount, 3)
    
  def test_celltype_from_header(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
"1	AG391_DN_1_67941-1-1_CELLTYPE=SS_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTC	ACCACCTCC	aaa	aaa	aaa"]
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    self.assertEqual(len(models.Sequence.objects.all()), 1)

    imgt_seq = models.Sequence.objects.all()[0]

    self.assertEqual(imgt_seq.cell_type, 'SS')

  #----------------------------------------------------------------------------
  # NOTE(srmeier): two sequences with the same V-D-J-REGION column but different cell types should be
  # marked as unique sequences and NOT lumped together
  def test_unique_on_celltype(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
      "1	AG391_DN_1_67941-1-1_CELLTYPE=MEM_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "2	AG391_DN_1_67942-1-1_CELLTYPE=DN_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa"];
    # NOTE(srmeier): parse all the sequences
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines);
    # NOTE(srmeier): assert that we have two sequences and not juse one
    self.assertEqual(len(models.Sequence.objects.all()), 2);

  #----------------------------------------------------------------------------
  # NOTE(srmeier): same as above but with additional fasta input
  def test_unique_on_celltype_FASTA(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
      "1	AG391_DN_1_67941-1-1_CELLTYPE=MEM_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "2	AG391_DN_1_67942-1-1_CELLTYPE=DN_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa"];
    fasta_input = [
      SeqRecord(Seq("ACCTCC"), id=">AG391_DN_1_67941-1-1|CELLTYPE=MEM|x"),
      SeqRecord(Seq("ACCTCC"), id=">AG391_DN_1_67942-1-1|CELLTYPE=DN|x")
    ];
    # NOTE(srmeier): parse all the sequences
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines, "", fasta_input);
    # NOTE(srmeier): assert that we have two sequences and not juse one
    self.assertEqual(len(models.Sequence.objects.all()), 2);

  #----------------------------------------------------------------------------
  # NOTE(srmeier): when the imgt_importer sets the hash table keys these keys then get used to
  # set the "sequence_string" and "v_string" for the sequence object. we want to ensure that
  # adding the cell_type to the hash key doesn't cause these fields to change at all
  def test_unsure_imgt_hash_key_does_not_influence_sequence_string_or_v_string(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
      "1	AG391_DN_1_67941-1-1_CELLTYPE=MEM_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "2	AG391_DN_1_67942-1-1_CELLTYPE=DN_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa"];
    # NOTE(srmeier): parse all the sequences
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines);
    # NOTE(srmeier): assert that we have two sequences and not juse one
    self.assertEqual(len(models.Sequence.objects.all()), 2);
    # NOTE(srmeier): grab the second sequence
    imgt_seq = models.Sequence.objects.all()[1];
    self.assertEqual(imgt_seq.sequence_string, "ACCTCC");
    self.assertEqual(imgt_seq.v_string, "ACC...TCC");

  #----------------------------------------------------------------------------
  # NOTE(srmeier): same as above but with additional fasta input
  def test_unsure_imgt_hash_key_does_not_influence_sequence_string_or_v_string_FASTA(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
      "1	AG391_DN_1_67941-1-1_CELLTYPE=MEM_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "2	AG391_DN_1_67942-1-1_CELLTYPE=DN_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa"];
    fasta_input = [
      SeqRecord(Seq("ACCTCC"), id=">AG391_DN_1_67941-1-1|CELLTYPE=MEM|x"),
      SeqRecord(Seq("ACCTCC"), id=">AG391_DN_1_67942-1-1|CELLTYPE=DN|x")
    ];
    # NOTE(srmeier): parse all the sequences
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines, "", fasta_input);
    # NOTE(srmeier): assert that we have two sequences and not juse one
    self.assertEqual(len(models.Sequence.objects.all()), 2);
    # NOTE(srmeier): grab the second sequence
    imgt_seq = models.Sequence.objects.all()[1];
    self.assertEqual(imgt_seq.sequence_string, "ACCTCC");
    self.assertEqual(imgt_seq.v_string, "ACC...TCC");

  #----------------------------------------------------------------------------
  # NOTE(srmeier): ensure that we are setting the DUPCOUNT correctly when using celltypes
  def test_unsure_correct_dupcounts_when_using_celltypes(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
      "1	AG391_DN_1_67941-1-1_CELLTYPE=MEM_DUPCOUNT=2_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "2	AG391_DN_1_67942-1-1_CELLTYPE=MEM_DUPCOUNT=2_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "3	AG391_DN_1_67943-1-1_CELLTYPE=MEM_DUPCOUNT=2_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "4	AG391_DN_1_67944-1-1_CELLTYPE=DN_DUPCOUNT=1_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa"];
    # NOTE(srmeier): parse all the sequences
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines);
    # NOTE(srmeier): assert that we have two sequences and not juse one
    self.assertEqual(len(models.Sequence.objects.all()), 2);
    # NOTE(srmeier): grab the first unique sequence
    imgt_seq = models.Sequence.objects.all()[0];
    self.assertEqual(imgt_seq.frequency, 6);
    imgt_seq = models.Sequence.objects.all()[1];
    self.assertEqual(imgt_seq.frequency, 1);

  #----------------------------------------------------------------------------
  # NOTE(srmeier): same as above but with additional fasta input
  def test_unsure_correct_dupcounts_when_using_celltypes_FASTA(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
      "1	AG391_DN_1_67941-1-1_CELLTYPE=MEM_DUPCOUNT=2_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "2	AG391_DN_1_67942-1-1_CELLTYPE=MEM_DUPCOUNT=2_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "3	AG391_DN_1_67943-1-1_CELLTYPE=MEM_DUPCOUNT=2_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa",
      "4	AG391_DN_1_67944-1-1_CELLTYPE=DN_DUPCOUNT=1_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa"];
    fasta_input = [
      SeqRecord(Seq("ACCTCC"), id=">AG391_DN_1_67941-1-1|CELLTYPE=MEM|DUPCOUNT=2|x"),
      SeqRecord(Seq("ACCTCC"), id=">AG391_DN_1_67942-1-1|CELLTYPE=MEM|DUPCOUNT=2|x"),
      SeqRecord(Seq("ACCTCC"), id=">AG391_DN_1_67943-1-1|CELLTYPE=MEM|DUPCOUNT=2|x"),
      SeqRecord(Seq("ACCTCC"), id=">AG391_DN_1_67944-1-1|CELLTYPE=DN|DUPCOUNT=1|x")
    ];
    # NOTE(srmeier): parse all the sequences
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines, "", fasta_input);
    # NOTE(srmeier): assert that we have two sequences and not juse one
    self.assertEqual(len(models.Sequence.objects.all()), 2);
    # NOTE(srmeier): grab the first unique sequence
    imgt_seq = models.Sequence.objects.all()[0];
    self.assertEqual(imgt_seq.frequency, 6);
    imgt_seq = models.Sequence.objects.all()[1];
    self.assertEqual(imgt_seq.frequency, 1);
  
  #----------------------------------------------------------------------------
  # NOTE(srmeier): make sure we are ignoring "No results" entries from IMGT
  def test_ignore_no_results(self):
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
      "1	AG391_DN_1_67941-1-1_CELLTYPE=MEM_DUPCOUNT=2_x	No results																",
      "2	AG391_DN_1_67942-1-1_CELLTYPE=MEM_DUPCOUNT=2_x	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTCC	ACCACCTCC	aaa	aaa	aaa"]
    # NOTE(srmeier): parse all the sequences
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines);
    # NOTE(srmeier): assert that we have one sequence
    self.assertEqual(len(models.Sequence.objects.all()), 1);

  def test_cdr3_frameshift(self):
    #pass
    self.input_imgt_lines = ["Sequence number	Sequence ID	Functionality	V-GENE and allele	J-GENE and allele	D-GENE and allele	V-D-J-REGION	V-J-REGION	V-REGION	FR1-IMGT	CDR1-IMGT	FR2-IMGT	CDR2-IMGT	FR3-IMGT	CDR3-IMGT	JUNCTION	J-REGION	FR4-IMGT	",
"1	AG391_DN_1_67941-1-1	nonproductive	Musmus TestV	Musmus TestJ	Musmus TestD	ACC...TCC	aaa	aaa	aaa	aaa	aaa	aaa	aaa	ACCACCTC	ACCACCTCC	aaa	aaa	aaa"]
    self.session.create_imgt_dataset_and_sequences(self.input_imgt_lines)
    self.assertEqual(len(models.Sequence.objects.all()), 1)
      
    imgt_seq = models.Sequence.objects.all()[0]
      
    self.assertEqual(imgt_seq.cdr3_frameshift, True)

class VirtualDatasetCase(FunnyTestCase):
  pass
