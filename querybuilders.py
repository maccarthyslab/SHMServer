import collections, itertools, Bio, os
from django.db.models import Count, Q
from shmserver.models import Hotspot
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import pdb
#{'min_mut': u'', 'query_type': u'mutated_at_site', 'max_mut': u'', 'position': u'99'}
base_nucleotides = ['A','C','G','T']
codes = {'Y':['C','T'], 'W':['A','T'],'R':['A','G'],'S':['G','C'],'N':['A','C','G','T']} #there are more but these are the main ones

def possible_spots_from_context(context):
  '''
  input: a context such as WR
  output: a list of all matching motifs. [eg "AA","AG","TA","TG"]
  '''
  if len(context) == 0:
    yield None
  elif len(context) == 1:
    if(any(context[0] == x for x in base_nucleotides)):
      yield context[0]
    else:
      for code in codes[context[0]]:
        yield code
  else:
    suffixes = possible_spots_from_context(context[1:])
    for suffix in suffixes:
      if(any(context[0] == x for x in base_nucleotides)):
        yield context[0] + suffix
      else:
        for code in codes[context[0]]:
          yield code + suffix
class QueryBuilder:
  #query_constraints = []
  
  def __init__(self, dataset):
    self.dataset = dataset
    self.query_constraints = []
    self.fasta_records = []
    
  def add_query_hash(self, input_hash):
    self.query_constraints += [input_hash]
    
  def sequence_set(self):
    base_sequence_set = self.dataset.sequence_set.prefetch_related('consensus').prefetch_related('mutation_set').all()#.filter(mutation__nt_position=5)
    ambiguous_object = FunQueryStuff(base_sequence_set)
    for constraint in self.query_constraints:
      if constraint["query_type"] == "mutated_at_site":
        ambiguous_object = MutationPositionDecorator(ambiguous_object, constraint)
      elif constraint["query_type"] == "mutations_per_sequence":
        ambiguous_object = MutationCountDecorator(ambiguous_object, constraint)
      elif constraint["query_type"] == "mutating_aa":
        ambiguous_object = AminoAcidDecorator(ambiguous_object, constraint)
      elif constraint["query_type"] == "at_mutations":
        ambiguous_object = NumberATMutationCountDecorator(ambiguous_object, constraint)
      elif constraint["query_type"] == "cg_mutations":
        ambiguous_object = NumberCGMutationCountDecorator(ambiguous_object, constraint)
      elif constraint["query_type"] == "mutations_at_hotspot":
        ambiguous_object = MutationAtHotspotDecorator(ambiguous_object, constraint)
      elif constraint["query_type"] == "indels_per_sequence":
        ambiguous_object = IndelCountDecorator(ambiguous_object, constraint)
      elif constraint["query_type"] == "sequence_count":
        ambiguous_object = MinimumSequenceDecorator(ambiguous_object, constraint)
    return(ambiguous_object.filtered)
  
  def export_to_fasta(self,filename="hi.fa"):
    base_url = os.path.join(self.dataset.media_path(), filename)
    self.generate_fasta()
    sequences = self.fasta_records
    sequences = [SeqRecord(Seq(self.dataset.longest_consensus().sequence_string.rstrip('\n')),id="CONSENSUS",description='')]+sequences
    SeqIO.write(sequences,open(base_url,"w"),"fasta")
    
  def generate_fasta(self):
    #TODO: get a better mismatch string
    ilen = len(self.dataset.longest_consensus().sequence_string)
    sequences = self.sequence_set()
    for sequence in sequences:
      self.fasta_records.append(SeqRecord(Seq(sequence.sequence_string.rstrip('\n')),id="placeholder_id|" + "DUPCOUNT="+str(sequence.norm_frequency())+"|CIGAR="+sequence.cigar_string(),description=""))
    
class FunQueryStuff:
  def __init__(self,initial_sequence_set):
    self.filtered = initial_sequence_set
    
  def filter(self,params):
    return(self.filtered)
    
class BaseDecorator:
  def __init__(self, filtered_object, params):
    self.filtered = filtered_object.filtered
    self.filter( params)
    
  def filter(self,params):
    pass

class MutationAtHotspotDecorator(BaseDecorator):

  
  def filter(self, params):
    if ("hs_id" in params.keys() and params["hs_id"].isdigit()):
      hs_id = int(params["hs_id"])
      if (len(self.filtered) > 0):
        hotspot = Hotspot.objects.get(id=hs_id)
        lcontext, rcontext = hotspot.lcontext, hotspot.rcontext
        lcontexts, rcontexts = list(possible_spots_from_context(lcontext)), list(possible_spots_from_context(rcontext))
        lcontexts = [x for x in lcontexts if x != None]
        rcontexts = [x for x in rcontexts if x != None]
        
        lquery = Q()
        for t in lcontexts:
          lquery |= Q(mutation__lcontext__endswith=t)
        rquery = Q()
        for t in rcontexts:
          rquery |= Q(mutation__rcontext__startswith=t)
        #this is kind of inaccurate for imgt data
        #mut_locations = self.filtered.first().consensus.identify_hotspots()[Hotspot.objects.get(id=hs_id)]
        if ("no_flag" in params.keys()) and params["no_flag"]:
          self.filtered = self.filtered.exclude(lquery).exclude(rquery).distinct()
        else:
          self.filtered = self.filtered.filter(lquery).filter(rquery).distinct()
          #self.filtered = self.filtered.filter(mutation__nt_position__in = mut_locations).distinct()
      else:
        pass

class MinimumSequenceDecorator(BaseDecorator):
  def filter(self, params):
    if "min_mut" in params.keys() and params["min_mut"].isdigit():
      if ("no_flag" in params.keys()) and params["no_flag"]:
        self.filtered = self.filtered.exclude(frequency__gte = params["min_mut"])
      else:
        self.filtered = self.filtered.filter(frequency__gte = params["min_mut"])
    if "max_mut" in params.keys() and params["max_mut"].isdigit():
      if ("no_flag" in params.keys()) and params["no_flag"]:
        self.filtered = self.filtered.exclude(frequency__lte = params["max_mut"])
      else:
        self.filtered = self.filtered.filter(frequency__lte = params["max_mut"])

class IndelCountDecorator(BaseDecorator):
  def filter(self, params):
    if "min_mut" in params.keys() and params["min_mut"].isdigit():
      if ("no_flag" in params.keys()) and params["no_flag"]:
        self.filtered = self.filtered.annotate(num_indel=Count('indel')).exclude(num_indel__gte = params["min_mut"])
      else:
        self.filtered = self.filtered.annotate(num_indel=Count('indel')).filter(num_indel__gte = params["min_mut"])
    if "max_mut" in params.keys() and params["max_mut"].isdigit():
      if ("no_flag" in params.keys()) and params["no_flag"]:
        self.filtered = self.filtered.annotate(num_indel=Count('indel')).exclude(num_indel__lte = params["max_mut"])
      else:
        self.filtered = self.filtered.annotate(num_indel=Count('indel')).filter(num_indel__lte = params["max_mut"])
  
    
class MutationPositionDecorator(BaseDecorator):
    
  def filter(self, params):
    if ("position" in params.keys() and params["position"].isdigit()):
      if ("no_flag" in params.keys()) and params["no_flag"]:
        self.filtered = self.filtered.exclude(mutation__nt_position = params["position"]) #consider using mutation__nt_position__in.distinct()
      else:
        self.filtered = self.filtered.filter(mutation__nt_position = params["position"])

class AminoAcidDecorator(BaseDecorator):

  def filter(self, params):
    if(params.get('aa_to') and params.get('aa_fr')):
      self.filtered = self.filtered.filter(Q(mutation__amino_acid_to = params["aa_to"])&Q(mutation__amino_acid_from = params["aa_fr"]))
    elif(params.get('aa_to')):
      self.filtered = self.filtered.filter(mutation__amino_acid_to = params["aa_to"])
    elif(params.get('aa_fr')):
      self.filtered = self.filtered.filter(mutation__amino_acid_from = params["aa_fr"])

class MutationCountDecorator(BaseDecorator):

  def filter(self, params):
    if "min_mut" in params.keys() and params["min_mut"].isdigit():
      if ("no_flag" in params.keys()) and params["no_flag"]:
        self.filtered = self.filtered.annotate(num_mutation=Count('mutation')).exclude(num_mutation__gte = params["min_mut"])
      else:
        self.filtered = self.filtered.annotate(num_mutation=Count('mutation')).filter(num_mutation__gte = params["min_mut"])
    if "max_mut" in params.keys() and params["max_mut"].isdigit():
      if ("no_flag" in params.keys()) and params["no_flag"]:
        self.filtered = self.filtered.annotate(num_mutation=Count('mutation')).exclude(num_mutation__lte = params["max_mut"])
      else:
        self.filtered = self.filtered.annotate(num_mutation=Count('mutation')).filter(num_mutation__lte = params["max_mut"])

class NumberATMutationCountDecorator(BaseDecorator):

  def filter(self, params):
    if "min_mut" in params.keys() and params["min_mut"].isdigit():
      self.filtered = self.filtered.filter(Q(mutation__mutation_from='A')|Q(mutation__mutation_from='T')).annotate(num_mutation=Count('mutation')).filter(num_mutation__gte = params["min_mut"])
    if "max_mut" in params.keys() and params["max_mut"].isdigit():
      self.filtered = self.filtered.filter(Q(mutation__mutation_from='A')|Q(mutation__mutation_from='T')).annotate(num_mutation=Count('mutation')).filter(num_mutation__lte = params["max_mut"])

class NumberCGMutationCountDecorator(BaseDecorator):
  
  def filter(self, params):
    if "min_mut" in params.keys() and params["min_mut"].isdigit():
      self.filtered = self.filtered.filter(Q(mutation__mutation_from='C')|Q(mutation__mutation_from='G')).annotate(num_mutation=Count('mutation')).filter(num_mutation__gte = params["min_mut"])
    if "max_mut" in params.keys() and params["max_mut"].isdigit():
      self.filtered = self.filtered.filter(Q(mutation__mutation_from='C')|Q(mutation__mutation_from='G')).annotate(num_mutation=Count('mutation')).filter(num_mutation__lte = params["max_mut"])