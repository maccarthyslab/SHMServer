nucleotides = ['A','C','G','T']
import scipy.stats
from shmserver.models import Hotspot

def mutation_statistics_dict(d1, d2):
  return(pooled_mutation_statistics_dict([d1],[d2]))
  
def unique_lazy_mutation_statistics_dict(d1, d2):
  return(pooled_unique_mutation_statistics_dict([d1],[d2]))

def motif_statistics_dict(d1, d2):
  return(pooled_motif_statistics_dict([d1],[d2]))

def unique_lazy_motif_statistics_dict(d1, d2):
  return(pooled_unique_motif_statistics_dict([d1],[d2]))
  
def merge_statistics_dict(dict1, dict2):
  final_dict = dict1.copy()
  for key in dict2.keys():
    if(final_dict.get(key)):
      if isinstance(dict2[key],dict):
        final_dict[key] = merge_statistics_dict(final_dict[key],dict2[key])
      else:
        final_dict[key] += dict2[key]
    else:
      final_dict[key] = dict2[key]
  return(final_dict)

def p_from_contig(count1, count2, othercount1, othercount2):
  if any(x <= 0 for x in [count1, count2, othercount1, othercount2]):
    return None
  else:
    return(scipy.stats.chi2_contingency([[count1,count2],[othercount1, othercount2]])[1])

def pooled_mutation_statistics_dict(list1, list2):
  stat1 = {}
  num_sites_1 = {'A':0,'C':0,'G':0,'T':0,'totalA':0,'totalC':0,'totalG':0,'totalT':0}
  stat2 = {}
  num_sites_2 = {'A':0,'C':0,'G':0,'T':0,'totalA':0,'totalC':0,'totalG':0,'totalT':0}
  
  for ds in list1:
    stat1 = merge_statistics_dict(stat1,ds.mutation_type_count())
    mutations_dict = ds.possible_mutations()
    for nt in nucleotides:
      num_sites_1[nt] += mutations_dict[nt]
      num_sites_1['total'+nt] += mutations_dict['total'+nt]
  for ds in list2:
    stat2 = merge_statistics_dict(stat2,ds.mutation_type_count())
    mutations_dict = ds.possible_mutations()
    for nt in nucleotides:
      num_sites_2[nt] += mutations_dict[nt]
      num_sites_2['total'+nt] += mutations_dict['total'+nt]
  return(output_comparison_dict(stat1, stat2, num_sites_1, num_sites_2))
  
def dict_from_sum(m, keys1,sum_N=False):
  '''
  input: a dict with statistics as well as mutation types eg T_to_C, total: A
  keys1 is like ['A_to_C', 'A_to_G', etc]
  sum_N says whether to sum the total count of sites (false when summing eg A to G AND A to C, since # of A sites is constant)
  output: a dict that is a sum of the relevant parts
  
  '''
  output_dict = {}
  for key in ['CT', 'CA', 'SITES_CT','SITES_CA', 'NCA','NCT']:
    output_dict[key] = iterate_sum_through_keys(m, key, keys1,sum_N)
  output_dict = mutation_dict_with_p(output_dict)
  return(output_dict)
  
def iterate_sum_through_keys(m, key, keys,sum_N=False):
  '''
  input: a dict with each key corresponding to another key, then the key to be summed through
  output: the sum of their values
  '''
  sum = 0
  summed_sites = []
  for input_key in keys:
    begin_char = input_key[0]
    if (not begin_char in summed_sites) or (key == 'CT') or (key == 'CA'): #CT, CA represent counts and they should be summed regardless
      sum += m[input_key][key]
    if '_to_' in input_key:
      summed_sites.append(begin_char)
  return sum
  
  
def mutation_dict_with_p(m):
  '''
  input: a dict with 'CT','SITES_CT', and 'NCT' (and corresponding CA, SITES_CA, NCA)
  output: same dict but with PR sites and p value
  '''
  m['PR_CT'] = (float(m['CT']) / m['NCT'] if m['NCT'] != 0 else None)
  m['PR_CA'] = (float(m['CA']) / m['NCA'] if m['NCA'] != 0 else None)
  m['P'] = p_from_contig(m['CT'],m['CA'], m['NCT']-m['CT'], m['NCA']-m['CA'])
  return(m)

def pooled_unique_mutation_statistics_dict(list1, list2):
  stat1 = {}
  num_sites_1 = {'A':0,'C':0,'G':0,'T':0,'totalA':0,'totalC':0,'totalG':0,'totalT':0}
  stat2 = {}
  num_sites_2 = {'A':0,'C':0,'G':0,'T':0,'totalA':0,'totalC':0,'totalG':0,'totalT':0}
  
  for ds in list1:
    stat1 = merge_statistics_dict(stat1,ds.unique_mutation_type_count())
    mutations_dict = ds.possible_mutations()
    for nt in nucleotides:
      num_sites_1[nt] += mutations_dict[nt]
      num_sites_1['total'+nt]+= mutations_dict[nt]
  for ds in list2:
    mutations_dict = ds.possible_mutations()
    stat2 = merge_statistics_dict(stat2,ds.unique_mutation_type_count())
    for nt in nucleotides:
      num_sites_2[nt] += mutations_dict[nt]
      num_sites_2['total'+nt]+= mutations_dict[nt]
  
  return(output_comparison_dict(stat1, stat2, num_sites_1, num_sites_2))

def output_comparison_dict(stat1, stat2, num_sites_1, num_sites_2):
  '''
  input: dicts containing mutation (nonmotif) statistics of various profiles, as well as the number of possible sites in both
  output: a comprehensive dict containing information on p values, transitions,transversion, etc
  '''
  m = {}
  ordering = 0
  for fromnt in nucleotides:
    for tont in list(set(nucleotides) - set([fromnt])):
      mutation_key = fromnt + "_to_" + tont
      fromandtont = fromnt+tont
      m[mutation_key] = {}
      m[mutation_key] = generic_dict(fromnt,stat1,stat2,num_sites_1,num_sites_2,fromandtont)

  m['total: A'] = dict_from_sum(m, ['A_to_C','A_to_G','A_to_T'])
  m['total: G'] = dict_from_sum(m, ['G_to_C','G_to_A','G_to_T'])
  m['total: T'] = dict_from_sum(m, ['T_to_C','T_to_A','T_to_G'])
  m['total: C'] = dict_from_sum(m, ['C_to_T','C_to_A','C_to_G'])
  m['sum: GC'] = dict_from_sum(m, ['total: G', 'total: C'], True)
  m['sum: AT'] = dict_from_sum(m, ['total: A', 'total: T'], True)
  m['sum: ALL'] = dict_from_sum(m, ['sum: GC', 'sum: AT'], True)
  m['transitions at AT'] = dict_from_sum(m, ['A_to_G', 'T_to_C'], True)
  m['transitions at CG'] = dict_from_sum(m, ['C_to_T', 'G_to_A'], True)
  m['transversions at AT'] = dict_from_sum(m, ['A_to_C', 'A_to_T','T_to_A','T_to_G'], True)
  m['transversions at CG'] = dict_from_sum(m, ['C_to_A', 'C_to_G','G_to_C','G_to_T'], True)

  m['total transitions'] = dict_from_sum(m, ['transitions at AT', 'transitions at CG'], True)
  m['total transversions'] = dict_from_sum(m, ['transversions at AT', 'transversions at CG'], True)
  
  return m

def pooled_motif_statistics_dict(list1, list2):
  '''
  Summarizes difference in mutational statistics between two lists of datasets
  '''
  
  stat1, stat2 = {}, {}
  mutations_list_1, mutations_list_2 = [], []
  
  for ds in list1:
    mutations_list_1.append(ds.mutations_at_motifs())
  for ds in list2:
    mutations_list_2.append(ds.mutations_at_motifs())
  num_motifs1 = aggregate_motif_mutations(list1)
  num_motifs2 = aggregate_motif_mutations(list2)
  
  m = {}
  for hs in Hotspot.objects.all():
    stat1[hs.full_motif()] = stats_from_mutation_list(mutations_list_1, hs)
    stat2[hs.full_motif()] = stats_from_mutation_list(mutations_list_2, hs)
    targeted_nt = hs.hotspot_target
    if (hs.full_motif() in stat1.keys() or hs.full_motif() in stat2.keys()):
      for tont in list(set(nucleotides) - set([targeted_nt])):
        mutation_key = hs.full_motif() + "_to_" + tont
        m[mutation_key] = {}
        m[mutation_key] = generic_motif_dict(tont,stat1,stat2,num_motifs1,num_motifs2,hs,tont)
  return m
  
def aggregate_motif_mutations(input_list):
  '''
  input: a list of datasets such as [dict1, dict2]
  output: a dict showing total number of possible motifs such as 'tC': 5, 'wrC':3, etc
  ''' 
  motif_mutation_dict = {}
  for hs in Hotspot.objects.all():
    motif_mutation_dict[hs.full_motif()] = 0
    motif_mutation_dict['total'+hs.full_motif()] = 0
  for dataset in input_list:
    pmm = dataset.possible_motif_mutations()
    for hs in Hotspot.objects.all():
      motif_mutation_dict[hs.full_motif()] += pmm[hs.full_motif()]
      motif_mutation_dict['total'+hs.full_motif()] += pmm['total'+hs.full_motif()]
  return(motif_mutation_dict)
  
def stats_from_mutation_list(mutations_list, hs):
  stats = {hs.full_motif(): 0}
  for mutation_list in mutations_list:
    if stats.get(hs.full_motif()):
      if mutation_list.get(hs.full_motif()):
        stats[hs.full_motif()] = merge_statistics_dict(stats[hs.full_motif()], mutation_list[hs.full_motif()])
    elif mutation_list.get(hs.full_motif()):
      stats[hs.full_motif()] = mutation_list[hs.full_motif()]
  return(stats[hs.full_motif()])

def pooled_unique_motif_statistics_dict(list1, list2):
  stat1, stat2 = {}, {}
  mutations_list_1 = []
  mutations_list_2 = []
  
  num_motifs1 = aggregate_motif_mutations(list1)
  num_motifs2 = aggregate_motif_mutations(list2)
  #possible_motif_mutations
  for ds in list1:
    mutations_list_1.append(ds.unique_mutations_at_motifs())
  for ds in list2:
    mutations_list_2.append(ds.unique_mutations_at_motifs())
  m = {}
  for hs in Hotspot.objects.all():
    stat1[hs.full_motif()] = stats_from_mutation_list(mutations_list_1, hs)
    stat2[hs.full_motif()] = stats_from_mutation_list(mutations_list_2, hs)
    targeted_nt = hs.hotspot_target
    if (hs.full_motif() in stat1.keys() or hs.full_motif() in stat2.keys()):
      for tont in list(set(nucleotides) - set([targeted_nt])):
        mutation_key = hs.full_motif() + "_to_" + tont
        m[mutation_key] = generic_motif_dict(tont,stat1,stat2,num_motifs1,num_motifs2,hs,tont)
  return m

def generic_dict(fromnt, stat1, stat2, num_motifs1, num_motifs2, full_motif):
  '''
  input: two statistics dict like 'CG':5 and 'CG': 3
  and corresponding num_motifs like 'C':10, 'G': 10
  output:  comparson of two as such: first one 0.5 second 0.3 etc
  '''
  #input: 2 statistics dict like {'cC'}
  
  #output: 
  m = {}
    
  m['CT'] = (stat1.get(full_motif) if stat1.get(full_motif) else 0)
  m['CA'] = (stat2.get(full_motif) if stat2.get(full_motif) else 0)
  m['SITES_CT'] = num_motifs1[fromnt]
  m['SITES_CA'] = num_motifs2[fromnt] ## of spots able to be mutated on consensus
  m['NCT'] = num_motifs1['total'+fromnt]
  m['NCA'] = num_motifs2['total'+fromnt]
  m['PR_CT'] = (float(m['CT']) / m['NCT'] if m['NCT'] != 0 else None)
  m['PR_CA'] = (float(m['CA']) / m['NCA'] if m['NCA'] != 0 else None)
  m['P'] = p_from_contig(m['CT'],m['CA'], m['NCT']-m['CT'], m['NCA']-m['CA'])
  return(m)
  
def generic_motif_dict(fromnt, stat1, stat2, num_motifs1, num_motifs2, hs,tont):
  '''
  input: two statistics dict like 'CG':5 and 'CG': 3
  and corresponding num_motifs like 'C':10, 'G': 10
  output:  comparson of two as such: first one 0.5 second 0.3 etc
  
  by convention, 
  ''' 
  m = {}

  m['CT'] = (stat1.get(hs.full_motif()).get(tont) if stat1.get(hs.full_motif()) and stat1.get(hs.full_motif()).get(tont) else 0)
  m['CA'] = (stat2.get(hs.full_motif()).get(tont) if stat2.get(hs.full_motif()) and stat2.get(hs.full_motif()).get(tont) else 0)
  m['SITES_CT'] = num_motifs1[hs.full_motif()]
  m['SITES_CA'] = num_motifs2[hs.full_motif()] ## of spots able to be mutated on consensus
  m['NCT'] = num_motifs1['total'+hs.full_motif()]
  m['NCA'] = num_motifs2['total'+hs.full_motif()]
  m['PR_CT'] = (float(m['CT']) / m['NCT'] if m['NCT'] != 0 else None)
  m['PR_CA'] = (float(m['CA']) / m['NCA'] if m['NCA'] != 0 else None)
  m['P'] = p_from_contig(m['CT'],m['CA'], m['NCT']-m['CT'], m['NCA']-m['CA'])
  return(m)