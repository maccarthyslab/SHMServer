from django.core.management.base import BaseCommand, CommandError
from shmserver.models import Consensus, Dataset, Sequence
from shmserver.plotters import Plotter
from Bio import SeqIO
import os, re

#this is a baseline task that is here just as an example to show the structure
#of how future tasks can be written

class Command(BaseCommand):
  help = 'Hello world.'

  def handle(self, *args, **options):
    print "Hello world!"
    d = Dataset.objects.get(id=420)
    p = Plotter()
    p.test()
    print d.id