from django.core.management.base import BaseCommand, CommandError
from shmserver.models import Consensus, Dataset
import shmserver.models
from Bio import SeqIO
import os, re
import cProfile

class Command(BaseCommand):
  help = 'This command is a way to test model code, e.g. for profiling:'

  def handle(self, *args, **options):
    d1 = Dataset.objects.get(id=387)
    print "yay"
    print d1.mutations_at_motifs()
    print "yay2"
    my_dict = shmserver.models.pooled_motif_statistics_dict([d1],[d1])
    print(my_dict)