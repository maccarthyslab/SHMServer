from django.core.management.base import BaseCommand, CommandError
from shmserver.models import Consensus, Dataset, Sequence, Session
from shmserver.plotters import Plotter
from Bio import SeqIO
import os, re
from datetime import date, timedelta
#this is a baseline task that is here just as an example to show the structure
#of how future tasks can be written

class Command(BaseCommand):
  help = 'Hello world.'

  def handle(self, *args, **options):
    s = Session.objects.filter(expire_by__lt=(date.today))
    for sub_ses in s:
      print "Deleting session " + str(sub_ses.id) + " with expiry date " + str(sub_ses.expire_by)
      sub_ses.delete()
    print "Done clearing data."