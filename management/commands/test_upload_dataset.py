from django.core.management.base import BaseCommand, CommandError
from shmserver.models import Consensus, Dataset, Sequence, Session
from Bio import SeqIO
import os, re, datetime

class Command(BaseCommand):
  help = 'For testing and debugging purposes, attempts to upload a dataset similar to web interface.'
  
  def add_arguments(self, parser):
    parser.add_argument('filename', nargs=1, type=str)
    #parser.add_argument('cutoff', nargs=1, default=0.8, type=float)
    
  def handle(self, *args, **options):
    s = Session(key='abcdefg',description="hi",expire_by=datetime.datetime.now() + datetime.timedelta(weeks=4))
    s.save()
    my_f = open(options['filename'][0], 'r')
    s.import_imgt(my_f, 'Test')
    s.delete()