from django.core.management.base import BaseCommand, CommandError
from shmserver.models import Hotspot, Consensus

class Command(BaseCommand):
  help = 'Populates the SHMServer hotspot table'
  
  established_hotspots = [['AG','C','T'],['AG','C','A'],['TG','C','A'],['TG','C','T'],['WR','C',''],['','G','YW'],['SY','C',''],['','G','RW'],['T','C',''],['CC','C',''],['','G','GG'],['','G','A'],['W','A',''],['','T','W']]
  
  def handle(self, *args, **options):
    for motif in self.established_hotspots:
      if len(Hotspot.objects.filter(lcontext=motif[0]).filter(rcontext=motif[2]).filter(hotspot_target=motif[1])) == 0:
        h = Hotspot(lcontext=motif[0],rcontext=motif[2],hotspot_target=motif[1],full_sequence=motif[0]+motif[1]+motif[2])
        h.save()
    for consensus in Consensus.objects.all():
      consensus.write_hotspots_to_table()
    self.stdout.write("hi")