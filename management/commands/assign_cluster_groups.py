from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist
from shmserver.models import Consensus, Dataset, Sequence
from Bio import SeqIO
import os, re

class Command(BaseCommand):
  help = 'Assigns clusters to groups to datasets.'
 
  def add_arguments(self, parser):
    parser.add_argument('dataset_id', nargs=1)
    parser.add_argument('cutoff', nargs=1, default=0.8, type=float)
    
  def handle(self, *args, **options):
    # NOTE: Split the dataset_id arguement by "-"
    ds_id = options['dataset_id'][0].split("-");
    ds_id_start = int(ds_id[0]); # first is the dataset to start clustering

    # NOTE: Check if a dataset range was given. If not then set the end to the start (to ensure single iteration)
    ds_id_end = ds_id_start;
    if len(ds_id) > 1:
      ds_id_end = int(ds_id[1]);

    # NOTE: Cluster all the datasets within the range given, using the same cutoff.
    for i in range(ds_id_start, ds_id_end+1):
      ds = None;
      try:
        ds = Dataset.objects.get(id=i)
      except ObjectDoesNotExist:
        print("Dataset with id = "+str(i)+" does not exist.");
      if ds is not None:
        ds.assign_cluster_groups_without_r(options['cutoff'][0])
