from django.core.management.base import BaseCommand, CommandError
from shmserver.models import Consensus
from Bio import SeqIO
import os, re

class Command(BaseCommand):
  help = 'This command imports IMGT Fasta immunoglobulin sequences into the Database Consensus table. Put fasta files in imgt_consensus_fasta'
  
  def handle(self, *args, **options):
    skipped_records = 0
    saved_records = 0
    for i in os.listdir("imgt_consensus_fasta"):
      if i.endswith(".fasta"):
        for each_record in SeqIO.parse("imgt_consensus_fasta/"+i, "fasta"):
          description = str(each_record.description.split("| |")[0]).replace("|","\|")
          temp_list = each_record.description.split("|")
          if len(temp_list) == 16:
            acc_number = str(temp_list[0])
            allele = str(temp_list[1])
            gene = str(temp_list[1].split("*")[0])
            group = gene.split("-")[0]
            if (re.findall("IG[H|L|K][V|D|J]",group) != []):
              group = re.findall("IG[H|L|K][V|D|J]",group)[0]
            sub_group = gene.split("-")[0]
            if (re.findall("[H|L|K][V|D|J]\d",sub_group) != []):
              sub_group = re.findall("[H|L|K][V|D|J]\d",sub_group)[0]
            species = str(temp_list[2])
            v_str = str(each_record.seq).upper()
            sequence_str = v_str # CAREFUL HERE. CHANGE ACCORDINGLY
            
            c_obj = None;

            try:
              c_obj = Consensus.objects.get(species=species, gene=gene, allele=allele);
            except Consensus.DoesNotExist:
              print("consensus not found ("+species+", "+gene+", "+allele+")");

              c_obj = Consensus(accession_number=acc_number,
                                consensus_group=group,
                                consensus_subgroup = sub_group,
                                short_description=description,
                                allele=allele,
                                gene=gene,
                                species=species,
                                gapped=True,
                                sequence_string=sequence_str,
                                v_region=v_str)
              c_obj.save()
              c_obj.write_hotspots_to_table()
            
              saved_records += 1

              continue;
            except Consensus.MultipleObjectsReturned:
              print("multiple consensus found ("+species+", "+gene+", "+allele+")");
              continue;

            #print("consensus is already in the DB ("+species+", "+gene+", "+allele+")");
            skipped_records += 1

          else:
            print("Input file not in correct format. Skipping "+ str(each_record.id))
            skipped_records += 1
      else:
        continue
    print "Skipped records: ", skipped_records
    print "Saved records: ", saved_records