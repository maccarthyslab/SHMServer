import hashlib
import time

def random_stuff():
  hash = hashlib.sha1()
  hash.update(str(time.time()))
  return(hash.hexdigest()[:10])